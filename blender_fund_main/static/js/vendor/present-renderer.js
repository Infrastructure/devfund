var __defProp = Object.defineProperty;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __publicField = (obj, key, value) => {
  __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
  return value;
};
var vertex_default = "#version 300 es\nin vec2 v_pos;void main(){gl_Position=vec4(v_pos,0.0,1.0);}";
var fragment_default = "#version 300 es\nprecision highp float;precision highp usampler2D;precision highp sampler2D;precision highp sampler2DArray;const int CMD_LINE=1;const int CMD_FRAME=4;const int CMD_ORI_RECT=5;const int CMD_GIFT=6;const int TILE_SIZE=5;const int CMD_DATA_BUFFER_LINE=512;const int TILE_CMDS_BUFFER_LINE=256;const int TILES_CMD_RANGE_BUFFER_LINE=4*1024;uniform vec4 color_bg;uniform vec4 gift_colors[32];uniform vec2 gift_sizes[32];uniform vec2 viewport_size;uniform vec2 view_scale;uniform vec2 view_offset;uniform sampler2D cmd_data;uniform usampler2D tile_cmds;uniform usampler2D tile_cmd_ranges;out vec4 fragColor;float scalar_triple_product(vec2 a,vec2 b,vec3 c){vec3 base_normal=vec3(0.0,0.0,a.x*b.y-a.y*b.x);return dot(base_normal,c);}float dot2(vec2 v){return dot(v,v);}vec4 get_cmd_data(int data_idx){ivec2 tex_coord=ivec2(data_idx % CMD_DATA_BUFFER_LINE,int(data_idx/CMD_DATA_BUFFER_LINE));return texelFetch(cmd_data,tex_coord,0);}vec4 get_style_data(int style_idx){ivec2 tex_coord=ivec2(style_idx % CMD_DATA_BUFFER_LINE,CMD_DATA_BUFFER_LINE-1);return texelFetch(cmd_data,tex_coord,0);}uint get_cmd_data_idx(int tile_cmd_idx){ivec2 tex_coord=ivec2(tile_cmd_idx % TILE_CMDS_BUFFER_LINE,int(tile_cmd_idx/TILE_CMDS_BUFFER_LINE));return texelFetch(tile_cmds,tex_coord,0).r;}uint get_tile_cmd_range_start(int tile_idx){ivec2 tex_coord=ivec2(tile_idx % TILES_CMD_RANGE_BUFFER_LINE,0);return texelFetch(tile_cmd_ranges,tex_coord,0).r;}mat2 rotate(float angle){float s=sin(angle);float c=cos(angle);return mat2(c,-s,s,c);}float dist_to_line(vec2 pos,vec2 p1,vec2 p2,float line_radius){vec2 v=(p2-p1);vec2 u=(pos-p1);float h=clamp(dot(u,v)/dot(v,v),0.0,1.0);return length(u-v*h)-line_radius+0.5;}float dist_to_quad(vec2 pos,vec2 a,vec2 b,vec2 c,vec2 d){vec2 ab=b-a;vec2 ap=pos-a;vec2 bc=c-b;vec2 bp=pos-b;vec2 cd=d-c;vec2 cp=pos-c;vec2 da=a-d;vec2 dp=pos-d;vec3 nor=vec3(0.0,0.0,ab.x*da.y-ab.y*da.x);return sqrt((sign(scalar_triple_product(ap,ab,nor))+sign(scalar_triple_product(bp,bc,nor))+sign(scalar_triple_product(cp,cd,nor))+sign(scalar_triple_product(dp,da,nor))<3.0)?min(min(min(dot2(ab*clamp(dot(ab,ap)/dot2(ab),0.0,1.0)-ap),dot2(bc*clamp(dot(bc,bp)/dot2(bc),0.0,1.0)-bp)),dot2(cd*clamp(dot(cd,cp)/dot2(cd),0.0,1.0)-cp)),dot2(da*clamp(dot(da,dp)/dot2(da),0.0,1.0)-dp)): 0.0f);}float dist_to_round_rect(vec2 pos,vec4 rect,float corner_radius){vec2 mid=vec2((rect.x+rect.z),(rect.y+rect.w))*0.5;float dist_x=(pos.x<mid.x)?(rect.x-pos.x):(pos.x-rect.z);float dist_y=(pos.y<mid.y)?(rect.y-pos.y):(pos.y-rect.w);dist_x=max(dist_x+corner_radius,0.0);dist_y=max(dist_y+corner_radius,0.0);return sqrt(dist_x*dist_x+dist_y*dist_y)-corner_radius;}void main(){vec2 frag_coord=vec2(gl_FragCoord.x,gl_FragCoord.y);frag_coord-=0.5;vec3 px_color=color_bg.xyz;float px_alpha=1.0;vec4 view_clip_rect=vec4(0,0,viewport_size.x,viewport_size.y);int num_tiles_x=(int(viewport_size.x)>>TILE_SIZE)+1;int num_tiles_y=(int(viewport_size.y)>>TILE_SIZE)+1;int num_tiles=num_tiles_x*num_tiles_y;int tile_x=int(frag_coord.x)>>TILE_SIZE;int tile_y=int(frag_coord.y)>>TILE_SIZE;int tile_n=tile_y*num_tiles_x+tile_x;int tile_cmds_idx=int(get_tile_cmd_range_start(tile_n));int tile_cmds_end=int(get_tile_cmd_range_start(tile_n+1));while(tile_cmds_idx<tile_cmds_end){int data_idx=int(get_cmd_data_idx(tile_cmds_idx++));vec4 cmd=get_cmd_data(data_idx++);int cmd_type=int(cmd[0]);int style_idx=int(cmd[1]);vec4 style=get_style_data(style_idx);float line_width=style[0];float corner_radius=style[1];vec4 shape_color=get_style_data(style_idx+1);vec4 shape_bounds=get_cmd_data(data_idx++);vec4 clip_rect=vec4(max(shape_bounds.x,view_clip_rect.x),max(shape_bounds.y,view_clip_rect.y),min(shape_bounds.z,view_clip_rect.z),min(shape_bounds.w,view_clip_rect.w));vec2 clip_clamp=vec2(clamp(frag_coord.x,clip_rect.x,clip_rect.z),clamp(frag_coord.y,clip_rect.y,clip_rect.w));float clip_dist=distance(clip_clamp,frag_coord.xy);float shape_dist=0.0;if(cmd_type==CMD_LINE){vec4 shape_def1=get_cmd_data(data_idx++);if(clip_dist>1.0)continue;float line_radius=line_width*0.5;shape_dist=dist_to_line(frag_coord,vec2(shape_def1.xy),vec2(shape_def1.zw),line_radius);}else if(cmd_type==CMD_FRAME){if(clip_dist>1.0)continue;float inner_corner_radius=max(corner_radius-line_width,0.0);vec4 outer_rect=vec4(shape_bounds.x,shape_bounds.y,shape_bounds.z-1.0,shape_bounds.w-1.0);vec4 inner_rect=vec4(shape_bounds.x+line_width,shape_bounds.y+line_width,shape_bounds.z-line_width-1.0,shape_bounds.w-line_width-1.0);float dist_to_outer_rect=dist_to_round_rect(frag_coord,outer_rect,corner_radius);float dist_to_inner_rect=dist_to_round_rect(frag_coord,inner_rect,inner_corner_radius);shape_dist=max(dist_to_outer_rect,1.0-dist_to_inner_rect);}else if(cmd_type==CMD_ORI_RECT){vec4 shape_def1=get_cmd_data(data_idx++);vec4 shape_def2=get_cmd_data(data_idx++);if(clip_dist>1.0)continue;vec2 half_dims=shape_def2.xy*0.5;vec2 pos=shape_def1.xy;float angle=acos(dot(shape_def1.zw,vec2(0.0,1.0)))*sign(shape_def1.z);vec2 p1=(pos+rotate(angle)*vec2(-half_dims.x,+half_dims.y))*view_scale+view_offset;vec2 p2=(pos+rotate(angle)*vec2(+half_dims.x,+half_dims.y))*view_scale+view_offset;vec2 p3=(pos+rotate(angle)*vec2(+half_dims.x,-half_dims.y))*view_scale+view_offset;vec2 p4=(pos+rotate(angle)*vec2(-half_dims.x,-half_dims.y))*view_scale+view_offset;shape_dist=dist_to_quad(frag_coord,p1,p2,p3,p4);}else if(cmd_type==CMD_GIFT){vec4 shape_def1=get_cmd_data(data_idx++);vec4 shape_def2=get_cmd_data(data_idx++);if(clip_dist>1.0)continue;int tierIdx=int(shape_def2.x);vec2 half_dims=gift_sizes[tierIdx].xy*0.5;vec2 pos=shape_def1.xy;float angle=acos(dot(shape_def1.zw,vec2(0.0,1.0)))*sign(shape_def1.z);vec2 p1=(pos+rotate(angle)*vec2(-half_dims.x,+half_dims.y))*view_scale+view_offset;vec2 p2=(pos+rotate(angle)*vec2(+half_dims.x,+half_dims.y))*view_scale+view_offset;vec2 p3=(pos+rotate(angle)*vec2(+half_dims.x,-half_dims.y))*view_scale+view_offset;vec2 p4=(pos+rotate(angle)*vec2(-half_dims.x,-half_dims.y))*view_scale+view_offset;shape_dist=dist_to_quad(frag_coord,p1,p2,p3,p4);shape_color=gift_colors[tierIdx];}float shape_coverage_mask=clamp(1.0-shape_dist,0.0,1.0);float shape_alpha=shape_color.a*shape_coverage_mask;px_alpha=shape_alpha+px_alpha*(1.0-shape_alpha);px_color=mix(px_color,shape_color.rgb,shape_alpha);}fragColor=vec4(px_color,px_alpha);}";
function hexToRGBFloat(hex) {
  return [
    parseInt(hex.slice(1, 3), 16) / 255,
    parseInt(hex.slice(3, 5), 16) / 255,
    parseInt(hex.slice(5, 7), 16) / 255,
    1
  ];
}
class Rect {
  constructor(x, y, w, h) {
    __publicField(this, "left");
    __publicField(this, "right");
    __publicField(this, "top");
    __publicField(this, "bottom");
    __publicField(this, "width");
    __publicField(this, "height");
    this.left = x;
    this.right = x + w;
    this.bottom = y;
    this.top = y + h;
    this.width = w;
    this.height = h;
  }
  contains(x, y) {
    return this.left <= x && x <= this.right && this.bottom <= y && y <= this.top;
  }
  intersects(other) {
    return this.left <= other.right && other.left <= this.right && this.bottom <= other.top && other.bottom <= this.top;
  }
  widen(val) {
    this.left -= val;
    this.bottom -= val;
    this.width += val * 2;
    this.height += val * 2;
    this.right = this.left + this.width;
    this.top = this.bottom + this.height;
  }
  widened(val) {
    return new Rect(
      this.left - val,
      this.bottom - val,
      this.width + val * 2,
      this.height + val * 2
    );
  }
  shrink(val) {
    this.left += val;
    this.bottom += val;
    this.width -= val * 2;
    this.height -= val * 2;
    this.right = this.left + this.width;
    this.top = this.bottom + this.height;
  }
  encapsulate(point) {
    this.left = Math.min(this.left, point[0]);
    this.right = Math.max(this.right, point[0]);
    this.bottom = Math.min(this.bottom, point[1]);
    this.top = Math.max(this.top, point[1]);
    this.width = this.right - this.left;
    this.height = this.top - this.bottom;
  }
}
class View extends Rect {
  constructor(x, y, w, h, scale, offset) {
    super(x, y, w, h);
    __publicField(this, "scaleX");
    __publicField(this, "scaleY");
    __publicField(this, "offsetX");
    __publicField(this, "offsetY");
    this.scaleX = scale[0];
    this.scaleY = scale[1];
    this.offsetX = offset[0];
    this.offsetY = offset[1];
  }
  getXYScale() {
    return Math.min(this.scaleX, this.scaleY);
  }
  // Transform a position value to this View's coordinates, in the horizontal axis.
  transformPosX(p) {
    return (p - this.left) * this.scaleX + this.left + this.offsetX;
  }
  // Transform a position to this View's coordinates, in the vertical axis.
  transformPosY(p) {
    return (p - this.bottom) * this.scaleY + this.bottom + this.offsetY;
  }
  // Transform a distance value to this View's coordinates, in the horizontal axis.
  transformDistX(d) {
    return d * this.scaleX;
  }
  // Transform a distance value to this View's coordinates, in the vertical axis.
  transformDistY(d) {
    return d * this.scaleY;
  }
  // Transform a rectangle to this View's coordinates.
  transformRect(r) {
    return new Rect(
      this.transformPosX(r.left),
      this.transformPosY(r.bottom),
      this.transformDistX(r.width),
      this.transformDistY(r.height)
    );
  }
}
const MAX_CMD_BUFFER_LINE = 512;
const MAX_CMD_DATA = MAX_CMD_BUFFER_LINE * MAX_CMD_BUFFER_LINE;
const MAX_STYLE_CMDS = MAX_CMD_BUFFER_LINE;
const MAX_SHAPE_CMDS = MAX_CMD_DATA - MAX_STYLE_CMDS;
const TILE_SIZE = 5;
const MAX_TILES = 4 * 1024 - 1;
const MAX_CMDS_PER_TILE = 128;
const TILE_CMDS_BUFFER_LINE = 256;
class UIRenderer {
  // Initialize the renderer: compile the shader and setup static data.
  constructor(canvas, colorBg = [0.7176470588235294, 0.7529411764705882, 0.9, 1], giftColors, giftWidths, giftHeights) {
    // Rendering context
    __publicField(this, "gl");
    // Viewport transform
    __publicField(this, "view", new View(0, 0, 1, 1, [1, 1], [0, 0]));
    __publicField(this, "viewport", { width: 1, height: 1 });
    // Shader data
    __publicField(this, "shaderInfo");
    __publicField(this, "buffers");
    __publicField(this, "cmdData", new Float32Array(MAX_CMD_DATA * 4));
    // Pre-allocate commands of 4 floats (128 width).
    __publicField(this, "cmdDataIdx", 0);
    // Tiles
    __publicField(this, "num_tiles_x", 1);
    __publicField(this, "num_tiles_y", 1);
    __publicField(this, "num_tiles_n", 1);
    __publicField(this, "cmdsPerTile", new Array(MAX_TILES));
    // Unpacked list of commands, indexed by tile. Used when adding shapes.
    __publicField(this, "tileCmds", new Uint16Array(TILE_CMDS_BUFFER_LINE * TILE_CMDS_BUFFER_LINE));
    // Packed list of commands.
    __publicField(this, "tileCmdRanges", new Uint16Array(MAX_TILES + 1));
    // Where each tile's data is in tileCmds. List of start indexes.
    // Style
    __publicField(this, "styleDataStartIdx", (MAX_CMD_DATA - MAX_STYLE_CMDS) * 4);
    // Start writing style to the last cmd data texture line.
    __publicField(this, "styleDataIdx", this.styleDataStartIdx);
    __publicField(this, "styleStep", 2 * 4);
    // Number of floats that a single style needs.
    // State
    __publicField(this, "stateColor", [-1, -1, -1, -1]);
    __publicField(this, "stateLineWidth", 1);
    __publicField(this, "stateCorner", 0);
    __publicField(this, "stateChanges", 0);
    if (giftColors.length !== giftWidths.length || giftColors.length !== giftHeights.length)
      throw new Error("Gift renderer: constructor passed mismatched number of gift sizes and colors");
    if (giftColors.length > 32)
      throw new Error("Gift renderer: too many gift tiers, update the shader");
    const gl = canvas.getContext("webgl2");
    if (!gl) {
      alert("Unable to initialize WebGL. Your browser may not support WebGL2.");
      throw new Error("UIRenderer failed to get WebGL 2 context");
    }
    this.gl = gl;
    const shaderProgram = initShaderProgram(gl, vertex_default, fragment_default);
    if (shaderProgram === null) {
      throw new Error("UIRenderer failed to initialize shader");
    }
    this.shaderInfo = {
      program: shaderProgram,
      attrs: {
        vertexPos: bindAttr(gl, shaderProgram, "v_pos")
      },
      uniforms: {
        vpSize: bindUniform(gl, shaderProgram, "viewport_size"),
        viewScale: bindUniform(gl, shaderProgram, "view_scale"),
        viewOffset: bindUniform(gl, shaderProgram, "view_offset"),
        bgColor: bindUniform(gl, shaderProgram, "color_bg"),
        giftColors: bindUniform(gl, shaderProgram, "gift_colors"),
        giftSizes: bindUniform(gl, shaderProgram, "gift_sizes"),
        cmdBufferTex: bindUniform(gl, shaderProgram, "cmd_data"),
        tileCmdRangesBufferTex: bindUniform(gl, shaderProgram, "tile_cmd_ranges"),
        tileCmdsBufferTex: bindUniform(gl, shaderProgram, "tile_cmds")
      }
    };
    this.buffers = {
      pos: gl.createBuffer(),
      // Sadly, WebGL2 does not support Buffer Textures (no gl.texBuffer() or gl.TEXTURE_BUFFER target).
      // It doesn't support 1D textures either. We're left with a UBO or a 2D image for command data storage.
      // Chose a 2D image because it can support more data than the UBO.
      // According to WebGL2 support statistics, there's support for textures of up to 4096x4096 pixels,
      // compared to UBOs of 16384 bytes.
      cmdBufferTexture: gl.createTexture(),
      tileCmdRangesTexture: gl.createTexture(),
      tileCmdsTexture: gl.createTexture()
    };
    const positions = new Float32Array([
      1,
      1,
      -1,
      1,
      1,
      -1,
      -1,
      -1
    ]);
    gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.pos);
    gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);
    gl.bindTexture(gl.TEXTURE_2D, this.buffers.cmdBufferTexture);
    gl.texStorage2D(
      gl.TEXTURE_2D,
      // Allocate immutable storage.
      1,
      // Number of mip map levels.
      gl.RGBA32F,
      // GPU internal format: 4x 32bit float components.
      MAX_CMD_BUFFER_LINE,
      MAX_CMD_BUFFER_LINE
    );
    disableMipMapping(gl);
    gl.bindTexture(gl.TEXTURE_2D, this.buffers.tileCmdRangesTexture);
    gl.texStorage2D(
      gl.TEXTURE_2D,
      // Allocate immutable storage.
      1,
      // Number of mip map levels.
      gl.R16UI,
      // GPU internal format: 16bit unsigned integer components.
      MAX_TILES + 1,
      1
    );
    disableMipMapping(gl);
    gl.bindTexture(gl.TEXTURE_2D, this.buffers.tileCmdsTexture);
    gl.texStorage2D(
      gl.TEXTURE_2D,
      // Allocate immutable storage.
      1,
      // Number of mip map levels.
      gl.R16UI,
      // GPU internal format: 16bit unsigned integer components.
      TILE_CMDS_BUFFER_LINE,
      TILE_CMDS_BUFFER_LINE
    );
    disableMipMapping(gl);
    {
      gl.useProgram(this.shaderInfo.program);
      gl.uniform4f(this.shaderInfo.uniforms.bgColor, colorBg[0], colorBg[1], colorBg[2], colorBg[3]);
      const giftColorData = new Float32Array(giftColors.length * 4);
      let i = 0;
      for (const color of giftColors) {
        giftColorData[i++] = color[0];
        giftColorData[i++] = color[1];
        giftColorData[i++] = color[2];
        giftColorData[i++] = color[3];
      }
      gl.uniform4fv(this.shaderInfo.uniforms.giftColors, giftColorData);
      const giftSizesData = new Float32Array(giftWidths.length * 2);
      i = 0;
      for (var j = 0; j < giftWidths.length; j++) {
        giftSizesData[i++] = giftWidths[j];
        giftSizesData[i++] = giftHeights[j];
      }
      gl.uniform2fv(this.shaderInfo.uniforms.giftSizes, giftSizesData);
      gl.useProgram(null);
    }
  }
  addOrientedRect(pos, ori, width, height, color, patternIdx) {
    const bounds = new Rect(pos[0], pos[1], 0, 0);
    const halfWidth = width * 0.5;
    const halfHeight = height * 0.5;
    bounds.widen(Math.sqrt(halfWidth * halfWidth + halfHeight * halfHeight));
    if (this.addPrimitiveShape(5, bounds, color, 0, 0)) {
      let w = this.cmdDataIdx;
      this.cmdData[w++] = pos[0];
      this.cmdData[w++] = pos[1];
      this.cmdData[w++] = ori[0];
      this.cmdData[w++] = ori[1];
      this.cmdData[w++] = width;
      this.cmdData[w++] = height;
      this.cmdData[w++] = patternIdx;
      w += 1;
      this.cmdDataIdx = w;
    }
  }
  addGift(pos, ori, width, height, tierIdx) {
    const bounds = new Rect(pos[0], pos[1], 0, 0);
    const halfWidth = width * 0.5;
    const halfHeight = height * 0.5;
    bounds.widen(Math.sqrt(halfWidth * halfWidth + halfHeight * halfHeight));
    if (this.addPrimitiveShape(6, bounds, [-1, -1, -1, -1], 0, 0)) {
      let w = this.cmdDataIdx;
      this.cmdData[w++] = pos[0];
      this.cmdData[w++] = pos[1];
      this.cmdData[w++] = ori[0];
      this.cmdData[w++] = ori[1];
      this.cmdData[w++] = tierIdx;
      w += 3;
      this.cmdDataIdx = w;
    }
  }
  // Internal functions to write data to the command buffers.
  // Private. Write the given shape to the global command buffer and add it to the tiles with which it overlaps.
  // Returns false if it was unable to allocate the command.
  writeCmdToTiles(cmdType, bounds) {
    let w = this.cmdDataIdx;
    if (w / 4 + 4 > MAX_SHAPE_CMDS) {
      console.warn("Too many shapes to draw.", w / 4 + 4, "of", MAX_SHAPE_CMDS);
      return false;
    }
    {
      const shape_tile_start_y = Math.max(bounds.bottom >> TILE_SIZE, 0);
      const shape_tile_start_x = Math.max(bounds.left >> TILE_SIZE, 0);
      const shape_tile_end_x = Math.min(bounds.right >> TILE_SIZE, this.num_tiles_x - 1);
      const shape_tile_end_y = Math.min(bounds.top >> TILE_SIZE, this.num_tiles_y - 1);
      for (let y = shape_tile_start_y; y <= shape_tile_end_y; y++) {
        for (let x = shape_tile_start_x; x <= shape_tile_end_x; x++) {
          const tile_idx = y * this.num_tiles_x + x;
          const num_tile_cmds = ++this.cmdsPerTile[tile_idx][0];
          if (num_tile_cmds > MAX_CMDS_PER_TILE - 2) {
            console.warn("Too many shapes in a single tile");
          }
          this.cmdsPerTile[tile_idx][num_tile_cmds] = w / 4;
        }
      }
    }
    this.cmdData[w++] = cmdType;
    this.cmdData[w++] = (this.styleDataIdx - this.styleDataStartIdx - this.styleStep) / 4;
    w += 2;
    this.cmdData[w++] = bounds.left;
    this.cmdData[w++] = bounds.bottom;
    this.cmdData[w++] = bounds.right;
    this.cmdData[w++] = bounds.top;
    this.cmdDataIdx = w;
    return true;
  }
  // Private. Write the given style to the global style buffer if it is different from the current active style.
  pushStyleIfNew(color, lineWidth, corner) {
    if (!this.stateColor.every((c, i) => c === color[i]) || lineWidth !== null && this.stateLineWidth !== lineWidth || corner !== null && this.stateCorner !== corner) {
      this.stateColor = color;
      this.stateLineWidth = lineWidth ?? 1;
      this.stateCorner = corner ?? 0;
      this.stateChanges++;
      let sw = this.styleDataIdx;
      if ((sw - this.styleDataStartIdx) / 4 + 2 > MAX_STYLE_CMDS) {
        console.warn("Too many different styles to draw.", sw / 4 + 2, "of", MAX_STYLE_CMDS);
        sw = this.styleDataStartIdx;
      }
      this.cmdData[sw++] = this.stateLineWidth;
      this.cmdData[sw++] = this.stateCorner;
      sw += 2;
      this.cmdData.set(this.stateColor, sw);
      sw += 4;
      this.styleDataIdx = sw;
    }
  }
  // Private. Write the given shape and its style to the command buffers, if it is in the current view.
  addPrimitiveShape(cmdType, bounds, color, lineWidth, corner) {
    const v = this.view;
    bounds = v.transformRect(bounds);
    if (bounds.right < v.left || bounds.left > v.right || bounds.top < v.bottom || bounds.bottom > v.top) {
      return false;
    }
    const view_scale = v.getXYScale();
    this.pushStyleIfNew(color, lineWidth * view_scale, corner * view_scale);
    return this.writeCmdToTiles(cmdType, bounds);
  }
  // Views
  setView(scale, offset) {
    const view = new View(0, 0, this.viewport.width, this.viewport.height, scale, offset);
    this.view = view;
    return view;
  }
  // Render Loop
  // Initialize the state for a new frame
  beginFrame() {
    this.viewport.width = this.gl.canvas.offsetWidth;
    this.viewport.height = this.gl.canvas.offsetHeight;
    this.num_tiles_x = (this.viewport.width >> TILE_SIZE) + 1;
    this.num_tiles_y = (this.viewport.height >> TILE_SIZE) + 1;
    this.num_tiles_n = this.num_tiles_x * this.num_tiles_y;
    if (this.num_tiles_n > MAX_TILES) {
      console.warn(
        "Too many tiles: ",
        this.num_tiles_n,
        "(",
        this.num_tiles_x,
        "x",
        this.num_tiles_y,
        "). Max is",
        MAX_TILES
      );
    }
    for (let i = 0; i < this.num_tiles_n + 1; i++) {
      this.tileCmdRanges[i] = 0;
    }
    for (let i = 0; i < this.num_tiles_n; i++) {
      this.cmdsPerTile[i] = new Uint16Array(MAX_CMDS_PER_TILE);
    }
    this.setView([1, 1], [0, 0]);
  }
  // Draw a frame with the current primitive commands.
  draw() {
    const gl = this.gl;
    gl.viewport(0, 0, this.viewport.width, this.viewport.height);
    gl.useProgram(this.shaderInfo.program);
    gl.invalidateFramebuffer(gl.FRAMEBUFFER, [gl.COLOR]);
    gl.uniform2f(this.shaderInfo.uniforms.vpSize, this.viewport.width, this.viewport.height);
    const v = this.view;
    gl.uniform2f(this.shaderInfo.uniforms.viewScale, v.scaleX, v.scaleY);
    gl.uniform2f(this.shaderInfo.uniforms.viewOffset, v.offsetX, v.offsetY);
    gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.pos);
    gl.enableVertexAttribArray(this.shaderInfo.attrs.vertexPos);
    gl.vertexAttribPointer(
      this.shaderInfo.attrs.vertexPos,
      // Shader attribute index
      2,
      // Number of elements per vertex
      gl.FLOAT,
      // Data type of each element
      false,
      // Normalized?
      0,
      // Stride if data is interleaved
      0
      // Pointer offset to start of data
    );
    let textureUnit = 0;
    {
      const numCmds = this.cmdDataIdx / 4;
      gl.activeTexture(gl.TEXTURE0 + textureUnit);
      gl.bindTexture(gl.TEXTURE_2D, this.buffers.cmdBufferTexture);
      let width = Math.min(numCmds, MAX_CMD_BUFFER_LINE);
      let height = Math.ceil(numCmds / MAX_CMD_BUFFER_LINE);
      gl.texSubImage2D(
        gl.TEXTURE_2D,
        0,
        // Transfer data
        0,
        0,
        width,
        height,
        // x,y offsets, width, height.
        gl.RGBA,
        gl.FLOAT,
        // Source format and type.
        this.cmdData
      );
      const numStyleData = (this.styleDataIdx - this.styleDataStartIdx) / 4;
      const styleWidth = Math.min(numStyleData, MAX_CMD_BUFFER_LINE);
      gl.texSubImage2D(
        gl.TEXTURE_2D,
        0,
        0,
        MAX_CMD_BUFFER_LINE - 1,
        styleWidth,
        1,
        // x,y offsets, width, height.
        gl.RGBA,
        gl.FLOAT,
        this.cmdData,
        this.styleDataStartIdx
      );
      gl.uniform1i(this.shaderInfo.uniforms.cmdBufferTex, textureUnit++);
      let tileCmdIdx = 0;
      for (let ti = 0; ti < this.num_tiles_n; ti++) {
        this.tileCmdRanges[ti] = tileCmdIdx;
        for (let i = 0; i < this.cmdsPerTile[ti][0]; i++) {
          this.tileCmds[tileCmdIdx++] = this.cmdsPerTile[ti][i + 1];
        }
      }
      this.tileCmdRanges[this.num_tiles_n] = tileCmdIdx;
      gl.activeTexture(gl.TEXTURE0 + textureUnit);
      gl.bindTexture(gl.TEXTURE_2D, this.buffers.tileCmdsTexture);
      width = Math.min(tileCmdIdx, TILE_CMDS_BUFFER_LINE);
      height = Math.ceil(tileCmdIdx / TILE_CMDS_BUFFER_LINE);
      gl.texSubImage2D(
        gl.TEXTURE_2D,
        0,
        0,
        0,
        width,
        height,
        // x,y offsets, width, height.
        gl.RED_INTEGER,
        gl.UNSIGNED_SHORT,
        this.tileCmds
      );
      gl.uniform1i(this.shaderInfo.uniforms.tileCmdsBufferTex, textureUnit++);
      width = Math.min(this.num_tiles_n, MAX_TILES) + 1;
      gl.activeTexture(gl.TEXTURE0 + textureUnit);
      gl.bindTexture(gl.TEXTURE_2D, this.buffers.tileCmdRangesTexture);
      gl.texSubImage2D(
        gl.TEXTURE_2D,
        0,
        0,
        0,
        width,
        1,
        // x,y offsets, width, height.
        gl.RED_INTEGER,
        gl.UNSIGNED_SHORT,
        this.tileCmdRanges
      );
      gl.uniform1i(this.shaderInfo.uniforms.tileCmdRangesBufferTex, textureUnit++);
    }
    gl.drawArrays(
      gl.TRIANGLE_STRIP,
      0,
      // Offset.
      4
      // Vertex count.
    );
    gl.disableVertexAttribArray(this.shaderInfo.attrs.vertexPos);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);
    gl.useProgram(null);
    this.cmdDataIdx = 0;
    this.setView([1, 1], [0, 0]);
    this.stateColor = [-1, -1, -1, -1];
    this.styleDataIdx = this.styleDataStartIdx;
    this.stateChanges = 0;
  }
}
function disableMipMapping(gl) {
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
}
function bindAttr(gl, program, attrName) {
  const attr_idx = gl.getAttribLocation(program, attrName);
  if (attr_idx === -1)
    throw new Error("UIRenderer: Can not bind attribute '" + attrName + "'. Misspelled in renderer or shader code?");
  return attr_idx;
}
function bindUniform(gl, program, attrName) {
  const loc = gl.getUniformLocation(program, attrName);
  if (loc === null)
    throw new Error("UIRenderer: Can not bind uniform '" + attrName + "'. Misspelled in renderer or shader code?");
  return loc;
}
function initShaderProgram(gl, vs_source2, fs_source2) {
  const vs = loadShader(gl, gl.VERTEX_SHADER, vs_source2);
  const fs = loadShader(gl, gl.FRAGMENT_SHADER, fs_source2);
  if (vs === null || fs === null)
    return null;
  const program = gl.createProgram();
  gl.attachShader(program, vs);
  gl.attachShader(program, fs);
  gl.linkProgram(program);
  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    console.error("UIRenderer: An error occurred compiling a shader program: " + gl.getProgramInfoLog(program));
    gl.deleteProgram(program);
    return null;
  }
  return program;
}
function loadShader(gl, shader_type, source_code) {
  const shader = gl.createShader(shader_type);
  gl.shaderSource(shader, source_code);
  gl.compileShader(shader);
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    console.error("UIRenderer: An error occurred compiling a shader: " + gl.getShaderInfoLog(shader));
    gl.deleteShader(shader);
    return null;
  }
  return shader;
}
