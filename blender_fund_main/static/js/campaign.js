/* This file references { UIRenderer, hexToRGBFloat } from "/static/js/vendor/present-renderer.js";
* */

async function fetchOrdersJSON() {
  const response = await fetch('/campaign/orders_raw');
  const orders = await response.json();
  return orders;
}
let ownOrderIdxs = []; // Default until the data arrives.
let giftHoverIdx = -1;
let liveDroppedGifts = 0;
let fitFactor = 1;

// Data - the presents!
async function fetchOfflineSimulationData() {
  const response = await fetch('/static/offline-simulation-data.json');
  const offlineSimulationData = await response.json();
  return offlineSimulationData;
}

// HTML area for the presents simulation.
const canvas = document.getElementById('campaign-gifts-pile');
canvas.height = canvas.offsetHeight;
canvas.width = canvas.offsetWidth;
const WIDTH = canvas.offsetWidth / 1;
const HEIGHT = canvas.offsetHeight / 1;

// Gift types.
const labels = ["Bronze", "Silver", "Gold", "Platinum", "Titanium", "Diamond"];
const subscriptionTiers = [5*12, 10*12, 25*12, 50*12, 100*12, 250*12]; // Anual value in EUR.
const donateOnceTiers = [5, 10, 25, 50, 100, 150, 250, 500, 1000, 5000, 10000]; // Order value in EUR. Gift sizes "round" to these values.

const numRenewTiers = subscriptionTiers.length; // Once for renewed, another for new subscriptions.
const numSubsTiers = subscriptionTiers.length * 2; // Once for renewed, another for new subscriptions.
const numOnceTiers = donateOnceTiers.length;
const masses = subscriptionTiers.concat(subscriptionTiers).concat(donateOnceTiers); // in euros :)
function getTierIdx(giftAmount, giftType) {
  // Match the order values given by the backend with the suggested tiers offered in the front end in euros.
  giftAmount = giftAmount / 100; // cents to euro units.
  // Recurring monthly subscriptions ('s'ubscribed, 'r'enewed).
  if (giftType === 'r' || giftType === 's') {
    giftAmount = giftAmount * 12;
  }

  // Look up the specified amount in the array, picking the closest tier in euros.
  const minTierIdx = (giftType === 'r') ? 0 : (giftType === 's') ? numRenewTiers : numSubsTiers; // Donations ('d') are in the second half.
  const maxTierIdx = (giftType === 'r') ? numRenewTiers : (giftType === 's') ? numSubsTiers : numSubsTiers+numOnceTiers;
  for (let idx = maxTierIdx-2;  idx >= minTierIdx ; idx--) {
    // If the value is bigger than this tier and closer to higher tier than to this one, we found it: return the highest.
    const upperRange = masses[idx+1] - masses[idx];
    if (giftAmount > masses[idx] + upperRange * 0.5) {
      return idx+1;
    }
  }
  return minTierIdx;
}


// Simulation Configuration

const gravityScale = 0.0003; // Controls the speed of the fall and how compressed the presents get.

const colorBg     = '#edf4fd';
const colorBoxOwn = '#ff5a44';
const colorBoxOnce= '#9dbbdf';
const colorBox5   = '#ffd1a8';
const colorBox10  = '#bddbff';
const colorBox25  = '#ffb668';
const colorBox50  = '#8aadff';
const colorBox100 = '#ee9543';
const colorBox250 = '#7795cd';
const colors = [//colorBox5, colorBox10, colorBox25, colorBox50, colorBox100, colorBox250
              ].concat(
                Array( // Renewals
                  '#bddbff',
                  '#a7c9f3',
                  '#8aadff',
                  '#7da0f3',
                  '#7795cd',
                  '#7294d2',
                ).concat(
                  Array( // New
                    '#f3c9a3',
                    '#ffd1a8',
                    '#ffb668',
                    '#f6b56e',
                    '#ef9c4f',
                    '#ee9543',
                    ).concat(
                    Array(numOnceTiers).fill('#ff8213'))))  // once
              .map((v) => hexToRGBFloat(v));
const colorOfOwnGift = hexToRGBFloat(colorBoxOwn);

const scale = 6 / Math.sqrt(12);  // Visual size based on gift value is balanced with "monthly factor".
let widthVariancePercent = [      // How much longer is one side vs the other.
  0.0, 0.7, 0.3, 0.5, 0.25, 0.1,  // e.g. 0.5 = one side is 50% longer than the other.
  0.0, 0.7, 0.3, 0.5, 0.25, 0.1,  // 0.0 is a square. Don't type 1.0 ;)
  ].concat(Array(numOnceTiers).fill(0.0)); // All donate once boxes are square.

// ~ end Configuration

// Calculate present sizes in px.
const m_to_px = 100;
const px_to_m = 1 / m_to_px;
const sidesPx = masses.map((v) => Math.sqrt(v) * scale);
const widthsPx =  sidesPx.map((v, i) => v + v * widthVariancePercent[i]);
const heightsPx = widthsPx.map((v, i) => sidesPx[i]*sidesPx[i] / v);
if (widthsPx[0] < 3 || heightsPx[0] < 3 || sidesPx[0] < 3)
  console.warn("Smallest present is only", widthsPx[0].toFixed(1), "x", heightsPx[0].toFixed(1),
    "px wide. It won't be well visible. Increase 'scale' and make it moar square.");
//console.log("Present widths in px:", widthsPx);
//console.log("Present heights in px:", heightsPx);

// Runtime data for the present simulation and present renderer.
class PresentSimData {
  presentID = 0;    // Index into the presents. It uniquely ids a present for the sim and is returned for the tooltip.
  tierIdx = 0;      // Index into the subscription tiers configurations (labels, box color, size, etc...).
  pos = [0.0, 0.0]; // Current position of the center of each present. In physics sim coordinates.
  ori = [0.0, 1.0]; // Current orientation of each present, as a normalized heading vector with Y-up being forward.
}
const presentSimData = [];

let orders = [];

// Setup the physics simulation using Matter.js

function addWalls(world, worldWidthPx, worldHeightPx) {
  // Make walls relatively thick, so that objects don't go through.
  // Position them around (outside) the simulation area.
  const wall = 500;
  const h = worldHeightPx * 2;
  const simOptions = { isStatic: true, render: { opacity: 1 }, label: "border"};
  Matter.Composite.add(world, [
    Matter.Bodies.rectangle(worldWidthPx*0.5, worldHeightPx+wall*0.5, worldWidthPx + wall, wall, simOptions), // Floor
    Matter.Bodies.rectangle(worldWidthPx + wall*0.5, -wall + h*0.5, wall, h, simOptions), // Right
    Matter.Bodies.rectangle(           0 - wall*0.5, -wall + h*0.5, wall, h, simOptions)  // Left
  ]);
}

function addPresents(world, worldWidthPx, worldHeightPx, orders) {
  // Calculate grid layout.
  const gap = 3; // num pixels in between gifts and the simulation margin, so things don't drag on each other.
  const gridCellSize = Math.floor(widthsPx[widthsPx.length - 3] + gap); // Temp: use third largest gift size.
  // const numCols = Math.ceil(worldWidthPx / gridCellSize);
  //console.log("Distributing", totalPresents, "gifts in", numCols, "cols,", numRows, "rows,", gridCellSize, "px cell side");
  //console.log(numCols * gridCellSize, worldWidthPx, worldWidthPx - (numCols * gridCellSize));
  const startY = worldHeightPx - 50;
  const startX = gap;

  // Create the matter.js bodies in a "stack" arrangement.
  let presentsGroup = Matter.Composite.create({ label: 'PresentStack' });

  let x = startX;
  let y = startY;
  let row = 0;
  let maxObservedHeightInRow = 0;
  for (let p=0; p<orders.length; p++) {
    let tierIdx = getTierIdx(orders[p][1], orders[p][2]);

    // Created the gift for the mattter.js simulation.
    // Create the shape.
    const width = widthsPx[tierIdx];
    const height = heightsPx[tierIdx];
    let body = Matter.Bodies.rectangle(x, y, width, height, {label: String(p)})

    // Re-query the bounds in case we introduce the shapes with an initial random orientation.
    let bodyHeight = body.bounds.max.y - body.bounds.min.y;
    let bodyWidth  = body.bounds.max.x - body.bounds.min.x;
    if (bodyHeight > maxObservedHeightInRow)
      maxObservedHeightInRow = bodyHeight;

    // Add the shape.
    Matter.Body.translate(body, { x: bodyWidth * 0.5, y: bodyHeight * 0.5 });
    Matter.Body.rotate(body, Math.random() * 20-10);
    Matter.Composite.addBody(presentsGroup, body);

    // Convert from matter.js (0,0) at top-left with y down and angles along x
    // to (0,0) at bottom-left with y up with angles along y.
    presentSimData[p].pos = [body.position.x * px_to_m, (HEIGHT - body.position.y) * px_to_m];
    presentSimData[p].ori = [Math.sin(body.angle), Math.cos(body.angle)];

    // Calculate position to spawn next gift.
    x = body.bounds.max.x + gap;
    if (x > worldWidthPx - gap) {
      x = startX;
      y -= maxObservedHeightInRow + gap;
      maxObservedHeightInRow = 0;
      row ++;
    }
  }

  Matter.Composite.add(world, presentsGroup);
}

// create engine
const engine = Matter.Engine.create({
  positionIterations: 10,
  velocityIterations: 10,
  enableSleeping: true,
  gravity: { scale: gravityScale },
});

// Create the setup.
const world = engine.world;


// Frame update loop.
const fps = 30;
const frame_dur = Math.floor(1000/fps);
let start_time = 0;
function tick_simulation(current_time) {
  // Calculate the time that has elapsed since the last frame
  const delta_time = current_time - start_time;
  // Tick a frame only after enough time accumulated.
  if (delta_time >= frame_dur && window.innerWidth >= 666) {
    start_time = current_time;

    updatePhysics(frame_dur);
    draw();
  }

  requestAnimationFrame(tick_simulation);
}

const uiRenderer = new UIRenderer(canvas, hexToRGBFloat(colorBg), colors, widthsPx, heightsPx);
tick_simulation(frame_dur);


function updatePhysics(delta_time) {
  // Tick the physics simulation.
  Matter.Engine.update(engine, delta_time);

  // Retrieve the simulation state from the physics sim to the renderer.
  const physicsSimBodies = Matter.Composite.allBodies(world);
  // Start at id 3 to skip the walls, plus any dynamically dropped gifts that matter.js might have prepended.
  let p = 0;
  let mp = 3 + liveDroppedGifts;
  for (; p < physicsSimBodies.length - 3 - liveDroppedGifts; p++, mp++) {
    const body = physicsSimBodies[mp];
    // Convert from matter.js (0,0) at top-left with y down and angles along x
    // to (0,0) at bottom-left with y up with angles along y.
    presentSimData[p].pos = [body.position.x * px_to_m, (HEIGHT - body.position.y) * px_to_m];
    presentSimData[p].ori = [Math.sin(body.angle), Math.cos(body.angle)];
  }
  // Append the dynamically dropped gifts where they belong.
  mp = 3;
  for (; mp < 3 + liveDroppedGifts; p++, mp++) {
    const body = physicsSimBodies[mp];
    // Convert from matter.js (0,0) at top-left with y down and angles along x
    // to (0,0) at bottom-left with y up with angles along y.
    presentSimData[p].pos = [body.position.x * px_to_m, (HEIGHT - body.position.y) * px_to_m];
    presentSimData[p].ori = [Math.sin(body.angle), Math.cos(body.angle)];
  }
}


function draw() {
  const ui = uiRenderer;
  ui.beginFrame();

  const view = ui.setView([fitFactor, fitFactor], [0, 0]);

  // Draw gifts (after the data arrived from the backend).
  if (presentSimData.length) {

    // Draw all presents.
    for (const present of presentSimData) {
      const tier = present.tierIdx;
      const p_px = [ present.pos[0] * m_to_px, present.pos[1] * m_to_px ];
      ui.addGift(p_px, present.ori, widthsPx[tier], heightsPx[tier], tier);
    }
    // Draw the own purchased gift(s) with a special color on top.
    for (const ownOrderIdx of ownOrderIdxs) {
      const present = presentSimData[ownOrderIdx];
      const tier = present.tierIdx;
      const p_px = [ present.pos[0] * m_to_px, present.pos[1] * m_to_px ];
      ui.addOrientedRect(p_px, present.ori, widthsPx[tier], heightsPx[tier], colorOfOwnGift, tier);
    }
    // Draw a semi-transparent shape for gift mouseover.
    if (giftHoverIdx !== -1) {
      const present = presentSimData[giftHoverIdx];
      const tier = present.tierIdx;
      const p_px = [ present.pos[0] * m_to_px, present.pos[1] * m_to_px ];
      ui.addOrientedRect(p_px, present.ori, widthsPx[tier], heightsPx[tier], [1.0, 1.0, 1.0, 0.2], tier);
    }
  }

  ui.draw();
}

const mouse = Matter.Mouse.create(canvas);
function mouseSetup(engine, world) {
  // add mouse control
  const mouseConstraint = Matter.MouseConstraint.create(engine, {
    mouse: mouse,
    constraint: {
      stiffness: 0.2,
      render: {
        visible: false
      }
    }
  });
  // Allow scrolling over the canvas
  mouseConstraint.mouse.element.removeEventListener("mousewheel", mouseConstraint.mouse.mousewheel);
  mouseConstraint.mouse.element.removeEventListener("DOMMouseScroll", mouseConstraint.mouse.mousewheel);

  Matter.Composite.add(world, mouseConstraint);

  const tooltip = document.getElementById('tooltip');
  function generateTooltipContent(orderIdx) {
    const currencySymbols = {'USD': '$', 'EUR': '&euro;', 'CNY': '&yen;'};
    const order = orders[orderIdx];
    const tierIdx = getTierIdx(order[1], order[2]);
    const name = order[3] !== '' ? order[3] : 'Someone';
    let verb = '';
    let value = '';
    let category = '';
    switch (order[2]) {
      case 'd':
        verb = 'donated';
        value = currencySymbols[order[4]] + '&nbsp;' + (Number(order[1]) / 100).toFixed(2);
        category = '';
        break;
      case 'r':
        verb = 'renewed their';
        value = labels[tierIdx];
        category = 'membership';
        break;
      case 's':
        verb = 'signed up with a';
        value = labels[tierIdx-numRenewTiers];
        category = 'membership';
        break;
    }
    const thankyou = (ownOrderIdxs.includes(orderIdx)) ? "<br><br>Thank you!" : "";
    return `<strong>${name}</strong>&nbsp;${verb}<br>${value}&nbsp;${category}&nbsp;${thankyou}`;
  }

  //Add event with 'mousemove'
  Matter.Events.on(mouseConstraint, 'mousemove', function (event) {
    const foundPhysics = Matter.Query.point(Matter.Composite.allBodies(world), event.mouse.position);
    if (foundPhysics[0] === undefined || foundPhysics.length === 0 || foundPhysics[0].label === "border") {
      tooltip.classList.remove('is-visible');
      giftHoverIdx = -1;
    } else {
      tooltip.classList.add('is-visible');

      const orderIdx = Number(foundPhysics[0].label);
      tooltip.innerHTML = generateTooltipContent(orderIdx);
      giftHoverIdx = orderIdx;
    }
  });

  document.addEventListener('mousemove', (event) => {
    updateTooltipPosition(event.clientX, event.clientY);
  });

  function updateTooltipPosition(x, y) {
    const offset = 10; // Adjust this value to control the distance from the cursor

    let leftPos = tooltip.style.left = x + offset;
    // If we get close to the right edge, flip the tooltip
    if ((window.innerWidth - x) < tooltip.offsetWidth ) {
      leftPos = x - tooltip.offsetWidth - offset;
    }
    tooltip.style.left = leftPos + 'px';

    tooltip.style.top = y + offset + window.scrollY - 50 + 'px';
  }

  // Show/hide tooltip example
  document.getElementById('campaign-gifts-pile').addEventListener('mouseover', () => {
    tooltip.classList.add('is-visible');
  });

  document.getElementById('campaign-gifts-pile').addEventListener('mouseout', () => {
    tooltip.classList.remove('is-visible');
  });
}
mouseSetup(engine, world);


function createPresentSimulationData(orders) {
  const totalPresents = orders.length;
  for (let p=0; p<totalPresents; p++) {
    // Create the gift in the data structure used by the custom renderer.
    // *All* gifts are added there, even if they're not simulated.
    let present = new PresentSimData();
    present.presentID = p;
    present.tierIdx = getTierIdx(orders[p][1], orders[p][2]);
    presentSimData.push(present);
  }
}

function createRuntimeSimulationData(orders, simWidth, simHeight) {
  addWalls(world, simWidth, simHeight);
  addPresents(world, simWidth, simHeight, orders);
}

function checkCanvasOccupancy(counts) {
  const totalBoxesArea = counts.reduce((result, v, i) => result + v * widthsPx[i] * heightsPx[i]);
  const totalGapsArea = totalBoxesArea * 0.25; // Add a factor for empty space in between the gifts.
  const totalRequestedArea = totalBoxesArea + totalGapsArea;
  const totalAvailableArea = WIDTH * (HEIGHT - 555); // Area under the text and donation box (measured to take 555px).
  const occupancyFactor = Math.sqrt(totalRequestedArea / totalAvailableArea);
  return occupancyFactor;
}

function gatherStats(orders) {
  // Gift purchases coming from the backend.
  const totalPresents = orders.length;
  let counts = Array(numSubsTiers + numOnceTiers).fill(0);
  for (let i=0; i < orders.length; i++) {
    const tierIdx = getTierIdx(orders[i][1], orders[i][2]);
    counts[tierIdx]++;
  }
  console.log("Present counts:", counts, " Total:", totalPresents);
  return counts;
}

function loadInitialPositions(simData) {
  for (let i = 0; i < simData.bodies.length; i++) {
    let body = simData.bodies[i];

    // Convert from matter.js (0,0) at top-left with y down and angles along x
    // to (0,0) at bottom-left with y up with angles along y.
    presentSimData[i].pos = [body.position.x * px_to_m, (HEIGHT - body.position.y) * px_to_m];
    presentSimData[i].ori = [Math.sin(body.angle), Math.cos(body.angle)];
  }
}

function updateProgressInfo(ordersCount) {
  const progressText = document.getElementById('progress-text');
  if (!progressText) {return}
  progressText.innerText = "Received " + ordersCount + " gifts so far!";

  const progressBar = document.getElementById('progress-bar');
  if (!progressBar) {return}
  let progressBarIndicator = document.createElement("span");
  progressBar.appendChild(progressBarIndicator);
  progressBarIndicator.style.width = (ordersCount / 100) + '%';
}

function teleportGiftToTop(body) {
    // Teleport the gift up, just above the screen, with a random rotation.
    let bodyHeight = body.bounds.max.y - body.bounds.min.y;
    Matter.Body.setPosition(body, { x: WIDTH * 0.3, y: -bodyHeight});
    Matter.Body.rotate(body, Math.random() * 20-10);
}

/**
 * Add a gift to the simulation (dropping from the top of the screen)
 * Call this using setTimeout(dropGift, ...);
 */
function dropGift(order, isOwn) {
  const tierIdx = getTierIdx(order[1], order[2]);
  let body = null;

  // Check if the gifts is already in the data structures or if we need to add a new one.
  if (order[0] === null && isOwn && ownOrderIdxs.length)
  {
    // Yes, gift already exists, teleport it up to drop.
    // Find the order in the existing arrays.
    const orderIdx = ownOrderIdxs[ownOrderIdxs.length - 1];
    order = orders[orderIdx];
    // Find the order in matterjs's prepended arrays.
    const physicsSimBodies = Matter.Composite.allBodies(world);
    if (3+orderIdx > physicsSimBodies.length || liveDroppedGifts!==0)
      console.error("Gift drop: indexing error for matterjs structure. liveDrop: ", liveDroppedGifts, "orderIdx: ", orderIdx);

    body = physicsSimBodies[3+orderIdx];
    if (Number(body.label) !== orderIdx)
      console.error("Gift drop: incorrect gift", body.label, physicsSimBodies);

    ownOrderIdxs.pop();
    setTimeout(()=> {
      ownOrderIdxs.push(orderIdx);
      teleportGiftToTop(body);
    }, 3000);

  } else {
    // Push a new gift onto the Matter.js simulation, the renderer and the orders.
    // Unfortunately, Matter.js prepends the object and there is no control over that.
    // The solution for now is to keep track of this offset so that updatePhysics() keeps things in order.
    liveDroppedGifts++;

    const orderIdx = orders.length;
    orders.push(order);

    // Set an ID using a big number, just to have something
    if (order[0] === null) {
      order[0] = orderIdx * 100;
    }

    let present = new PresentSimData();
    present.presentID = orderIdx;
    present.tierIdx = tierIdx;
    presentSimData.push(present);

    if (isOwn) {
      ownOrderIdxs.push(orderIdx);
    }

    const width = widthsPx[tierIdx];
    const height = heightsPx[tierIdx];
    body = Matter.Bodies.rectangle(WIDTH * 0.3, -height , width, height, {label: String(orderIdx)})
    Matter.World.add(world, body);

    teleportGiftToTop(body)
  }
}

async function loadData() {
  let ordersJSON = await fetchOrdersJSON();
  orders = ordersJSON['orders'];
  ownOrderIdxs = ordersJSON['ownOrderIndices'];

  const counts = gatherStats(orders);
  const occupancyFactor = checkCanvasOccupancy(counts);
  // Dynamically scale the simulation to fit the lower half of the screen.
  // e.g.: If there's too many boxes and they overflow the area they should be in (occupancyFactor = 2x),
  // Then, the simulation is run with 2x the width (invFitFactor = 2x), so that the gifts are spread wider.
  // The result is uniformly scaled to fit the view (fitFactor = 0.5x).
  // The gits's size is kept constant so that the canvas can be resized at runtime without affecting the simulation.
  const invFitFactor = occupancyFactor; // Simulation area is scaled by invFitFactor (e.g. '2x')
  fitFactor = 1 / invFitFactor; // View scales the simulation by fitFactor (e.g.: '0.5x').

  // Apply the transform to Matterjs' mouse queries and constraint.
  mouse.scale.x = invFitFactor;
  mouse.scale.y = invFitFactor;
  mouse.offset.y = HEIGHT - Math.round(HEIGHT / fitFactor);

  // Create data to be rendered.
  createPresentSimulationData(orders);

  // Uncomment to use live data
  createRuntimeSimulationData(orders, WIDTH * invFitFactor, HEIGHT);

  // Uncomment to use baked data
  // let offlineSimulationData = await fetchOfflineSimulationData();
  // loadInitialPositions(offlineSimulationData);

  updateProgressInfo(ordersJSON['ordersCount']);

  // Uncomment to debug dropGift
  // setTimeout(dropGift([8989898, 5000, 'r', 'Jane Doe']), 1000)
}

