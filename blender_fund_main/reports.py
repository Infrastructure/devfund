import json

from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Sum, Count, Case, When
from django.db.models.functions import TruncDay

from colorhash import ColorHash
from looper.money import Money
import looper.models


def _hex2rgb(value: str) -> tuple:
    value = value[1:]
    return (int(value[:2], 16), int(value[2:4], 16), int(value[4:], 16))


def row_color(p):
    return ColorHash(str(p)).rgb


class _BaseReport(object):
    _plan_colors_sorted = (
        ('Bronze', '#aaaaff'),
        ('Silver', '#8080ff'),
        ('Gold', '#2a2aff'),
        ('Platinum', '#0000d4'),
        ('Titanium', '#000080'),
        ('Diamond', '#000055'),
        ('Subtotal Individual sponsors', '#000055'),
        ('Corporate_Bronze', '#c8beb7'),
        ('Corporate_Silver', '#ac9d93'),
        ('Corporate_Gold', '#6c5d53'),
        ('Corporate_Platinum', '#d0e3ea'),
        ('Corporate_Titanium', '#363535'),
        ('Corporate_Patron', '#483e37'),
        ('Subtotal Corporate sponsors', '#0a0807'),
    )
    row_colors = {
        key: _hex2rgb(hex_color) for key, hex_color in _plan_colors_sorted
    }
    row_colors_cancelled = {
        key: _hex2rgb(f'{hex_color[:3]}00{hex_color[5:]}') for key, hex_color in _plan_colors_sorted
    }
    row_colors_stopped = {
        key: _hex2rgb(f'{hex_color[:3]}ff{hex_color[5:]}') for key, hex_color in _plan_colors_sorted
    }

    def row_title(self, r) -> str:
        if isinstance(r, str):
            title = r
        elif isinstance(r, looper.models.Product) and 'Donation' in r.name:
            title = 'One-time donations'
        else:
            title = f'{r}'
        return title

    def sort_kwargs(self):
        """Define a sort function for report lines."""
        sort_keys = []
        for subtotal_title, rows in self.subtotals:
            sort_keys.extend(rows)
            sort_keys.append(subtotal_title)

        def f_sort_keys(v) -> int:
            if getattr(v, 'cents', None):
                return v.cents - 99999999
            line_title = str(getattr(v, 'plan', v))
            if line_title in sort_keys:
                return sort_keys.index(line_title)
            return -1
        return f_sort_keys

    def get_row_color(self, row) -> str:
        """Allow overriding row colors, defaulting to a colour hash based on row label."""
        return self.row_colors.get(str(row), row_color(str(row)))


class RevenueReportQueries(_BaseReport):
    date_hierarchy = 'paid_at'

    def __init__(self, subtotals):
        self.subtotals = subtotals

    def _fill_missing_data(self, rows):
        """
        Fill in missing rows so that all tooltips have the same number of lines.

        Changes given rows in place.
        """
        _dates = set()
        for _row in rows.values():
            _dates.update([_['date'] for _ in _row])
        if not _dates:
            # Nothing to do: this dataset is empty
            return

        for key, _row in rows.items():
            _by_date = {_['date']: _ for _ in _row}
            for _date in sorted(_dates):
                if _date not in _by_date:
                    _by_date[_date] = {
                        'date': _date,
                        'gross_revenue': 0,
                        'orders_paid': 0,
                        'orders_refunded': 0,
                        'refunds': 0,
                        'taxes': 0,
                        'y': 0,
                    }
            rows[key] = list(_by_date.values())

    def aggregate_revenue(self, queryset):
        """Aggregate and annotate a given queryset with values needed for displaying the revenue."""
        return queryset.annotate(
            date=TruncDay(self.date_hierarchy)
        ).values('date').annotate(
            y=Sum('price') / 100,
            gross_revenue=Sum('price'),
            refunds=Sum('refunded'),
            taxes=Sum('tax'),
            orders_paid=Count('id'),
            orders_refunded=Count(Case(When(refunded__gt=0, then=1))),
        ).order_by('-date')

    def get_aggregated_rows(self, queryset):
        plans = looper.models.Plan.objects.all()
        rows = {
            p: list(self.aggregate_revenue(
                queryset.filter(subscription__plan_id=p.id)
            ))
            for p in plans
        }
        products = {p.pk: p for p in looper.models.Product.objects.filter(plan__isnull=True)}
        product_orders_q = queryset.filter(subscription__isnull=True)
        rows.update({
            products[product_id]: list(self.aggregate_revenue(
                product_orders_q.filter(product=product_id)
            ))
            for product_id in products
        })
        # Subscription orders that don't appear to belong to any existing plans:
        other_queryset = queryset
        for p in plans:
            other_queryset = other_queryset.exclude(subscription__plan_id=p.id)
        for product_id in products:
            other_queryset = other_queryset.exclude(subscription__isnull=True, product=product_id)
        if other_queryset.exists():
            rows['Other'] = list(self.aggregate_revenue(other_queryset))

        self._fill_missing_data(rows)
        return rows

    def get_aggregated_rows_one_time_amounts(self, queryset, currency):
        products = looper.models.Product.objects.filter(plan__isnull=True)
        product_orders_q = queryset.filter(subscription__isnull=True, product__isnull=False)
        buckets = [
            (0, 10_00),
            (10_00, 25_00),
            (25_00, 50_00),
            (50_00, 100_00),
            (100_00, 250_00),
            (250_00, 500_00),
            (500_00, 999999999),
        ]
        rows = {}
        for product in products.all():
            rows.update({
                (
                    f'{Money(currency, more_than).just_whole}'
                    f'-{Money(currency, less_than).just_whole}'
                ): list(
                    self.aggregate_revenue(
                        product_orders_q.filter(
                            product=product.pk, price__gte=more_than, price__lt=less_than
                        )
                    )
                )
                for more_than, less_than in buckets
            })
        self._fill_missing_data(rows)
        return rows

    def get_chart(self, rows, currency):
        color_rows = {p: self.get_row_color(p) for p in rows}
        color_rows['Other'] = (0, 0, 0)

        chart_data = [
            {
                'data': rows[pv],
                'label': self.row_title(pv),
                'backgroundColor': f'rgba{color_rows[pv]}',
            } for pv in sorted(rows.keys(), key=self.sort_kwargs(), reverse=True)
        ]
        # For purposes of presentation only, show amounts in EUR by default
        currency = currency or 'EUR'
        return {
            'datasets': json.dumps(chart_data, cls=DjangoJSONEncoder),
            'aggregate_by': self.date_hierarchy,
            'aggregate_by_prefix': Money(currency, 0).currency_symbol,
        }

    def get_subtotals(self, rows, currency):
        subtotals = []
        flattened_rows = [item for items in rows.values() for item in items]
        subtotals.append(dict(
            gross_revenue=Money(
                currency,
                sum(_['gross_revenue'] for _ in flattened_rows),
            ),
            refunds=Money(
                currency,
                sum(_['refunds'] for _ in flattened_rows),
            ),
            taxes=Money(
                currency,
                sum(_['taxes'] for _ in flattened_rows),
            ),
            orders_paid=sum(_['orders_paid'] for _ in flattened_rows),
            orders_refunded=sum(_['orders_refunded'] for _ in flattened_rows),
        ))
        return subtotals


class StartedCancelledExpiredReportQueries(_BaseReport):  # TODO
    def __init__(self, date_after, date_before):
        self.date_after = date_after
        self.date_before = date_before

    def aggregate_by(self, queryset, date_field, negative=False):
        """Annotate a given Subscription queryset with counts per day."""
        _count = - Count('id') if negative else Count('id')
        return queryset.annotate(
            date=TruncDay(date_field)
        ).values('date').annotate(
            y=_count,
        ).order_by('-date')

    def get_aggregated_rows(self, queryset):
        plans = list(looper.models.Plan.objects.all())
        rows = {}
        rows.update({
            ('started', plan): list(
                self.aggregate_by(
                    queryset.filter(
                        plan_id=plan.pk,
                        started_at__gte=self.date_after,
                        started_at__lt=self.date_before,
                    ),
                    'started_at',
                )
            )
            for plan in plans
        })
        rows.update({
            ('cancelled', plan): list(
                self.aggregate_by(
                    queryset.filter(
                        plan_id=plan.pk,
                        # FIXME: what about the ones that were cancelled immediately
                        # because they were on-hold?
                        pending_cancellation_since__gte=self.date_after,
                        pending_cancellation_since__lt=self.date_before,
                    ),
                    'pending_cancellation_since',
                    negative=True,
                )
            )
            for plan in plans
        })
        rows.update({
            ('stopped', plan): list(
                self.aggregate_by(
                    queryset.filter(
                        plan_id=plan.pk,
                        next_payment__gte=self.date_after,
                        next_payment__lt=self.date_before,
                        status='on-hold',
                        intervals_elapsed__gte=1,
                    ),
                    'next_payment',
                    negative=True,
                )
            )
            for plan in plans
        })
        return rows

    def get_chart(self, queryset):
        rows = self.get_aggregated_rows(queryset)
        color_rows = {p: self.get_row_color(p) for p in rows}
        chart_data = [{
            'data': rows[p],
            'label': self.row_title(f'{p[0].capitalize()} {p[1]}'),
            'backgroundColor': f'rgba{color_rows[p]}',
        } for p in sorted(rows.keys(), key=self.sort_kwargs())]
        return {
            'datasets': json.dumps(chart_data, cls=DjangoJSONEncoder),
            'no_legend': True,
        }

    def sort_kwargs(self):
        """Define a sort function for report lines."""
        sort_keys = [plan_name for plan_name, _ in self._plan_colors_sorted]

        def f_sort_keys(v) -> int:
            event, plan = v
            idx = plan.name in sort_keys and sort_keys.index(plan.name) or -1
            if plan in sort_keys:
                return 100 - idx
            if event == 'cancelled':
                return - (idx + 100)
            if event == 'stopped':
                return - (idx + 200)
            return idx
        return f_sort_keys

    def get_row_color(self, p):
        event, plan = p
        fallback_color = row_color(p)
        if event == 'started':
            return self.row_colors.get(plan.name, fallback_color)
        if event == 'cancelled':
            return self.row_colors_cancelled.get(plan.name, fallback_color)
        if event == 'cancelled':
            return self.row_colors_stopped.get(plan.name, fallback_color)
        return fallback_color
