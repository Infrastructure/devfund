import typing

from html.parser import HTMLParser
import re

from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse


def absolute_url(viewname: str,
                 args: typing.Optional[tuple] = None,
                 kwargs: typing.Optional[dict] = None) -> str:
    """Same as django.urls.reverse() but then as absolute URL.

    For simplicity this assumes HTTPS is used, unless in DEBUG mode.
    """
    from urllib.parse import urljoin

    proto = 'http' if settings.DEBUG else 'https'
    domain = get_current_site(None).domain
    relative_url = reverse(viewname, args=args, kwargs=kwargs)
    return urljoin(f'{proto}://{domain}/', relative_url)


def is_noreply(email: str) -> bool:
    """Return True if the email address is a no-reply address."""
    return email.startswith('noreply@') or email.startswith('no-reply@')


class HTMLFilter(HTMLParser):
    skip_text_of = ('a', 'style')
    text = ''
    skip_tag_text = False

    def handle_starttag(self, tag, attrs):
        if tag in self.skip_text_of:
            self.skip_tag_text = True
        for name, value in attrs:
            if name == 'href':
                self.skip_tag_text = True
                self.text += value
        if tag in ('quote', 'q'):
            self.text += '“'
        if tag in ('h1', 'h2', 'h3', 'h4'):
            self.text += '\n\n'
        if tag in ('td',):
            self.text += ' '

    def handle_endtag(self, tag):
        if tag in self.skip_text_of:
            self.skip_tag_text = False
        if tag in ('quote', 'q'):
            self.text += '”\n\n'
        if tag in ('tr',):
            self.text += '\n'
        if tag in ('h1', 'h2', 'h3', 'h4'):
            self.text += '\n\n'

    def handle_data(self, data):
        if self.skip_tag_text:
            return
        self.text += data


def html_to_text(data: str) -> str:
    f = HTMLFilter()
    f.feed(data)
    lines = [_.lstrip(' \t') for _ in f.text.split('\n')]
    skip_empty = 0
    for line in lines:
        if not re.match(r'^\s*$', line):
            break
        skip_empty += 1
    return '\n'.join(lines[skip_empty:]).strip()
