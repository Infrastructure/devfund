# Generated by Django 3.2.9 on 2023-10-23 14:34

import blender_fund_main.admin
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('looper', '0078_move_customer_fields_to_address'),
        ('blender_fund_main', '0029_load_flatpages'),
    ]

    operations = [
        migrations.CreateModel(
            name='AutomaticPaymentEmailPreview',
            fields=[
            ],
            options={
                'verbose_name': '[Emails previews] Payment notification',
                'managed': False,
                'proxy': True,
            },
            bases=(blender_fund_main.admin._RenderEmailPreview, 'looper.order'),
        ),
        migrations.CreateModel(
            name='MembershipChangedEmailPreview',
            fields=[
            ],
            options={
                'verbose_name': '[Emails previews] Membership notification',
                'managed': False,
                'proxy': True,
            },
            bases=(blender_fund_main.admin._RenderEmailPreview, 'blender_fund_main.membership'),
        ),
    ]
