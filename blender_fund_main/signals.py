import logging
import typing

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import transaction
from django.dispatch import receiver
import django.db.models.signals as django_signals

from blender_id_oauth_client import signals as bid_signals

import looper.signals
import looper.models
from . import models

import blender_fund_main.tasks as tasks


User = get_user_model()
log = logging.getLogger(__name__)

# The signal's sender is the membership itself.
membership_activated = django_signals.Signal()  # Providing args: ['old_status']
membership_deactivated = django_signals.Signal()  # Providing args: ['old_status']
membership_cancelled = django_signals.Signal()
# Sent when a membership is created in an inactive state because it still needs payment.
# This happens when the payment method doesn't support transactions/charges (e.g. bank).
membership_created_needs_payment = django_signals.Signal()


@receiver(django_signals.post_save, sender=looper.models.Subscription)
def handle_created_subscription(sender, instance: looper.models.Subscription, **kwargs):
    """Create a new Membership associated with the Subscription.

    Note that this does not yet activate the membership. This is done
    in response to the `looper.signals.subscription_activated signal`.
    """
    if kwargs.get('raw'):
        return

    if not kwargs.get('created'):
        return

    subscription = instance
    if hasattr(subscription, 'membership'):
        log.info(
            'Not creating membership for customer %r subscription %r; membership already attached',
            subscription.customer,
            subscription,
        )
        return

    # TODO handle case when there is no matching MembershipLevel and Plan Name
    # Now we have a try except because when running tests in Looper we do not create
    # MembershipLevel. In the blender_fund project this should never fail.
    try:
        membership_level = models.MembershipLevel.objects.get(plan=subscription.plan)
    except models.MembershipLevel.DoesNotExist:
        log.warning('MembershipLevel %r does not exist' % subscription.plan.name)
        return

    log.info('Creating membership for customer %r subscription %r',
             subscription.customer, subscription)
    m = models.Membership(
        customer=subscription.customer,
        display_name=subscription.customer.billing_address.full_name,
        level=membership_level,
        subscription=subscription,
    )
    m.save()

    payment_method = subscription.payment_method
    if not payment_method:
        return
    if not payment_method.gateway.provider.supports_transactions:
        # Without an action from the user, this membership is never going to get paid.
        membership_created_needs_payment.send(sender=m)


@receiver(django_signals.post_save, sender=looper.models.Subscription)
def handle_managed_flag(sender, instance: looper.models.Subscription, **kwargs):
    if kwargs.get('raw'):
        return

    membership: typing.Optional[models.Membership] = getattr(instance, 'membership', None)
    if not membership:
        return

    is_managed = instance.collection_method == 'managed'
    if membership.is_managed == is_managed:
        return

    membership.is_managed = is_managed
    if membership.pk:
        log.debug('Updating is_managed flag for membership %r due to save of subscription %r',
                  membership.pk, instance.pk)
        membership.save(update_fields={'is_managed'})


@receiver(looper.signals.subscription_activated)
def handle_activated_subscription(sender: looper.models.Subscription, **kwargs):
    """Activate Membership associated with the activated Subscription."""

    # Iterate over each Membership explicitly to ensure that the appropriate
    # save() methods are called and signals are sent.
    for membership in models.Membership.objects.filter(subscription=sender):
        membership.status = 'active'
        membership.save(update_fields={'status'})


@receiver(looper.signals.subscription_deactivated)
def handle_deactivated_subscription(sender: looper.models.Subscription, **kwargs):
    """Deactivate Membership associated with the Subscription."""

    # Iterate over each Membership explicitly to ensure that the appropriate
    # save() methods are called and signals are sent.
    for membership in models.Membership.objects.filter(subscription=sender):
        membership.status = 'inactive'
        membership.save(update_fields={'status'})


@receiver(bid_signals.user_created)
def set_customer_fullname(sender, instance: User, oauth_info: dict, **kwargs):
    """Create a Customer when a new User is created via OAuth."""
    if kwargs.get('raw'):
        return

    my_log = log.getChild('set_customer_fullname')
    if not instance.customer:
        my_log.error(
            'NOT updating user full_name of user %s, as they have no Customer', instance.pk
        )
        return

    my_log.info('Updating billing_address full_name as result of OAuth login of %s', instance.pk)
    billing_address = instance.customer.billing_address
    billing_address.full_name = oauth_info['full_name']
    billing_address.save()


@receiver(django_signals.post_save, sender=User)
def create_customer(sender, instance: User, created, raw, **kwargs):
    if kwargs.get('raw'):
        return

    from looper.models import Customer

    if raw:
        return

    if not created:
        return

    my_log = log.getChild('create_customer')
    try:
        customer = instance.customer
    except Customer.DoesNotExist:
        pass
    else:
        my_log.debug(
            'Newly created User %d already has a Customer %d, not creating new one',
            instance.pk,
            customer.pk,
        )
        billing_address = customer.billing_address
        if not billing_address.pk:
            my_log.info('Creating new billing address due to creation of user %s', instance.pk)
            billing_address.email = instance.email
            billing_address.save()
        return

    my_log.info('Creating new Customer due to creation of user %s', instance.pk)
    with transaction.atomic():
        customer = Customer.objects.create(user=instance)
        billing_address = customer.billing_address
        billing_address.email = instance.email
        billing_address.save()

# TODO(Sybren): respond to M2M change on Membership, when assigning membership to another user.


@receiver(membership_activated)
def generate_activity_membership_activated(sender: models.Membership, **kwargs):
    models.Activity.objects.create(
        category=models.Activity.Categories.MEMBERSHIP_START,
        membership=sender,
    )


@receiver(looper.signals.automatic_payment_succesful)
def generate_activity_automatic_renewal(sender: looper.models.Order, **kwargs):
    if sender.subscription.intervals_elapsed == 0:
        return
    if 'display_name' not in sender.subscription.membership.level.visible_attributes:
        return
    models.Activity.objects.create(
        category=models.Activity.Categories.MEMBERSHIP_RENEW,
        membership=sender.subscription.membership,
    )


@receiver(django_signals.post_save, sender=looper.models.Transaction)
def create_donation(sender, instance: looper.models.Transaction, **kwargs):
    """Create a Donation when a paid Transaction linked to one-time Order is saved."""
    if kwargs.get('raw'):
        return

    if not instance.paid:
        return
    order = instance.order
    # Doesn't look like a donation order, skipping
    if order.subscription_id or not order.product:
        return
    # Donation was already created for this order
    if hasattr(order, 'donation'):
        return
    with transaction.atomic():
        donation = models.Donation.objects.create(order=order)
        models.Activity.objects.create(
            donation=donation,
            title=donation.activity_text,
            category=models.Activity.Categories.DONATION,
        )


@receiver(django_signals.post_save, sender=looper.models.Order)
def _add_to_active_campaigns(sender, instance: looper.models.Order, **kwargs):
    if kwargs.get('raw'):
        return

    order = instance
    if order.status != 'paid' or not order.paid_at:
        return

    campaigns_q = models.Campaign.objects.filter(
        is_active=True, starts_at__lte=order.paid_at, ends_at__gt=order.paid_at
    )
    if not campaigns_q.exists():
        return

    if (
        order.subscription_id
        and hasattr(order.subscription, 'membership')
        and order.subscription.membership.level.category == 'CORP'
    ):
        # Skip corporate orders
        return

    with transaction.atomic():
        for campaign in campaigns_q:
            log.info('Adding order pk=%s to campaign %s', order.pk, campaign)
            campaign.orders.add(order)


@receiver(django_signals.m2m_changed, sender=models.CampaignsOrders)
def _grant_campaign_badges(sender, instance: models.Campaign, **kwargs):
    if kwargs.get('raw'):
        return

    action = kwargs.get('action')
    pk_set = kwargs.get('pk_set')
    if action != 'post_add' or not pk_set:
        return
    instance.grant_badges(order_pks=pk_set)


@receiver(membership_activated)
@receiver(membership_cancelled)
@receiver(membership_created_needs_payment)
def _on_membership_status_changed(sender: models.Membership, **kwargs):
    tasks.send_mail_membership_status_changed(membership_id=sender.pk)


@receiver(looper.signals.automatic_payment_succesful)
@receiver(looper.signals.automatic_payment_soft_failed)
@receiver(looper.signals.automatic_payment_failed)
def _on_automatic_payment_performed(
    sender: looper.models.Order,
    transaction: looper.models.Transaction,
    **kwargs,
):
    tasks.send_mail_automatic_payment_performed(order_id=sender.pk, transaction_id=transaction.pk)


@receiver(looper.signals.managed_subscription_notification)
def _on_managed_subscription_notification(sender: looper.models.Subscription, **kwargs):
    tasks.send_mail_managed_subscription_notification(subscription_id=sender.pk)
