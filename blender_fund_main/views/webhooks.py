"""Defines webhook views."""
import logging

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
import stripe

from looper.models import Gateway
from blender_fund_main.tasks import handle_payment_intent_succeeded, handle_setup_intent_succeeded

logger = logging.getLogger(__name__)


@method_decorator(csrf_exempt, name='dispatch')
class StripeWebhookView(View):
    """Handle Stripe events arriving via a webhook."""

    def post(self, request, *args, **kwargs):
        """Handle Stripe webhook request: only POST is expected here."""
        if 'HTTP_STRIPE_SIGNATURE' not in request.META:
            logger.error('Missing signature')
            return HttpResponse(status=401)

        gateway = Gateway.objects.get(name='stripe')
        try:
            event = gateway.provider.construct_event_from_request(request)
        except ValueError as e:
            logger.error('Error parsing payload: {}'.format(str(e)))
            return HttpResponse(status=400)
        except stripe.error.SignatureVerificationError as e:
            logger.error('Error verifying webhook signature: {}'.format(str(e)))
            return HttpResponse(status=400)

        logger.info(
            'Processing event type=%s for object_id=%s',
            event.type, event.data.object.id
        )
        if event.type == 'payment_intent.succeeded':
            handle_payment_intent_succeeded(event)
        elif event.type == 'setup_intent.succeeded':
            handle_setup_intent_succeeded(event)
        else:
            logger.info('Unhandled event type {}'.format(event.type))

        return HttpResponse(status=200)
