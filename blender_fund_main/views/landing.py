import logging
import typing
import random
from collections import defaultdict

from cachetools import cached, TTLCache
from django.http import JsonResponse
from django.shortcuts import render
from django.db import connection
from django.db.models import Count, Q

import looper.context_processors
import looper.middleware
import looper.models
from looper.money import Money
import looper.money
import waffle

from blender_fund_main import models

# Amount of money made for each membership level category.
MoneyPerCategory = typing.Mapping[str, Money]

log = logging.getLogger(__name__)


def get_total_income_in_euro() -> MoneyPerCategory:
    """Compute the total income of the Blender Foundation.

    :return: The total income, on a yearly/12 basis, converted to Euros
    """

    # If there is no subscription, we don't know how much money
    # we're getting, so we can safely ignore those.
    prices = models.Membership.objects.active().exclude(
        subscription__isnull=True
    ).select_related(
        'subscription'
    ).values_list(
        'subscription__currency',
        'subscription__interval_length',
        'subscription__interval_unit',
        'subscription__price',
    )
    sum_by_currency = defaultdict(int)
    for (currency, interval_length, interval_unit, price) in prices:
        sum_by_currency[currency] += int(
            price / (interval_length * (12 if interval_unit == 'year' else 1))
        )
    return looper.money.sum_to_euros(
        Money(currency=currency, cents=sum) for currency, sum in sum_by_currency.items()
    )


@cached(cache=TTLCache(maxsize=2, ttl=600))
def total_income_in_currency(currency) -> MoneyPerCategory:
    """Convert the total Foundation income to the given currency."""

    total_in_eur = get_total_income_in_euro()
    return looper.money.convert_currency(total_in_eur, to_currency=currency)


def get_context(request):
    query = models.Membership.objects.visible().exclude(
        level__category='CORP'
    ).exclude(
        display_name=''
    ).exclude(
        display_name__isnull=True
    ).select_related('level').all()
    memberships = defaultdict(list)
    for result in query:
        result.limit_fields()
        memberships[result.level.name].append(result)
    for mems in memberships.values():
        random.shuffle(mems)

    currency = looper.context_processors.preferred_currency_name(request)

    query = (
        models.MembershipLevel.objects.values('category')
        .annotate(membership__count=Count('membership', filter=Q(membership__status='active')))
        .order_by('category')
    )
    memberships_per_category = {item['category']: item['membership__count'] for item in query}

    activity = models.Activity.objects.select_related('membership').select_related(
        'membership__level'
    ).all()[:5]
    # Add extra variables and return the updated context

    summed_income =  { "currency_symbol": "€", "just_whole": 10000 }
    mpc = { "INDIV": 1000, "CORP": 100 }

    mems = {
        "Diamond": [
            {
                "url": "https://shopify.com",
                "display_name": "Shopify",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/SHOP-020ea41a.png",
                },
            },
            {
                "url": "https://gnome.org",
                "display_name": "Gnome",
                "logo": {
                    "url": "https://brand.gnome.org/logos/gnome_horizontal_lockup.png",
                },
            },
            {
                "url": "https://google.com",
                "display_name": "Google",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/google_BIG-532a9d4b.png",
                },
            },
            {
                "url": "https://microsoft.com",
                "display_name": "Microsoft",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/MSFT-a203b22d.png",
                },
            },
            {
                "url": "https://box.com",
                "display_name": "Box",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/BOX-f6f9d385.png",
                },
            },
            {
                "url": "https://meta.com",
                "display_name": "Meta",
                "logo": {
                    "url": "https://freepnglogo.com/image/meta-logo-png",
                },
            }
        ],
        "Titanium": [
            {
                "url": "https://shopify.com",
                "display_name": "Shopify",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/SHOP-020ea41a.png",
                },
            },
            {
                "url": "https://gnome.org",
                "display_name": "Gnome",
                "logo": {
                    "url": "https://brand.gnome.org/logos/gnome_horizontal_lockup.png",
                },
            },
            {
                "url": "https://google.com",
                "display_name": "Google",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/google_BIG-532a9d4b.png",
                },
            },
            {
                "url": "https://microsoft.com",
                "display_name": "Microsoft",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/MSFT-a203b22d.png",
                },
            },
            {
                "url": "https://box.com",
                "display_name": "Box",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/BOX-f6f9d385.png",
                },
            },
            {
                "url": "https://meta.com",
                "display_name": "Meta",
                "logo": {
                    "url": "https://freepnglogo.com/image/meta-logo-png",
                },
            }
        ],
        "Platinum": [
            {
                "url": "https://shopify.com",
                "display_name": "Shopify",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/SHOP-020ea41a.png",
                },
            },
            {
                "url": "https://gnome.org",
                "display_name": "Gnome",
                "logo": {
                    "url": "https://brand.gnome.org/logos/gnome_horizontal_lockup.png",
                },
            },
            {
                "url": "https://google.com",
                "display_name": "Google",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/google_BIG-532a9d4b.png",
                },
            },
            {
                "url": "https://microsoft.com",
                "display_name": "Microsoft",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/MSFT-a203b22d.png",
                },
            },
            {
                "url": "https://box.com",
                "display_name": "Box",
                "logo": {
                    "url": "https://companieslogo.com/img/orig/BOX-f6f9d385.png",
                },
            },
            {
                "url": "https://meta.com",
                "display_name": "Meta",
                "logo": {
                    "url": "https://freepnglogo.com/image/meta-logo-png",
                },
            }
        ],
        "Gold": [
            {
                "logo": {
                    "url": "https://companieslogo.com/img/orig/SHOP-020ea41a.png",
                },
            },
            {
                "logo": {
                    "url": "https://brand.gnome.org/logos/gnome_horizontal_lockup.png",
                },
            },
            {
                "logo": {
                    "url": "https://companieslogo.com/img/orig/google_BIG-532a9d4b.png",
                },
            },
            {
                "logo": {
                    "url": "https://companieslogo.com/img/orig/MSFT-a203b22d.png",
                },
            },
            {
                "logo": {
                    "url": "https://companieslogo.com/img/orig/BOX-f6f9d385.png",
                },
            },
            {
                "logo": {
                    "url": "https://freepnglogo.com/image/meta-logo-png",
                },
            }
        ],
        "Silver": [
            {
                "display_name": "Shopify",
            },
            {
                "display_name": "Microsoft",
            },
            {
                "display_name": "Gnome",
            },
            {
                "display_name": "Box",
            },
            {
                "display_name": "Google",
            },
            {
                "display_name": "Meta",
            }
        ],
        "Bronze": [
            {
                "display_name": "Shopify",
            },
            {
                "display_name": "Gnome",
            },
            {
                "display_name": "Box",
            },
            {
                "display_name": "Microsoft",
            },
            {
                "display_name": "Google",
            },
            {
                "display_name": "Meta",
            }
        ]
    }

    return {
        'activity': activity,
        'memberships_corporate': models.MembershipLevel.objects.filter(
            category='CORP'
        ).prefetch_related('membership_set').order_by('-order'),
        'memberships': mems, #memberships,
        'memberships_per_category': mpc, #memberships_per_category,
        'memberships_total_count': 10000 ,#sum(memberships_per_category.values()),
        'summed_income': summed_income, #total_income_in_currency(currency),
    }


def landing(request):
    if waffle.flag_is_active(request, 'campaign'):
        return landing_campaign(request)
    context = get_context(request)
    return render(request, 'blender_fund/landing.html', context)


def landing_campaign(request):
    return render(request, 'blender_fund_main/landing_campaign.html', {})


def credits(request):
    context = get_context(request)
    return render(request, 'blender_fund_main/credits.html', context)


def get_orders_json_synthetic(request):
    # Randomly generate X presents given a rough probability of purchases per subscription value.
    orders = []

    # Tier values and probabilities were obtained empyrically prior to the campaign start.
    subscription_tiers = [5, 10, 25, 50, 100, 250]
    tier_probs = [0.66917, 0.18233, 0.12030, 0.01504, 0.00752, 0.00564]

    # Split between once donations and subscriptions/donations. Fabricated (non empyrical) values.
    type_probs = [0.5, 0.1, 0.4]  # r, s, d

    # Generate X gift orders.
    for i in range(10000):
        order_type = random.choices(['r', 's', 'd'], weights=type_probs)[0]
        if order_type == 'd':
            order_amount = random.choices(
                [2, 5, 10, 15, 20, 25, 33, 50],  # Random values in euros according to prob below.
                [0.05, 0.4, 0.35, 0.05, 0.1, 0.02, 0.005, 0.075])[0]
        else:
            random_value = random.uniform(0, 1)
            tier_index = 0
            tier_prob = tier_probs[tier_index]
            while random_value > tier_prob:
                tier_index += 1
                tier_prob += tier_probs[tier_index]
            order_amount = subscription_tiers[tier_index]

        order_amount *= 100 # Convert to cents.
        orders.append([
            i,            # id (int)
            order_amount, # amount (200)
            order_type,   # type (d/r/s) donated/renewed/subscribed
            '',           # name ('' for annoymous or 'Full Name')
            'EUR'         # currency (EUR, USD)
        ])

    # Add a couple of singular big orders.
    orders.append([len(orders), 5000*100, 'd', '', 'EUR'])
    orders.append([len(orders),  500*100, 'd', '', 'EUR'])
    orders.append([len(orders),  100*100, 'd', '', 'EUR'])
    return JsonResponse({'orders': orders, 'ownOrderIndices': [0], 'ordersCount': len(orders)})


def get_orders_json_raw(request):
    # For development use generated gift amounts instead of real data.
    # return get_orders_json_synthetic(request)

    orders = []
    with connection.cursor() as cursor:
        cursor.execute("""
            select
                co.order_id,
                coalesce(s.price_in_cents, o.price_in_cents),
                s.interval_length,
                s.interval_unit,
                o.currency,
                m.display_name as m_display_name,
                m.is_private,
                d.display_name as d_display_name,
                case when s.id is not null then o.created_at - s.created_at > interval '7 days'
                     else false end as s_renewed
            from blender_fund_main_campaignsorders co
            join looper_order o on o.id = co.order_id
            left join looper_subscription s on s.id = o.subscription_id
            left join blender_fund_main_membership m on m.subscription_id = s.id
            left join blender_fund_main_donation d on d.order_id = co.order_id
            where co.campaign_id = 1
            and m.level_id < 7 or m.level_id is null
            order by co.order_id
        """)
        for row in cursor.fetchall():
            id, price, interval_length, interval_unit, currency, m_name, m_private, d_name, s_renewed = row
            if interval_length is not None and interval_unit is not None:
                price = int(price / (interval_length * (12 if interval_unit == 'year' else 1)))
            name = ''
            if d_name is not None:
                name = d_name
            elif m_name is not None and m_private is False:
                name = m_name
            drs = 'd'
            if interval_length is not None:
                drs = 's'
                if s_renewed:
                    drs = 'r'
            orders.append(
                [
                    id,
                    price,
                    drs,
                    name,
                    currency,
                ]
            )

    return JsonResponse(
        {
            'orders': orders,
            'ownOrderIndices': _get_own_order_indices(request, orders),
            'ordersCount': len(orders),
        }
    )


def _get_last_order_id(request):
    stripe_session_id = request.session.get('stripe_session_id')
    last_order_id = request.session.get('last_order_id')
    if stripe_session_id and not last_order_id:
        if not last_order_id:
            import stripe
            session = stripe.checkout.Session.retrieve(stripe_session_id)
            gateway = looper.models.Gateway.objects.get(name='stripe')
            gatewayorder = looper.models.GatewayOrderId.objects.filter(
                gateway=gateway,
                gateway_order_id=session.payment_intent,
            ).first()
            if gatewayorder:
                last_order_id = gatewayorder.order_id
                request.session['last_order_id'] = last_order_id
    return last_order_id


def _get_own_order_indices(request, orders):
    own_order_indices = []
    last_order_id = None
    try:
        last_order_id = _get_last_order_id(request)
    except Exception:
        log.exception('failed to get last_order_id')

    if last_order_id:
        try:
            own_order_indices.append([order[0] for order in orders].index(last_order_id))
        except ValueError:
            pass

    if request.user.is_authenticated:
        last_order = looper.models.Order.objects.filter(
            customer=request.user.customer,
            status='paid',
        ).order_by('-id').first()
        if last_order:
            last_order_id = last_order.pk
            try:
                own_order_indices.append([order[0] for order in orders].index(last_order_id))
            except ValueError:
                pass

    return own_order_indices
