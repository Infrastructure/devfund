from datetime import datetime, timedelta
import urllib.parse

from django.core.exceptions import ValidationError
from django.db.models import Q
from django.forms import Form, fields
from django.shortcuts import redirect, reverse
from django.utils import timezone
from django.views.generic import ListView, TemplateView

from blender_fund_main.models import Activity
from blender_fund_main.reports import RevenueReportQueries, StartedCancelledExpiredReportQueries, _hex2rgb
import blender_fund_main.admin as admin
import looper.models


class ActivityListView(ListView):
    paginate_by = 25
    model = Activity


class ReportsView(TemplateView):
    template_name = 'blender_fund_main/reports.html'
    default_range_days = 30
    model = looper.models.Order
    donation_row_colors = {
        key: _hex2rgb(hex_color) for key, hex_color in (
            ('0-10', '#ccff00'),
            ('10-25', '#00ff00'),
            ('25-50', '#00d400'),
            ('50-100', '#00aa00'),
            ('100-250', '#005500'),
            ('250-500', '#002b00'),
            ('500-9999999', '#FFA500'),
        )
    }

    class FilterParamsForm(Form):
        currency = fields.ChoiceField(choices=(('EUR', 'EUR'), ('USD', 'USD')), required=False)
        # **N.B.**: Always [date_after, date_before)
        date_after = fields.DateField(required=True)
        date_before = fields.DateField(required=True)

        def _date_to_timezone_aware_midnight(self, date_field):
            min_time = datetime.min.time()
            d = self.cleaned_data[date_field]
            return datetime.combine(d, min_time).replace(tzinfo=timezone.utc)

    def _get_initial(self):
        _now = timezone.now()
        two_weeks_ago = _now - timedelta(days=self.default_range_days)
        return {
            'date_after': two_weeks_ago,
            'date_before': _now,
        }

    def get(self, *args, **kwargs):
        self.initial = self._get_initial()
        try:
            return super().get(*args, **kwargs)
        except ValidationError:
            default_params = self.FilterParamsForm(data=self.initial)
            assert default_params.is_valid()
            query_string = urllib.parse.urlencode(default_params.cleaned_data)
            return redirect(reverse('activity-reports') + f'?{query_string}')

    def get_context_data(self, *args, **kwargs):
        form = self.FilterParamsForm(
            data=dict(self.request.GET.items()),
            initial=self.initial,
        )
        if not form.is_valid():
            raise ValidationError(form.errors)

        currency = form.cleaned_data.get('currency')
        date_after = form._date_to_timezone_aware_midnight('date_after')
        date_before = form._date_to_timezone_aware_midnight('date_before') + timedelta(days=1)
        filters = Q(
            status='paid',
            paid_at__isnull=False,
            paid_at__gte=date_after,
            paid_at__lt=date_before,
            currency__in=('EUR', 'USD'),
        )
        if currency:
            filters = filters & Q(currency__exact=currency)
        q = self.model.objects.filter(filters).select_related(
            'product',
            'product__plan',
            'subscription',
            'subscription__plan',
        )
        report = RevenueReportQueries(subtotals=admin.FundRevenuePerPlanAndProductAdmin.subtotals)
        rows = report.get_aggregated_rows(q)
        chart = report.get_chart(rows, currency)
        chart['canvas_id'] = 'chart-1'
        subtotals = report.get_subtotals(rows, currency)

        one_time_per_sum_rows = report.get_aggregated_rows_one_time_amounts(q, currency)
        report.row_colors = self.donation_row_colors
        one_time_chart = report.get_chart(one_time_per_sum_rows, currency)
        one_time_chart['canvas_id'] = 'chart-2'
        one_time_subtotals = report.get_subtotals(one_time_per_sum_rows, currency)

        subscription_report = StartedCancelledExpiredReportQueries(date_after, date_before)
        subscription_q = looper.models.Subscription.objects
        subscription_chart = subscription_report.get_chart(subscription_q)
        subscription_chart['canvas_id'] = 'chart-3'

        context = super().get_context_data(*args, **kwargs)
        context['chart'] = chart
        context['chart_one_time'] = one_time_chart
        context['subscription_chart'] = subscription_chart
        context['subtotals'] = subtotals
        context['subtotals_one_time'] = one_time_subtotals
        context['form'] = form
        context['today'] = self.initial['date_before'].date
        context['yesterday'] = (self.initial['date_before'] - timedelta(days=1)).date
        return context
