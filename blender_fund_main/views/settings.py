import logging
import pathlib
from typing import Optional, Set

import django.forms
import django.utils.timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import FormView, TemplateView, View

import looper.models
from looper.money import Money
from looper.views.mixins import ExpectReadableIPAddressMixin
import looper.exceptions
import looper.stripe_utils
import looper.utils
import looper.views.checkout

from ..models import Membership
from .. import forms

log = logging.getLogger(__name__)

RECHARGEABLE_SUBS_STATUSES = {'active', 'on-hold'}


@login_required
def settings_home(request):
    mems = request.user.customer.memberships.select_related('subscription', 'level').all()
    if len(mems) == 1:
        return redirect('settings_membership_edit', membership_id=mems[0].pk)
    mems = sorted(mems, key=Membership.template_sort)
    context = {'memberships': mems}
    return render(request, 'settings/home.html', context=context)


class SingleMembershipMixin(LoginRequiredMixin):
    @property
    def membership_id(self) -> int:
        return self.kwargs['membership_id']

    def get_membership(self) -> Membership:
        return get_object_or_404(
            self.request.user.customer.memberships,
            pk=self.membership_id)

    def get_context_data(self, **kwargs) -> dict:
        mem = self.get_membership()
        subs: Optional[looper.models.Subscription] = mem.subscription

        return {
            **super().get_context_data(**kwargs),
            'membership': mem,
            'subscription': subs,
        }

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            # The AnonymousUser instance doesn't have a 'memberships' property,
            # but login checking only happens in the super().dispatch() call.
            return self.handle_no_permission()
        response = self.pre_dispatch(request, *args, **kwargs)
        if response:
            return response
        return super().dispatch(request, *args, **kwargs)

    def pre_dispatch(self, request, *args, **kwargs) -> Optional[HttpResponse]:
        """Called between a permission check and calling super().dispatch().

        This allows you to get the current membership, or otherwise do things
        that require the user to be logged in.

        :return: None to continue handling this request, or a
            HttpResponse to stop processing early.
        """


class MembershipView(SingleMembershipMixin, FormView):
    form_class = forms.MembershipForm
    template_name = 'settings/membership_edit.html'

    membership: Membership
    fields_allowed_to_update: Set[str]

    def pre_dispatch(self, request, *args, **kwargs):
        self.membership = self.get_membership()

        allowed_by_memlevel = self.membership.level.visible_attributes_set
        self.fields_allowed_to_update = allowed_by_memlevel | {'is_private'}

    def get_initial(self) -> dict:
        membership = self.membership
        initial = {k: getattr(membership, k)
                   for k in self.fields_allowed_to_update}
        return initial

    def get_form(self, form_class=None) -> forms.MembershipForm:
        form = super().get_form(form_class)
        membership = self.membership

        # If some fields should not be edited, simply show them as hidden.
        # The value will be ignored when saving, because of fields_allowed_to_update
        for attr in membership.level.hidden_attributes_set:
            form.fields[attr].widget = django.forms.HiddenInput()
            form.fields[attr].required = False
        return form

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        subs: Optional[looper.models.Subscription] = ctx['subscription']
        order: Optional[looper.models.Order] = None if not subs else subs.latest_order()
        trans: Optional[looper.models.Transaction] = None
        if order:
            trans = order.latest_transaction()

        # Whether there could be a charge (either manual or automatic)
        # for this subscription.
        may_be_charged = (subs is not None and
                          subs.status in RECHARGEABLE_SUBS_STATUSES and
                          subs.collection_method != 'managed')

        # TODO: tests allow memberhips without subscriptions,
        # not sure if IRL this is the case as well
        extend_form = None
        if subs:
            currency = subs.currency
            order_pk = self.request.GET.get('order_pk', None)
            extend_form = forms.FlexiblePaymentForm(
                initial={'currency': currency, 'order_pk': order_pk}
            )
            price_per_month = self.membership.level.price_per_month(currency)
            price_field = extend_form.fields['price']
            price_field.label += f' Enter any amount in {price_per_month.currency_symbol}'
            price_field.widget.attrs['placeholder'] = price_per_month.decimals_string
            price_field.widget.attrs['min'] = price_per_month.decimals_string
        return {
            **ctx,
            'may_be_charged': may_be_charged,
            'order': order,
            'transaction': trans,
            'extend_form': extend_form,
        }

    def form_valid(self, form):
        membership = self.get_membership()

        if membership.logo:
            old_logo = pathlib.Path(membership.logo.path)
        else:
            old_logo = None

        # TODO: move this into the Membership.save() method, after saving, to
        # keep the filesystem in sync with the database (even when the DB save fails).
        for allowed_field in self.fields_allowed_to_update & set(form.cleaned_data.keys()):
            if allowed_field == 'logo':
                self.update_logo(form, membership)
            else:
                setattr(membership, allowed_field, form.cleaned_data[allowed_field])

        # TODO(Sybren): flag this membership as being edited and posisbly requiring review.
        membership.save(update_fields=self.fields_allowed_to_update)

        self.maybe_delete_logo(membership, old_logo)

        return super().form_valid(form)

    def update_logo(self, form: forms.MembershipForm, membership: Membership) -> None:
        """Handle logo deletiong as well as uploads."""
        if form.cleaned_data['logo'] is None:
            # No new logo uploaded, but 'clear' checkbox also not checked.
            return

        if form.cleaned_data['logo'] is False:
            # We are deleting the image
            current_path = pathlib.Path(membership.logo.path)
            if current_path.is_file():
                current_path.unlink()
            membership.logo = None
        elif 'logo' in self.request.FILES:
            # We are uploading a new file
            membership.logo = self.request.FILES['logo']

    def maybe_delete_logo(self, membership: Membership, old_logo: Optional[pathlib.Path]):
        """Delete the old logo if deleted or replaced by a new one."""

        if not old_logo or not old_logo.exists():
            return

        if membership.logo:
            current_path = pathlib.Path(membership.logo.path)
            if current_path == old_logo:
                return

        log.debug('Deleting old logo %s for membership %d', old_logo, membership.id)
        old_logo.unlink()

    def get_success_url(self) -> str:
        """Return the current URL."""
        return self.request.build_absolute_uri()


class CancelMembershipView(SingleMembershipMixin, FormView):
    template_name = 'settings/membership_cancel.html'
    form_class = forms.CancelMembershipForm
    initial = {'confirm': False}

    _log = log.getChild('CancelMembershipView')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'back_url': reverse(
                'settings_membership_edit', kwargs={'membership_id': self.membership_id}
            ),
        }

    def get_success_url(self) -> str:
        return reverse('settings_membership_edit',
                       kwargs={'membership_id': self.membership_id})

    def form_valid(self, form):
        membership = self.get_membership()
        self._log.info('Cancelling membership pk=%d on behalf of user pk=%d',
                       membership.pk, self.request.user.pk)
        membership.cancel()
        return super().form_valid(form)


class CancelMembershipViaTokenView(FormView):
    template_name = 'settings/membership_cancel.html'
    form_class = forms.CancelMembershipForm
    initial = {'confirm': False}

    _log = log.getChild('CancelMembershipViaTokenView')

    def get_membership(self, token):
        linktoken = get_object_or_404(looper.models.LinkCustomerToken, token=token)
        # if the linktoken has been already used, we need to lookup customer via a user,
        # because customer field has been set to null
        customer = linktoken.customer or linktoken.linked_to_user.customer
        memberships = customer.memberships
        memberships_count = memberships.count()
        if memberships_count != 1:
            self._log.error(
                'Expected exactly one membership, found %s, customer pk=%s',
                memberships_count,
                customer.pk,
            )
        return memberships.first()

    def get_context_data(self, **kwargs):
        token = self.kwargs['token']
        membership = self.get_membership(token)

        return {
            **super().get_context_data(**kwargs),
            'back_url': reverse('link_membership', kwargs={'token': token}),
            'membership': membership,
            'subscription': membership.subscription,
        }

    def form_valid(self, form):
        token = self.kwargs['token']
        membership = self.get_membership(token)
        self._log.info('Cancelling membership pk=%d using token=%s',
                       membership.pk, token)
        membership.cancel()
        return super().form_valid(form)

    def get_success_url(self) -> str:
        # land on the same page, the template will hide the form
        # and confirm that cancellation has happened
        return reverse('settings_membership_cancel_via_token',
                       kwargs={'token': self.kwargs['token']})


class ExtendMembershipView(SingleMembershipMixin, ExpectReadableIPAddressMixin, FormView):
    """Allow users to extend their membership by paying any amount."""
    # TODO(Sybren): maybe move this into Looper, or at least some of the code.
    # I put it here for now so that we have access to the membership without
    # jumping through any hoops.

    http_method_names = ['post']
    form_class = forms.FlexiblePaymentForm

    membership: Membership
    subscription: looper.models.Subscription
    price_per_month: Money

    def pre_dispatch(self, request, *args, **kwargs) -> Optional[HttpResponse]:
        self.membership = self.get_membership()

        # Ensure that this view is only handled when there is a subscription.
        # Without subscription we have no next_payment date to shift.
        # Managed memberships can also not be extended in this manner.
        if not self.membership.subscription_id or self.membership.is_managed:
            return redirect('membership_extend_not_possible', membership_id=self.membership_id)

        self.subscription = self.membership.subscription
        self.price_per_month = self.membership.level.price_per_month(self.subscription.currency)

        return None

    def get_context_data(self, **kwargs) -> dict:
        days_per_month = 365 / 12  # Average number of days in a month.
        return {
            **super().get_context_data(**kwargs),
            # For showing the user.
            'price_per_month': self.price_per_month,

            # For calculating the new next_payment date in JavaScript.
            'price_per_day': self.price_per_month // days_per_month,
        }

    def get_form(self, form_class=None) -> forms.FlexiblePaymentForm:
        form: forms.FlexiblePaymentForm = super().get_form(form_class)
        price_field = form.fields['price']
        price_field.label += f' Enter any amount in {self.price_per_month.currency_symbol}'
        price_field.widget.attrs['placeholder'] = self.price_per_month.decimals_string
        price_field.widget.attrs['min'] = '5.00'
        form.initial['gateway'] = 'stripe'
        return form

    def get_initial(self) -> dict:
        return {
            **super().get_initial(),
            'currency': self.price_per_month.currency,
            'email': self.membership.customer.billing_address.email,
            'full_name': self.membership.customer.billing_address.full_name,
        }

    def get_form_kwargs(self) -> dict:
        return {
            **super().get_form_kwargs(),
        }

    def form_valid(self, form: forms.FlexiblePaymentForm) -> HttpResponse:
        response = self._check_customer_ip_address()
        if response:
            return response

        # Create a new order, or use the one passed via stripe session cancel_url
        order_pk = form.cleaned_data.get('order_pk')
        if order_pk:
            order: looper.models.Order = get_object_or_404(self.subscription.order_set, pk=order_pk)
            log.debug('Reusing order pk=%r for extending subscription pk=%r',
                      order.pk, self.subscription.pk)
        else:
            # Create an order with the correct payment method, as this may be a different one
            # than connected to the subscription.
            order = self.subscription.generate_order(save=False)
            log.info('Creating order to extend subscription pk=%r', self.subscription.pk)
        # Make sure the order reflects whatever the user entered in the form, rather
        # than the subscription's price.
        order.price = form.cleaned_data['price']
        order.save()

        success_url = self.request.build_absolute_uri(
            reverse(
                'membership_extend_done',
                kwargs={
                    'membership_id': self.membership.id,
                    'stripe_session_id': 'CHECKOUT_SESSION_ID',
                }
            )
        )
        success_url = success_url.replace('CHECKOUT_SESSION_ID', '{CHECKOUT_SESSION_ID}', 1)
        cancel_url = self.request.build_absolute_uri(
            reverse('settings_membership_edit', kwargs={'membership_id': self.membership.id})
        ) + f'?order_pk={order.pk}'
        session = looper.stripe_utils.create_stripe_checkout_session_for_order(
            order,
            success_url,
            cancel_url,
            payment_intent_metadata={'extend_subscription': True, 'order_id': order.pk},
        )
        return redirect(session.url)


class ExtendMembershipNotPossibleView(SingleMembershipMixin, TemplateView):
    template_name = 'settings/extend_membership_not_possible.html'


class ExtendMembershipDoneView(SingleMembershipMixin, View):
    log = log.getChild('ExtendMembershipDoneView')

    def get(self, request, *args, **kwargs):
        membership = get_object_or_404(Membership, pk=self.kwargs['membership_id'])
        stripe_session_id = self.kwargs['stripe_session_id']
        gateway = looper.models.Gateway.objects.get(name='stripe')
        stripe_session = gateway.provider.retrieve_checkout_session_expanded(stripe_session_id)
        payment_intent = stripe_session.payment_intent
        order_id = int(payment_intent.metadata['order_id'])
        order = get_object_or_404(looper.models.Order, pk=order_id)
        if order.subscription != membership.subscription:
            log.error(
                'Membership pk=%s from url doesn\'t match order pk=% from payment_intent.metadata',
                membership.pk, order.pk,
            )
            return HttpResponseBadRequest()
        if not payment_intent.metadata.get('extend_subscription', None):
            log.error(
                'payment_intent.metadata is missing extend_subscripiton=True',
            )
            return HttpResponseBadRequest()

        log.info('Processing stripe success url order pk=%s with status %s', order.pk, order.status)
        if order.status in {'paid', 'fulfilled'}:
            log.info('Order is already paid, probably success_url is visited twice')
            # nothing to do
        elif not order.may_transition_to('paid'):
            # should never happen here normally, TODO a proper template
            log.error('Order can\'t be paid')
            return HttpResponseBadRequest()
        else:
            log.info('Processing payment_intent id=%s', payment_intent.id)
            if payment_intent.status != 'succeeded':
                log.error('payment_intent has status %s, won\'t process', payment_intent.status)
                return HttpResponseBadRequest()

            log.info('Creating a transaction based on a succeeded payment_intent')
            customer_ip_address = looper.utils.get_client_ip(self.request)
            looper.stripe_utils.process_payment_intent_for_subscription_order(
                payment_intent,
                order,
                customer_ip_address,
            )

        return redirect('settings_membership_edit', membership_id=membership.id)
