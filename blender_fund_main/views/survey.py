from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views.generic import TemplateView
import waffle

from blender_fund_main.models import Survey


class SurveyView(LoginRequiredMixin, TemplateView):

    def get_template_names(self):
        """Show different templates if survey flag is on or off."""
        if waffle.flag_is_active(self.request, 'survey'):
            return ['blender_fund_main/survey.html',]
        else:
            return ['blender_fund_main/survey_closed.html']

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if survey := Survey.objects.filter(user=self.request.user).first():
            ctx['survey'] = survey
        return ctx

    def post(self, request):
        response_data = {}
        for key, value in self.request.POST.items():
            if key == 'csrfmiddlewaretoken':
                continue
            if key.startswith('multi__'):
                items = [k for k in request.POST.getlist(key) if k]
                # Skip empty list
                if not items:
                    continue
                response_data[key] = request.POST.getlist(key)
            else:
                if not value:
                    continue
                response_data[key] = value
        Survey.objects.update_or_create(
            user=self.request.user,
            defaults={'response_data': response_data},
        )
        return redirect('survey-done')


class SurveyDoneView(LoginRequiredMixin, TemplateView):
    template_name = 'blender_fund_main/survey_done.html'
