import datetime
import logging
import urllib.parse

from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import HttpResponseBadRequest, HttpResponseNotFound, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views.decorators.http import require_POST
from django.views.generic import TemplateView, View
import stripe

from looper.money import Money
import looper.models
import looper.stripe_utils
import looper.utils

from blender_fund_main.forms import DonationDisplayNameForm
import blender_fund_main.tasks as tasks

logger = logging.getLogger(__name__)


class ExpressCheckoutDonateOnceView(View):
    """Redirects to stripe-hosted checkout page for one-time donation, success_url set to our
    thank you page.
    """

    def get(self, request, *args, **kwargs):
        # Fallbacks shouldn't be needed here, but currently it's possible to submit the form with an
        # empty custom amount. It would be nice to improve client-side validation, but until then
        # just offer to make a small donation.
        fallback_amount = 500
        fallback_currency = 'EUR'
        amount = request.GET.get('valueAmount', fallback_amount)
        try:
            amount = int(amount) or fallback_amount
        except ValueError:
            amount = fallback_amount
        if amount < 0:
            return HttpResponseBadRequest()
        currency = request.GET.get('valueCurrency') or fallback_currency
        if currency.upper() not in settings.SUPPORTED_CURRENCIES:
            return HttpResponseBadRequest()

        # The reason we aren't templating session ID into success_url here
        # is to prevent a scenario when people might share their thank you pages
        # and we leak some important personal data.
        # As long as the thank you page relies on the session cookie for fetching
        # and displaying anything personal there's no danger of that happening.
        success_url = self.request.build_absolute_uri(reverse('donate_once_thank_you'))
        session = stripe.checkout.Session.create(
            line_items=[
                {
                    'price_data': {
                        'currency': currency.lower(),
                        'product_data': {'name': 'One-time donation'},
                        'unit_amount': amount,
                    },
                    'quantity': 1,
                },
            ],
            mode='payment',
            submit_type='donate',
            success_url=success_url,
            ui_mode='hosted',
        )
        request.session['stripe_session_id'] = session.id
        request.session['stripe_session_created'] = session.created
        return redirect(session.url)


class ExpressCheckoutPlanVariationView(View):
    """
    Redirects to stripe-hosted checkout page to pay for a new membership.

    This view is a distilled copy-paste of looper.views.checkout_stripe.CheckoutStripeView.
    The main difference is that we don't collect customer.billing_address, expecting to populate
    it later, when the checkout session succeeds.
    This flow doesn't handle the bank gateway.
    """

    def get(self, request, *args, **kwargs):
        try:
            looper.utils.clean_ip_address(request)
        except ValidationError:
            return HttpResponseBadRequest()

        plan_variation = get_object_or_404(
            looper.models.PlanVariation, pk=self.kwargs['plan_variation']
        )

        self.user = self.request.user
        if self.user.is_authenticated:
            self.customer = self.user.customer
        else:
            self.customer = None

        success_url = self.request.build_absolute_uri(
            reverse(
                'stripe_success_create_subscription',
                kwargs={
                    'stripe_session_id': 'CHECKOUT_SESSION_ID',
                }
            )
        )
        # we have to do it to avoid uri-encoding of curly braces,
        # otherwise stripe doesn't do the template substitution
        success_url = success_url.replace('CHECKOUT_SESSION_ID', '{CHECKOUT_SESSION_ID}', 1)
        cancel_url = self.request.build_absolute_uri(
            reverse('landing')
        )

        session = looper.stripe_utils.create_stripe_checkout_session_for_plan_variation(
            plan_variation,
            self.customer,
            success_url,
            cancel_url,
        )
        return redirect(session.url)


class DonateOnceThankYou(TemplateView):
    """Shows a thank you page and sets the donatedAt cookie."""
    template_name = 'blender_fund_main/donate_once_thank_you.html'
    # A minimum amount in cents at which thank you page shows a display name form
    min_amount_for_display_name = 500

    def get_context_data(self):
        logger.info(
            'User pk=%s visited %s',
            self.request.user.is_authenticated and self.request.user.pk or '-',
            self.__class__.__name__,
        )
        context = super().get_context_data()
        stripe_session_id = self.request.session.get('stripe_session_id')
        if not stripe_session_id:
            return context

        session = stripe.checkout.Session.retrieve(
            stripe_session_id, expand=['payment_intent', 'payment_intent.latest_charge']
        )
        payment_intent = session.payment_intent
        # Gather paid_at date from Stripe's because the order might not be recorded yet
        paid_at = (
            session.payment_status == 'paid'
            and payment_intent.latest_charge
            and payment_intent.latest_charge.created
            or None
        )
        context['paid_at'] = paid_at

        amount = (
            payment_intent
            and payment_intent.amount_received
            or 0
        )
        if amount >= self.min_amount_for_display_name:
            context['amount'] = Money(payment_intent.currency.upper(), amount)
            context['form'] = DonationDisplayNameForm()
        return context


@require_POST
def save_donation_display_name(request):
    """
    Save a public display name for a Donation.

    Expected to be called from a thank you page with a valid checkout session ID.
    """
    stripe_session_id = request.session.get('stripe_session_id')
    stripe_session_created = request.session.get('stripe_session_created')
    if not stripe_session_id or not stripe_session_created:
        return HttpResponseNotFound('')

    created_at = datetime.datetime.fromtimestamp(stripe_session_created)
    name_change_dealine = created_at + datetime.timedelta(hours=1)
    if datetime.datetime.now() > name_change_dealine:
        return JsonResponse(
            {'errors': {'display_name': ['Name can no longer be changed']}},
            status=400,
        )

    form = DonationDisplayNameForm(data=request.POST)
    if not form.is_valid():
        return JsonResponse({'errors': form.errors}, status=400)

    display_name = form.cleaned_data['display_name']
    tasks.update_donation_display_name(
        stripe_session_id=stripe_session_id,
        display_name=display_name,
    )
    return JsonResponse({'display_name': display_name})
