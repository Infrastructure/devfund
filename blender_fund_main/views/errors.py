import logging

from django.conf import settings
from django.shortcuts import render

log = logging.getLogger(__name__)


def error_handler_500(request):
    log.exception('Uncaught exception, going to return a 500 Internal Server Error')
    return render(request, 'errors/error500.html', {}, status=500)


def test_error_500(request):
    if settings.DEBUG:
        # When in debug mode just raising an exception will show
        # the debug screen, and not call the error handler. We have
        # to do that ourselves.
        try:
            raise NotImplementedError('this is an exception that should cause a 500 error')
        except NotImplementedError:
            # Call the handler from within an exception handler,
            # to ensure sys.exc_info() returns something useful.
            return error_handler_500(request)
    else:
        raise NotImplementedError('this is an exception that should cause a 500 error')
