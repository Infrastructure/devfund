import logging

from django.contrib.auth.mixins import AccessMixin, LoginRequiredMixin
from django.db import transaction
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView

from looper.models import (
    GatewayCustomerId,
    LinkCustomerToken,
    Order,
    PaymentMethod,
    Transaction,
)
import blender_fund_main.models as models

from .. import forms

logger = logging.getLogger(__name__)


def _get_linktoken(token: str):
    return get_object_or_404(LinkCustomerToken, token=token)


@transaction.atomic()
def merge_customer(token: str, old_customer, user):
    """
    Reassign all records linked to old_customer to the customer of the given user.

    Even though LinkMembershipView and LinkDonationView exist as separate views
    (due to them needing different templates and different human-readable URLs),
    they do essentially the same thing: move data from an account-less customer
    to the customer of the logged in user and then delete the old customer.
    """
    linktoken = _get_linktoken(token)
    # update token, so it can't be used twice
    linktoken.customer = None
    linktoken.linked_to_user = user
    linktoken.save()

    # collect campaign orders to reissue their badges later
    campaign_orders = list(models.CampaignsOrders.objects.filter(order__customer=old_customer))

    to_customer = user.customer
    # prepare for membership save: make sure payment method won't be dropped on customer change
    GatewayCustomerId.objects.filter(customer=old_customer).update(customer=to_customer)
    PaymentMethod.objects.filter(customer=old_customer).update(customer=to_customer)
    for membership in old_customer.memberships.all():
        # save the change that internally cascades to subscription, orders and transactions
        membership.customer = to_customer
        membership.save()

    # one-time donation weren't changed when membership customer was updated,
    # so they need to be updated manually
    Transaction.objects.filter(customer=old_customer).update(customer=to_customer)
    Order.objects.filter(customer=old_customer).update(customer=to_customer)

    # drop billing address as we have warned
    old_customer.billing_address.delete()

    # now it's safe to delete the old customer as nothing is referencing it
    old_customer.delete()

    return campaign_orders


def merge_customer_and_grant_badges(token: str, old_customer, user):
    """Merge old_customer and the customer of the given user and grant badges."""

    campaign_orders = merge_customer(token, old_customer, user)

    # grant campaign badges of the old customer to the newly linked account
    for campaign_order in campaign_orders:
        campaign_order.campaign.grant_badges({campaign_order.order_id})


class LinkMembershipView(AccessMixin, FormView):
    template_name = 'blender_fund_main/link_membership.html'
    form_class = forms.LinkMembershipForm
    success_url = reverse_lazy('settings_home')

    def _get_linktoken(self, token):
        return get_object_or_404(LinkCustomerToken, token=token)

    def _get_membership(self, token):
        linktoken = get_object_or_404(LinkCustomerToken, token=token)
        memberships = linktoken.customer.memberships
        memberships_count = memberships.count()
        if memberships_count != 1:
            logger.error(
                'Expected exactly one membership, found %s, customer pk=%s',
                memberships_count,
                linktoken.customer_id,
            )
        return memberships.first()

    def dispatch(self, request, *args, **kwargs):
        token = self.kwargs['token']
        linktoken = self._get_linktoken(token)
        linked_user = linktoken.linked_to_user
        # first use
        if linked_user is None:
            return super().dispatch(request, *args, **kwargs)
        # owner visits the claim link again
        if linked_user == request.user:
            return redirect('settings_home')
        # someone else is trying to access the link that has been used
        else:
            raise Http404()

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['cancel_url'] = reverse(
            'settings_membership_cancel_via_token', kwargs={'token': self.kwargs['token']}
        )
        context['membership'] = self._get_membership(self.kwargs['token'])
        return context

    def form_valid(self, form):
        token = self.kwargs['token']
        membership = self._get_membership(token)
        user = self.request.user
        if not user.is_authenticated:
            return self.handle_no_permission()
        old_customer = membership.customer
        assert old_customer.user is None

        merge_customer_and_grant_badges(token, old_customer, user)

        return super().form_valid(form)


class LinkDonationView(LoginRequiredMixin, FormView):
    template_name = 'blender_fund_main/link_donation.html'
    form_class = forms.LinkDonationForm
    success_url = reverse_lazy('settings_receipts')

    def _get_order(self, token):
        linktoken = get_object_or_404(LinkCustomerToken, token=token)
        orders = linktoken.customer.order_set
        orders_count = orders.count()
        if orders_count != 1:
            logger.error(
                'Expected exactly one order found %s, customer pk=%s',
                orders_count,
                linktoken.customer_id,
            )
        return orders.first()

    def dispatch(self, request, *args, **kwargs):
        token = self.kwargs['token']
        linktoken = _get_linktoken(token)
        linked_user = linktoken.linked_to_user
        # first use
        if linked_user is None:
            return super().dispatch(request, *args, **kwargs)
        # owner visits the claim link again
        if linked_user == request.user:
            return redirect('settings_receipts')
        # someone else is trying to access the link that has been used
        raise Http404()

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['order'] = self._get_order(self.kwargs['token'])
        return context

    def form_valid(self, form):
        token = self.kwargs['token']
        order = self._get_order(token)
        user = self.request.user
        old_customer = order.customer
        assert old_customer.user is None

        merge_customer_and_grant_badges(token, old_customer, user)

        return super().form_valid(form)
