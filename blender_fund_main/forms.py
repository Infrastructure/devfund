import logging

from django import forms
import django.utils.timezone

import looper.form_fields
import looper.gateways
import looper.models

from . import models

log = logging.getLogger(__name__)


class MembershipForm(forms.Form):
    """Form for editing a membership by customers.

    Note that this form is adjusted by MembershipView.get_form() to account
    for the allowed fields.
    """
    display_name = forms.CharField(label='Public Name', max_length=100)
    description = forms.CharField(label='Description', max_length=255, required=False,
                                  widget=forms.Textarea)
    url = forms.URLField(label='URL', max_length=255, required=False)
    logo = forms.ImageField(label='Your Logo', required=False, widget=forms.ClearableFileInput)
    is_private = forms.BooleanField(label='Keep my membership private', required=False)

    # TODO Add logic to display fields depending on the visible_fields of the Membership Level.
    # Currently the form field visibility is overridden on form creation, base on the Membership
    # Level, which provides a list of visible_properties


class CancelMembershipForm(forms.Form):
    confirm = forms.BooleanField(label='Confirm Membership Cancellation')


class MembershipAdminAddForm(forms.ModelForm):
    """Form for adding Managed Memberships in the admin."""

    class Meta:
        model = models.Membership
        fields = '__all__'  # ignored because the admin provides the fieldsets.

    log = log.getChild('MembershipAdminAddForm')

    status = forms.ChoiceField(label='Status',
                               initial='active',
                               choices=models.Membership.STATUS_CHOICES)

    interval_unit = forms.ChoiceField(
        label='Interval Unit',
        choices=looper.models.Subscription.INTERVAL_UNITS,
        initial=looper.models.Subscription.DEFAULT_INTERVAL_UNIT,
        help_text='The units in which "interval length" is expressed')
    interval_length = forms.IntegerField(
        initial=1,
        help_text='How many "interval units" between notification mails to the Manager, and used '
                  'for computing the amount of money per month this membership brings in (for the '
                  'stats on the front page).')

    currency = forms.ChoiceField(label='Currency',
                                 choices=looper.models.CURRENCIES,
                                 initial=looper.models.DEFAULT_CURRENCY)
    price = looper.form_fields.MoneyFormField(
        help_text='Amount of money per interval this membership represents. Only used for '
                  'statistics, such as the thermometer on the front page.')

    def save(self, commit=True) -> models.Membership:
        memb: models.Membership = super().save(commit=commit)

        self.log.info('Creating subscription for managed membership %r', memb.pk)

        # Ordinarily the membership is created in response to the creation of a subscription.
        # However, this time it's the opposite, so we have to create a suitable subscription now.
        start_time = django.utils.timezone.now()
        subs = looper.models.Subscription(
            # Fixed properties
            status='active',
            collection_method='managed',
            started_at=start_time,

            # Properties from the form
            interval_unit=self.cleaned_data['interval_unit'],
            interval_length=self.cleaned_data['interval_length'],
            currency=self.cleaned_data['currency'],
            price=self.cleaned_data['price'],
            # next_payment is automatically set by saving with status='active'

            # Properties copied from the Membership
            customer_id=memb.customer_id,
            plan_id=memb.level.plan_id,
            membership=memb,
        )

        # Prevent another membership from being created. However, this is not picked up
        # by Django (as it is the membership that contains the foreign key, and not the
        # subscription), so we need to set it explicitly on the membership too.
        # subs.membership = memb
        subs.save()

        memb.subscription_id = subs.pk
        if commit and memb.pk:
            # We can only save this to the database if the membership
            # already has a primary key.
            memb.save(update_fields={'subscription_id'})

        return memb


class FlexiblePaymentForm(forms.Form):
    currency = forms.CharField(widget=forms.HiddenInput())
    order_pk = forms.CharField(widget=forms.HiddenInput(), required=False)
    price = looper.form_fields.MoneyFormField(label='How much do you want to pay?')


class LinkMembershipForm(forms.Form):
    agree_to_link_membership = forms.BooleanField(
        label='Link this membership to my account',
        label_suffix='',
        required=True,
        initial=False,
    )


class LinkDonationForm(forms.Form):
    agree_to_link_donation = forms.BooleanField(
        label='Link this donation to my account',
        label_suffix='',
        required=True,
        initial=False,
    )


class DonationDisplayNameForm(forms.ModelForm):
    """Change publicly displayed Donation name."""

    class Meta:
        model = models.Donation
        fields = ['display_name']
