"""Delete tasks completed more than half a year ago."""
from datetime import timedelta
import logging

from django.core.management.base import BaseCommand
from django.utils import timezone

from background_task.models import CompletedTask

from .utils import chunks

DAYS = 6 * 30
DELETION_DELTA = timedelta(days=DAYS)
help_text = f"Delete tasks completed more than {DELETION_DELTA.days} days ago."
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Command(BaseCommand):  # noqa: D101
    help = help_text
    batch_size = 1000

    def add_arguments(self, parser):
        """Add limit argument."""
        _limit_text = 'Delete no more that this number of records then quit'
        parser.add_argument('--limit', type=int, default=1000, help=_limit_text)
        _dry_run_text = (
            "Just show show many completed tasks would be deletd; don't actually deleted them"
        )
        parser.add_argument('--dry-run', action='store_true', help=_dry_run_text)

    def handle(self, *args, **options):
        """Find and delete completed tasks matching the criteria."""
        is_dry_run = options['dry_run']
        prior_to = timezone.now() - DELETION_DELTA
        tasks_q = CompletedTask.objects.filter(run_at__lt=prior_to).order_by('pk')
        logger.info('Total tasks run before %s: %s', prior_to.date(), tasks_q.count())
        task_pks = tasks_q[: options['limit']].values_list('pk', flat=True)
        logger.info(
            f'{is_dry_run and "Would" or "Will"} delete %s completed tasks in batches of %s',
            len(task_pks),
            self.batch_size,
        )

        if not is_dry_run:
            for ids in chunks(task_pks, self.batch_size):
                tasks_q.filter(pk__in=ids)._raw_delete(tasks_q.db)

        logger.info('Total tasks run before %s remaining: %s', prior_to.date(), tasks_q.count())
