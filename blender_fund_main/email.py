import logging
import typing

from django.conf import settings
from django.template import loader

from . import models
import looper.models
import looper.signals

from blender_fund_main.utils import absolute_url, html_to_text

log = logging.getLogger(__name__)


def construct_membership_mail(membership: models.Membership) -> typing.Tuple[str, str, str]:
    """Construct a mail about a membership.

    :return: tuple (html, text, subject)
    """
    customer = membership.customer
    user = customer.user
    membership_url = link_membership_url = None
    if not user:
        if getattr(membership.customer, 'token', None):
            link_membership_url = absolute_url(
                'link_membership', kwargs={'token': membership.customer.token.token}
            )
    else:
        membership_url = absolute_url(
            'settings_membership_edit', kwargs={'membership_id': membership.pk}
        )

    if membership.status == 'active':
        verb = 'activated'
    elif (
        membership.status == 'inactive'
        and membership.subscription_id
        and membership.subscription.status == 'on-hold'
        and membership.subscription.payment_method.method_type == 'ba'
    ):
        # Send out an email notifying about the required bank payment
        verb = 'bankinfo'
    elif membership.status == 'inactive':
        verb = 'deactivated'
    else:
        raise Exception(f'Membership pk={membership.id} has unexpected status {membership.status}')

    context = {
        'user': user,
        'customer': customer,
        'full_name': customer.billing_address.full_name,
        'email': customer.billing_address.email,
        'membership': membership,
        'subscription': membership.subscription,
        'membership_url': membership_url,
        'link_membership_url': link_membership_url,
        'verb': verb,
    }

    subject: str = loader.render_to_string(
        f'blender_fund_main/emails/membership_{verb}_subject.txt', context)
    context['subject'] = subject.strip()

    email_body_html = loader.render_to_string(
        f'blender_fund_main/emails/membership_{verb}.html', context)
    # Generate plain text content from the HTML one
    email_body_txt = html_to_text(email_body_html)

    return email_body_html, email_body_txt, context['subject']


def construct_payment_mail(
    order: looper.models.Order, transaction: looper.models.Transaction
) -> typing.Tuple[str, str, str]:
    membership = order.subscription.membership
    customer = order.customer
    user = customer.user

    membership_url = link_membership_url = None
    if not user:
        if getattr(membership.customer, 'token', None):
            link_membership_url = absolute_url(
                'link_membership', kwargs={'token': membership.customer.token.token}
            )
    else:
        membership_url = absolute_url(
            'settings_membership_edit', kwargs={'membership_id': membership.pk}
        )
    pay_url = absolute_url('looper:checkout_existing_order', kwargs={'order_id': order.pk})
    receipt_url = absolute_url('settings_receipt', kwargs={'order_id': order.pk})

    context = {
        'user': user,
        'customer': customer,
        'full_name': customer.billing_address.full_name,
        'email': customer.billing_address.email,
        'order': order,
        'membership': membership,
        'pay_url': pay_url,
        'receipt_url': receipt_url,
        'membership_url': membership_url,
        'failure_message': transaction.failure_message,
        'payment_method': transaction.payment_method.recognisable_name,
        'maximum_collection_attemps': settings.LOOPER_CLOCK_MAX_AUTO_ATTEMPTS,
        'link_membership_url': link_membership_url,
    }

    subject: str = loader.render_to_string(
        f'blender_fund_main/emails/payment_{order.status}_subject.txt', context).strip()
    context['subject'] = subject

    email_body_html = loader.render_to_string(
        f'blender_fund_main/emails/payment_{order.status}.html', context)
    # Generate plain text content from the HTML one
    email_body_txt = html_to_text(email_body_html)

    return email_body_html, email_body_txt, context['subject']


def construct_managed_subscription_mail(subscription: looper.models.Subscription):
    customer = subscription.customer
    user = customer.user
    membership = subscription.membership
    subs_admin_url = absolute_url('admin:looper_subscription_change',
                                  kwargs={'object_id': subscription.id})
    memb_admin_url = absolute_url('admin:blender_fund_main_membership_change',
                                  kwargs={'object_id': membership.id})
    context = {
        'user': user,
        'customer': customer,
        'full_name': settings.LOOPER_MANAGER_MAIL,
        'email': settings.LOOPER_MANAGER_MAIL,
        'subscription': subscription,
        'membership': membership,
        'subs_admin_url': subs_admin_url,
        'memb_admin_url': memb_admin_url,
    }

    subject: str = loader.render_to_string(
        'blender_fund_main/emails/managed_memb_notif_subject.txt', context).strip()
    context['subject'] = subject

    email_body_html = loader.render_to_string(
        'blender_fund_main/emails/managed_memb_notif.html', context)
    # Generate plain text content from the HTML one
    email_body_txt = html_to_text(email_body_html)

    return email_body_html, email_body_txt, context['subject']


def construct_donation_received_mail(order):
    customer = order.customer
    user = customer.user

    receipts_url = receipt_url = receipt_pdf_url = link_donation_url = None
    if not user:
        if getattr(order.customer, 'token', None):
            link_donation_url = absolute_url(
                'link_donation', kwargs={'token': order.customer.token.token}
            )
    else:
        receipts_url = absolute_url('settings_receipts')
        receipt_url = absolute_url('settings_receipt', kwargs={'order_id': order.pk})
        receipt_pdf_url = absolute_url('settings_receipt_pdf', kwargs={'order_id': order.id})

    context = {
        'user': user,
        'customer': customer,
        'full_name': customer.billing_address.full_name,
        'email': order.email,
        'order': order,
        'receipt_url': receipt_url,
        'receipts_url': receipts_url,
        'receipt_pdf_url': receipt_pdf_url,
        'link_donation_url': link_donation_url,
    }

    verb = 'donation_received'
    subject: str = loader.render_to_string(
        f'blender_fund_main/emails/{verb}_subject.txt', context).strip()
    context['subject'] = subject

    email_body_html = loader.render_to_string(
        f'blender_fund_main/emails/{verb}.html', context
    )
    # Generate plain text content from the HTML one
    email_body_txt = html_to_text(email_body_html)

    return email_body_html, email_body_txt, context['subject']
