from unittest import mock

from django.conf import settings
from django.contrib.auth.models import User

from looper.tests import AbstractBaseTestCase
from .. import models
import looper.models

import blender_fund_main.tasks as tasks


@mock.patch(
    'blender_fund_main.tasks.grant_or_revoke_badge',
    new=tasks.grant_or_revoke_badge.task_function,
)
class MembershipBadgeLinkTest(AbstractBaseTestCase):
    fixtures = ['gateways', 'devfund', 'systemuser']  # So that membership levels are set up correctly.

    def setUp(self):
        self.user = User.objects.create_user('harry', 'harry@blender.org')

    @mock.patch('blender_fund_main.badges.change_badge')
    def test_membership_change_status(self, mock_change_badge):
        membership = models.Membership(
            customer=self.user.customer,
            subscription=None,
            display_name='Harry de Beuker',
            status='pending',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
        )
        membership.save()
        mock_change_badge.assert_not_called()

        membership.status = 'active'
        membership.save(update_fields={'status'})
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='devfund_bronze',
            revoke='',
        )

        membership.status = 'inactive'
        membership.save(update_fields={'status'})
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='',
            revoke='devfund_bronze',
        )

    @mock.patch('blender_fund_main.badges.change_badge')
    def test_create_delete_active_membership(self, mock_change_badge):
        membership = models.Membership(
            customer=self.user.customer,
            subscription=None,
            display_name='Harry de Beuker',
            status='active',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
        )
        membership.save()
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='devfund_bronze',
            revoke='',
        )

        membership.delete()
        mock_change_badge.assert_called_with(
            user=self.user,
            revoke='devfund_bronze',
            grant='',
        )

    @mock.patch('blender_fund_main.badges.change_badge')
    def test_changed_badge_name(self, mock_change_badge):
        membership = models.Membership(
            customer=self.user.customer,
            subscription=None,
            display_name='Harry de Beuker',
            status='active',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
            granted_badge='devfund_previous_badge',
        )
        membership.save()
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='devfund_bronze',
            revoke='devfund_previous_badge',
        )

    @mock.patch('blender_fund_main.badges.change_badge')
    def test_membership_change_badge_name(self, mock_change_badge):
        membership = models.Membership(
            customer=self.user.customer,
            subscription=None,
            display_name='Harry de Beuker',
            status='active',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
        )
        membership.save()
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='devfund_bronze',
            revoke='',
        )

        memlev = membership.level
        memlev.bid_badge_name = 'your-membership'
        memlev.save(update_fields={'bid_badge_name'})
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='your-membership',
            revoke='devfund_bronze',
        )

    @mock.patch('blender_fund_main.badges.change_badge')
    def test_membership_change_is_private(self, mock_change_badge):
        membership = models.Membership(
            customer=self.user.customer,
            subscription=None,
            display_name='Harry de Beuker',
            status='pending',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
        )
        membership.save()
        mock_change_badge.assert_not_called()

        membership.status = 'active'
        membership.save(update_fields={'status'})
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='devfund_bronze',
            revoke='',
        )

        membership.is_private = True
        membership.save(update_fields={'is_private'})
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='',
            revoke='devfund_bronze',
        )

    @mock.patch('blender_fund_main.badges.change_badge')
    def test_double_memberships_one_private(self, mock_change_badge):
        mem1 = models.Membership.objects.create(
            customer=self.user.customer,
            subscription=None,
            display_name='Harry de Beuker',
            status='active',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
        )
        mem2 = models.Membership.objects.create(
            customer=self.user.customer,
            subscription=None,
            display_name='Harry de Beuker',
            status='active',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
        )
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='devfund_bronze',
            revoke='',
        )
        mock_change_badge.reset_mock()

        mem2.is_private = True
        mem2.save(update_fields={'is_private'})
        mock_change_badge.assert_not_called()

        mem1.is_private = True
        mem1.save(update_fields={'is_private'})
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='',
            revoke='devfund_bronze',
        )

    @mock.patch('blender_fund_main.badges.change_badge')
    def test_double_memberships_delete_one(self, mock_change_badge):
        mem1 = models.Membership.objects.create(
            customer=self.user.customer,
            subscription=None,
            display_name='Harry de Beuker',
            status='active',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
        )
        mem2 = models.Membership.objects.create(
            customer=self.user.customer,
            subscription=None,
            display_name='Harry de Beuker',
            status='active',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
        )
        mock_change_badge.assert_called_with(
            user=self.user,
            grant='devfund_bronze',
            revoke='',
        )
        mock_change_badge.reset_mock()

        mem2.delete()
        mock_change_badge.assert_not_called()

        mem1.delete()
        mock_change_badge.assert_called_with(
            user=self.user,
            revoke='devfund_bronze',
            grant='',
        )

    @mock.patch('blender_fund_main.badges.change_badge')
    def test_badge_system_user(self, mock_change_badge):
        membership = models.Membership(
            customer=looper.models.Customer.objects.get(user_id=settings.LOOPER_SYSTEM_USER_ID),
            subscription=None,
            display_name='System the User',
            status='pending',
            level=models.MembershipLevel.objects.filter(name='Bronze').first(),
        )
        membership.save()
        mock_change_badge.assert_not_called()

        membership.status = 'active'
        membership.save(update_fields={'status'})
        mock_change_badge.assert_not_called()

        membership.status = 'inactive'
        membership.save(update_fields={'status'})
        mock_change_badge.assert_not_called()

    # TODO(Sybren): reassign membership to another user, should also reassign badge.
