from unittest.mock import patch

from django.contrib.auth.models import User
import django.core.mail

from looper.tests import AbstractLooperTestCase
import looper.signals

from blender_fund_main.utils import absolute_url
import blender_fund_main.tasks as tasks


@patch(
    'blender_fund_main.tasks.send_mail_managed_subscription_notification',
    new=tasks.send_mail_managed_subscription_notification.task_function,
)
@patch(
    'blender_fund_main.tasks.send_mail_membership_status_changed',
    new=tasks.send_mail_membership_status_changed.task_function,
)
class ManagedMembershipNotificationMailTest(AbstractLooperTestCase):
    fixtures = ['default_site', 'gateways', 'devfund', 'testuser', 'systemuser']

    def setUp(self):
        super().setUp()
        self.user = User.objects.get(email='harry@blender.org')

    def test_notification_mail(self):
        subs = self.create_active_subscription()
        subs.collection_method = 'managed'
        subs.save()
        self.assertEqual('active', subs.membership.status)

        looper.signals.managed_subscription_notification.send(sender=subs)

        # An email should have been sent to the manager about the next_payment,
        # and an email to the member about the activated membership.
        self.assertEqual(2, len(django.core.mail.outbox))
        the_mail: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[1]
        self.assertIn(subs.membership.level.name, the_mail.body)

        subs_admin_url = absolute_url('admin:looper_subscription_change',
                                      kwargs={'object_id': subs.id})
        memb_admin_url = absolute_url('admin:blender_fund_main_membership_change',
                                      kwargs={'object_id': subs.membership.id})
        self.assertIn(subs_admin_url, the_mail.body)
        self.assertIn(memb_admin_url, the_mail.body)
        self.assertIn(subs.membership.level.name, the_mail.body)
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
