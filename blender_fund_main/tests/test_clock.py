from datetime import timedelta
from unittest.mock import patch

from dateutil.relativedelta import relativedelta
from django.core import mail
from django.test import override_settings
from django.utils import timezone
import responses

from looper import admin_log
from looper.clock import Clock
from looper.models import Gateway, Subscription, PlanVariation
from looper.tests import AbstractLooperTestCase
from looper.tests.factories import SubscriptionFactory, create_customer_with_billing_address
import looper.exceptions

from blender_fund_main.tests.utils import (
    expected_pdf_text_tmpl,
    extract_text_from_pdf_bytes,
    to_ap_style_month,
)
from blender_fund_main.utils import html_to_text
import blender_fund_main.tasks as tasks


expected_managed_email_subj = 'Blender Development Fund managed membership needs attention'
expected_managed_email_body = '''Blender Development Fund managed membership needs attention

Dear admin@example.com,

Jane Doe has a Silver membership that just
passed its 'next payment' date.

See DevFund admin links:

- The Membership: https://fund.local:8010/admin/blender_fund_main/membership/1/change/
- The Subscription: https://fund.local:8010/admin/looper/subscription/1/change/


--
Kind regards,
Blender Foundation'''
expected_soft_failed_email_body = '''Blender Development Fund: payment failed (but we'll try again)

Dear Jane Doe,

Automatic payment of your Blender Development Fund membership failed.
Don't worry, we will automatically try again soon.
This was collection attempt 1 of 3.
If things still fail after that, we'll send you another email.

The error we received from the payment provider was: fake error message.

For now you can either wait for another automatic try, or visit
https://fund.local:8010/checkout/pay/1 to pay manually.


You can always go to https://fund.local:8010/settings/membership/1 to view and update your membership.


--
Kind regards,
Blender Foundation'''  # noqa: E501
expected_failed_email_body = '''Blender Development Fund: payment failed

Dear Jane Doe,

Automatic payment of your Blender Development Fund membership failed.
We have tried 3 times, but none of those attempts was succesful.
As a result, we have suspended your membership for now.

The error we received from the payment provider was: fake error message.

To resume your membership, please visit https://fund.local:8010/checkout/pay/1

You can always go to https://fund.local:8010/settings/membership/1 to view and update your membership.


--
Kind regards,
Blender Foundation'''  # noqa: E501
expected_payment_received_email_body = '''Blender Development Fund: payment received

Dear Jane Doe,

Automatic payment of your Blender Development Fund membership (€ 10.00)
was successful. Thank you for your contribution!

You will find the online receipt at https://fund.local:8010/settings/receipts/1
Your contribution helps dozens of people to work on Blender development, online infrastructure and documentation.
Check our annual reports and the grants overview at https://fund.blender.org/grants/.

In case this transaction was not authorized by you, please contact us by responding to this email. We will refund immediately.


You can always go to https://fund.local:8010/settings/membership/1 to view and update your membership.


--
Kind regards,
Blender Foundation'''  # noqa: E501


def _write_mail(mail):
    for email in mail.outbox:
        name = email.subject.replace(' ', '_')
        with open(f'/tmp/{name}.txt', 'w+') as f:
            f.write(str(email.body))
        for content, mimetype in email.alternatives:
            with open(f'/tmp/{name}.{mimetype.replace("/", ".")}', 'w+') as f:
                f.write(str(content))


@patch(
    'blender_fund_main.tasks.send_mail_automatic_payment_performed',
    new=tasks.send_mail_automatic_payment_performed.task_function,
)
@patch(
    'blender_fund_main.tasks.send_mail_managed_subscription_notification',
    new=tasks.send_mail_managed_subscription_notification.task_function,
)
class TestClockEmails(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['default_site']

    def setUp(self):
        super().setUp()
        self.subscription = self._create_membership_due_now()

    def _create_membership_due_now(self) -> Subscription:
        customer = create_customer_with_billing_address(
            country='NL', full_name='Jane Doe', email='jane.doe+billing@example.com'
        )
        now = timezone.now()
        planv = PlanVariation.objects.active().get(
            currency='EUR', interval_unit='month', interval_length=1, is_default_for_currency=True,
            collection_method='automatic',
            plan__name='Silver',
        )
        with patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now + relativedelta(months=-1)
            # print('fake now:', mock_now.return_value)
            subscription = SubscriptionFactory(
                currency=planv.price.currency,
                customer=customer,
                payment_method__customer_id=customer.pk,
                payment_method__gateway_id=Gateway.objects.get(is_default=True).pk,
                payment_method__recognisable_name='Test payment method',
                plan=planv.plan,
                price=planv.price,
                status='active',
            )
        self.assertEqual(subscription.payment_method.gateway.name, 'stripe')
        self._check_due_now(subscription)
        # Sanity check: no orders generated yet:
        self.assertIsNone(subscription.latest_order())
        assert subscription.membership
        return subscription

    def _check_due_now(self, subscription):
        now = timezone.now()
        # Sanity check: next payment should be due already:
        self.assertLess(subscription.next_payment, now)
        # Because some months have 31 days, next payment date can be over a day late in this test
        self.assertAlmostEqual(now, subscription.next_payment, delta=timedelta(hours=25))

    @responses.activate
    def test_automated_payment_soft_failed_email_is_sent(self):
        now = timezone.now()

        def _charge_error(*args, **kwargs):
            raise looper.exceptions.GatewayError(message='fake error message')

        # Tick the clock and check that order and transaction were created
        with patch(
            'looper.gateways.StripeGateway.transact_sale',
            side_effect=_charge_error,
        ):
            Clock().tick()

        # The subscription should not be renewed
        self.subscription.refresh_from_db()
        self.assertEqual('active', self.subscription.status)
        self.assertEqual(1, self.subscription.intervals_elapsed)
        self._check_due_now(self.subscription)

        # Test the order
        new_order = self.subscription.latest_order()
        new_order.refresh_from_db()
        self.assertEqual('soft-failed', new_order.status)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNotNone(new_order.display_number)
        self.assertAlmostEqual(now, new_order.retry_after, delta=timedelta(days=3))

        # Test the transaction
        new_transaction = new_order.latest_transaction()
        self.assertEqual('failed', new_transaction.status)
        self.assertEqual('fake error message', new_transaction.failure_message)
        self.assertEqual(self.subscription.price, new_transaction.amount)
        entries_q = admin_log.entries_for(new_transaction)
        self.assertEqual(1, len(entries_q))
        self.assertIn('fail', entries_q.first().change_message)

        # Check that an email notification is sent
        customer = self.subscription.customer
        user = customer.user
        _write_mail(mail)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.to, [user.customer.billing_address.email])
        # TODO(anna): set the correct reply_to
        self.assertEqual(email.reply_to, [])
        # TODO(anna): set the correct from_email DEFAULT_FROM_EMAIL
        self.assertEqual(email.from_email, 'Blender Development Fund <fund@blender.org>')
        self.assertEqual(
            email.subject, "Blender Development Fund: payment failed (but we'll try again)"
        )
        self.assertEqual(email.alternatives[0][1], 'text/html')
        self.assertEqual(email.body, expected_soft_failed_email_body, email.body)
        self.assertEqual(
            html_to_text(email.alternatives[0][0]),
            expected_soft_failed_email_body,
            html_to_text(email.alternatives[0][0]),
        )
        self.assertEqual(len(email.attachments), 0)

    @responses.activate
    def test_automated_payment_failed_email_is_sent(self):
        now = timezone.now()
        order = self.subscription.generate_order()
        # Make the clock attempt to charge the same order one last time
        order.retry_after = now - timedelta(seconds=2)
        order.collection_attempts = 2
        order.status = 'soft-failed'
        order.save(update_fields={'collection_attempts', 'retry_after', 'status'})
        self.assertIsNotNone(self.subscription.latest_order())

        # Tick the clock and check that order and transaction were created
        # TODO: check that barge was revoked
        def _charge_error(*args, **kwargs):
            raise looper.exceptions.GatewayError(message='fake error message')
        with patch(
            'looper.gateways.StripeGateway.transact_sale',
            side_effect=_charge_error,
        ):
            Clock().tick()

        # The subscription should be on hold
        self.subscription.refresh_from_db()
        self.assertEqual('on-hold', self.subscription.status)
        self.assertEqual(0, self.subscription.intervals_elapsed)
        self._check_due_now(self.subscription)

        # Test the order
        new_order = self.subscription.latest_order()
        # It must be the same order
        self.assertEqual(order.pk, new_order.pk)
        self.assertEqual('failed', new_order.status)
        self.assertEqual(3, new_order.collection_attempts)
        self.assertAlmostEqual(now, new_order.retry_after, delta=timedelta(days=3))

        # Test the transaction
        new_transaction = new_order.latest_transaction()
        self.assertEqual('failed', new_transaction.status)
        self.assertEqual('fake error message', new_transaction.failure_message)
        self.assertEqual(self.subscription.price, new_transaction.amount)
        entries_q = admin_log.entries_for(new_transaction)
        self.assertEqual(1, len(entries_q))
        self.assertIn('fail', entries_q.first().change_message)

        # Check that an email notification is sent
        customer = self.subscription.customer
        user = customer.user
        self.assertEqual(len(mail.outbox), 1)
        _write_mail(mail)
        email = mail.outbox[0]
        self.assertEqual(email.to, [user.customer.billing_address.email])
        # TODO(anna): set the correct reply_to
        self.assertEqual(email.reply_to, [])
        # TODO(anna): set the correct from_email DEFAULT_FROM_EMAIL
        self.assertEqual(email.from_email, 'Blender Development Fund <fund@blender.org>')
        self.assertEqual(email.subject, 'Blender Development Fund: payment failed')
        self.assertEqual(email.alternatives[0][1], 'text/html')
        self.assertEqual(email.body, expected_failed_email_body, email.body)
        self.assertEqual(
            html_to_text(email.alternatives[0][0]),
            expected_failed_email_body,
            html_to_text(email.alternatives[0][0]),
        )
        self.assertEqual(len(email.attachments), 0)

    @responses.activate
    def test_automated_payment_paid_email_is_sent(self):
        now = timezone.now()
        self.assertEqual(self.subscription.collection_method, 'automatic')
        self.assertEqual(self.subscription.payment_method.gateway.name, 'stripe')

        # Tick the clock and check that subscription renews, order and transaction were created
        with patch(
            'looper.gateways.StripeGateway.transact_sale',
            return_value={'transaction_id': 'mock-transaction-id'},
        ):
            Clock().tick()

        # Test the order
        new_order = self.subscription.latest_order()
        new_order.refresh_from_db()
        self.assertEqual('paid', new_order.status)
        self.assertEqual(1, new_order.collection_attempts)
        self.assertIsNone(new_order.retry_after)
        self.assertIsNotNone(new_order.display_number)
        # self.assertNotEqual(last_order_pk, new_order.pk)

        # The subscription should be renewed now.
        self.subscription.refresh_from_db()
        self.assertEqual('active', self.subscription.status)
        self.assertEqual(1, self.subscription.intervals_elapsed)
        self.assertAlmostEqual(
            now + relativedelta(months=1),
            self.subscription.next_payment,
            delta=timedelta(seconds=1),
        )

        # Test the transaction
        new_transaction = new_order.latest_transaction()
        self.assertEqual('succeeded', new_transaction.status)
        self.assertEqual(self.subscription.price, new_transaction.amount)
        entries_q = admin_log.entries_for(new_transaction)
        self.assertEqual(1, len(entries_q))
        self.assertIn('success', entries_q.first().change_message)

        # Check that an email notification is sent
        customer = self.subscription.customer
        user = customer.user
        _write_mail(mail)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.to, [user.customer.billing_address.email])
        # TODO(anna): set the correct reply_to
        self.assertEqual(email.reply_to, [])
        # TODO(anna): set the correct from_email DEFAULT_FROM_EMAIL
        self.assertEqual(email.from_email, 'Blender Development Fund <fund@blender.org>')
        self.assertEqual(email.subject, 'Blender Development Fund: payment received')
        self.assertEqual(email.alternatives[0][1], 'text/html')
        self.assertEqual(email.body, expected_payment_received_email_body, email.body)
        self.assertEqual(
            html_to_text(email.alternatives[0][0]),
            expected_payment_received_email_body,
            html_to_text(email.alternatives[0][0]),
        )

        # Check that receipt PDF is attached to the email
        self.assertEqual(len(email.attachments), 1)
        title, content, mime = email.attachments[0]
        self.assertEqual(title, f'blender-development-fund-receipt-{new_order.pk}.pdf')
        self.assertEqual(mime, 'application/pdf')
        pdf_text = extract_text_from_pdf_bytes(content)
        self.assertEqual(
            pdf_text,
            expected_pdf_text_tmpl.format(
                order=new_order,
                expected_address=' Jane Doe\nNetherlands',
                expected_currency_symbol='€',
                expected_date=to_ap_style_month(new_order.paid_at.strftime('%b. %-d, %Y')),
                expected_description=' Membership level\n Silver\n',
                expected_email='jane.doe+billing@example.com',
                expected_payment_method='Test payment method',
                expected_total='10',
            ),
            pdf_text,
        )

    @override_settings(LOOPER_MANAGER_MAIL='admin@example.com')
    def test_managed_subscription_notification_email_is_sent(self):
        now = timezone.now()
        self.subscription.collection_method = 'managed'
        self.subscription.save(update_fields={'collection_method'})

        # Tick the clock and check that subscription renews, order and transaction were created
        Clock().tick()

        # The subscription should be renewed now.
        self.subscription.refresh_from_db()
        self.assertEqual('active', self.subscription.status)
        self.assertEqual(1, self.subscription.intervals_elapsed)
        self.assertAlmostEqual(now, self.subscription.next_payment, delta=timedelta(hours=25))
        self.assertAlmostEqual(now, self.subscription.last_notification, delta=timedelta(seconds=1))

        # Check that an email notification is sent
        _write_mail(mail)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.to, ['admin@example.com'])
        self.assertEqual(email.from_email, 'Blender Development Fund <fund@blender.org>')
        self.assertEqual(email.subject, expected_managed_email_subj)
        self.assertEqual(email.alternatives[0][1], 'text/html')
        self.assertEqual(email.body, expected_managed_email_body, email.body)
        self.assertEqual(
            html_to_text(email.alternatives[0][0]),
            expected_managed_email_body,
            html_to_text(email.alternatives[0][0]),
        )
        self.assertEqual(len(email.attachments), 0)
