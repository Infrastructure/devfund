import logging
from pathlib import Path
import tempfile
from urllib.parse import urljoin

from django.urls import reverse
from django.test import override_settings

from looper.tests import AbstractLooperTestCase
from .. import models

log = logging.getLogger(__name__)


class AbstractMembershipEditTest(AbstractLooperTestCase):
    def setUp(self):
        super().setUp()

        self.mem = models.Membership.objects.create(
            level_id=1,
            status='active',
            customer=self.user.customer,
        )
        self.url = reverse('settings_membership_edit',
                           kwargs={'membership_id': self.mem.id})
        self.client.force_login(self.user)

    def assertIsRedirectToEditPage(self, resp):
        self.assertEqual(302, resp.status_code, resp.content.decode())
        self.assertEqual(urljoin('http://testserver/', self.url), resp['Location'])

    def switch_to_corporate(self):
        self.mem.level_id = 7
        self.mem.save()
        self.mem.refresh_from_db()


class LogoUpdateTest(AbstractMembershipEditTest):
    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.media_root = Path(self.tempdir.name)
        self.overrider = override_settings(MEDIA_ROOT=self.media_root)
        self.overrider.enable()

        self.logo = Path(__file__).with_name('blender.png')
        self.logo2 = Path(__file__).with_name('half-file.png')

        super().setUp()

    def tearDown(self):
        super().tearDown()
        self.overrider.disable()
        self.tempdir.cleanup()

    def test_no_change(self):
        resp = self.client.post(self.url)
        self.assertIsRedirectToEditPage(resp)

        self.mem.refresh_from_db()
        self.assertFalse(self.mem.logo)

    def assertLogoStored(self):
        self.mem.refresh_from_db()
        logo_path = self.media_root / self.mem.logo.name
        found = '\n'.join(str(f) for f in sorted(self.media_root.rglob(f'*{logo_path.suffix}')))
        is_found = '' if logo_path.exists() else 'not '
        self.assertTrue(logo_path.exists(),
                        f'{self.logo.name} unexpectedly {is_found}found in {found}')

    def test_new_logo__not_allowed(self):
        with self.logo.open('rb') as logofile:
            resp = self.client.post(self.url, {'logo': logofile})
        self.assertIsRedirectToEditPage(resp)

        self.mem.refresh_from_db()
        self.assertFalse(self.mem.logo)

        self.assertFalse(list(self.media_root.rglob(f'*.png')))

    def test_new_logo__allowed(self):
        # Switch to a level that allows logo editing.
        self.switch_to_corporate()

        with self.logo.open('rb') as logofile:
            resp = self.client.post(self.url, {
                'display_name': 'Müüü',
                'logo': logofile,
                'url': '',
            })
        self.assertIsRedirectToEditPage(resp)

        self.mem.refresh_from_db()
        self.assertTrue(self.mem.logo)
        self.assertLogoStored()

    def test_replace_logo(self):
        self.test_new_logo__allowed()
        old_logo_path = self.media_root / self.mem.logo.name

        with self.logo.open('rb') as logofile:
            resp = self.client.post(self.url, {
                'display_name': 'Müüü',
                'logo': logofile,
            })
        self.assertIsRedirectToEditPage(resp)

        self.mem.refresh_from_db()
        self.assertTrue(self.mem.logo)
        self.assertLogoStored()

        self.assertFalse(old_logo_path.exists(),
                         f'Old logo {old_logo_path} should have been deleted')

    def test_update_other_info_after_existing_logo_exists(self):
        self.test_new_logo__allowed()
        old_logo_path = self.media_root / self.mem.logo.name

        resp = self.client.post(self.url, {
            'display_name': 'Müüü',
            'description': '',
            'url': 'https://dr.sergey.ru/',
            'logo': '',
        })
        self.assertIsRedirectToEditPage(resp)

        self.mem.refresh_from_db()
        self.assertTrue(self.mem.logo)
        self.assertLogoStored()
        self.assertTrue(old_logo_path.exists(), f'Old logo {old_logo_path} should have been kept')

    def test_delete_logo(self):
        self.test_new_logo__allowed()
        old_logo_path = self.media_root / self.mem.logo.name

        resp = self.client.post(self.url, {
            'display_name': 'Müüü',
            'logo-clear': 'on',
        })
        self.assertIsRedirectToEditPage(resp)

        self.mem.refresh_from_db()
        self.assertFalse(self.mem.logo)
        self.assertFalse(old_logo_path.exists())


class MembershipEditTest(AbstractMembershipEditTest):
    def test_update_privacy(self):
        resp = self.client.post(self.url, {'is_private': 'on'})
        self.assertIsRedirectToEditPage(resp)

        self.mem.refresh_from_db()
        self.assertTrue(self.mem.is_private)

        resp = self.client.post(self.url, {})
        self.assertIsRedirectToEditPage(resp)

        self.mem.refresh_from_db()
        self.assertFalse(self.mem.is_private)

    def test_update_display_name(self):
        self.switch_to_corporate()
        resp = self.client.post(self.url, {'display_name': 'Доктор Сергей'})
        self.assertIsRedirectToEditPage(resp)
        self.mem.refresh_from_db()
        self.assertEqual('Доктор Сергей', self.mem.display_name)

    def test_update_all_nonfile(self):
        self.switch_to_corporate()
        resp = self.client.post(self.url, {
            'display_name': 'Доктор Сергей',
            'description': 'Тайная пещера доктора Сергея',
            'url': 'https://dr.sergey.ru/',
        })
        self.assertIsRedirectToEditPage(resp)
        self.mem.refresh_from_db()
        self.assertEqual('Доктор Сергей', self.mem.display_name)
        self.assertFalse(self.mem.description)  # currently the field is nullable
        self.assertEqual('https://dr.sergey.ru/', self.mem.url)

    def test_update_then_erase(self):
        self.test_update_all_nonfile()

        resp = self.client.post(self.url, {
            'display_name': 'Доктор Сергей',
            'description': '',
            'url': '',
        })
        self.assertIsRedirectToEditPage(resp)
        self.mem.refresh_from_db()
        self.assertEqual('Доктор Сергей', self.mem.display_name)
        self.assertFalse(self.mem.description)  # currently the field is nullable
        self.assertEqual('', self.mem.url)

    def test_anonymous(self):
        self.client.logout()

        resp = self.client.get(self.url)
        self.assertEqual(302, resp.status_code)
        # This should redirect to some login page; we don't care much about the details.
        self.assertIn('login', resp['Location'])

        resp = self.client.post(self.url, {'is_private': 'on'})
        self.assertEqual(302, resp.status_code)
        # This should redirect to some login page; we don't care much about the details.
        self.assertIn('login', resp['Location'])

    def test_get_form(self):
        resp = self.client.get(self.url)
        self.assertEqual(resp.status_code, 200)

    def test_nonexistant_membership(self):
        nonexistant_id = models.Membership.objects.latest('id').id + 1
        url = reverse('settings_membership_edit', kwargs={'membership_id': nonexistant_id})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 404)

    def test_url_without_protocol(self):
        self.switch_to_corporate()
        resp = self.client.post(self.url, {
            'display_name': 'Доктор Сергей',
            'description': 'Тайная пещера доктора Сергея',
            'url': 'dr.sergey.ru',
        })
        self.assertIsRedirectToEditPage(resp)
        self.mem.refresh_from_db()
        self.assertEqual('http://dr.sergey.ru', self.mem.url)
