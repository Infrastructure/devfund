from django.contrib.auth.models import User
from django.urls import reverse

from looper.tests import AbstractLooperTestCase
from .. import models


class ChangeUserTest(AbstractLooperTestCase):
    gateway_name = 'bank'

    def test_change_user(self):
        new_user = User.objects.create_user(username='henkie', email='henkie@example.com')
        subs = self.create_active_subscription()
        mem: models.Membership = subs.membership

        subs.latest_order().generate_transaction()

        mem.customer = new_user.customer
        mem.save(update_fields={'customer'})

        # The subscription should be transferred, with orders and transactions.
        new_cid = new_user.customer.pk
        mem.refresh_from_db()
        subs.refresh_from_db()
        self.assertEqual(new_cid, mem.customer_id)
        self.assertEqual(new_cid, subs.customer_id)
        self.assertIsNone(subs.payment_method)
        self.assertEqual([new_cid], [o.customer_id for o in subs.order_set.all()])
        self.assertIsNone(subs.order_set.first().payment_method)
        self.assertEqual([new_cid], [t.customer_id
                                     for o in subs.order_set.all()
                                     for t in o.transaction_set.all()])

        # After this, getting the receipts (which iterates orders) should still work
        # for both users.
        self.client.force_login(self.user)
        resp = self.client.get(reverse('settings_receipts'))
        self.assertEqual(200, resp.status_code)

        self.client.force_login(new_user)
        resp = self.client.get(reverse('settings_receipts'))
        self.assertEqual(200, resp.status_code)
