from unittest.mock import patch

from django.contrib.auth.models import User
from django.dispatch import receiver
from django.urls import reverse
import django.core.mail

from looper.tests import AbstractLooperTestCase
from looper.tests.factories import SubscriptionFactory

from blender_fund_main.tests.utils import (
    expected_pdf_text_tmpl,
    extract_text_from_pdf_bytes,
    to_ap_style_month,
)
from blender_fund_main.utils import html_to_text
import blender_fund_main.models
import blender_fund_main.signals
import blender_fund_main.tasks as tasks


expected_email_text_activated_tmpl = '''Blender Development Fund membership Activated

Dear Harry de Bøker,
Your Blender Fund membership was activated! Thanks for your support.

You can always go to https://fund.local:8010/settings/membership/1 to view and update your membership.


--
Kind regards,
Blender Foundation'''  # noqa: E501
expected_email_text_deactivated_tmpl = '''Blender Development Fund membership Deactivated

Dear Harry de Bøker,
Your Blender Fund membership was deactivated.

You can always go to https://fund.local:8010/settings/membership/1 to view and update your membership.


--
Kind regards,
Blender Foundation'''  # noqa: E501


@patch(
    'blender_fund_main.tasks.send_mail_membership_status_changed',
    new=tasks.send_mail_membership_status_changed.task_function,
)
class MembershipActivationSignalTest(AbstractLooperTestCase):
    fixtures = ['systemuser', 'default_site', 'gateways', 'devfund']

    def setUp(self):
        self.user = User.objects.create_user('harry', 'harry@blender.org')
        self.full_name = 'Harry de Bøker'
        billing_address = self.user.customer.billing_address
        # We set full_name manually because IRL it's only set via
        # set_customer_fullname signal, which doesn't get called in this test
        # because it doesn't follow the OAuth flow.
        billing_address.full_name = self.full_name
        billing_address.save(update_fields={'full_name'})

    def assert_mail_ok(self) -> django.core.mail.message.EmailMultiAlternatives:
        the_mail: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[-1]
        self.assertIn(self.user.customer.billing_address.full_name, the_mail.body)
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        return the_mail

    def cancel_membership(self, mem: blender_fund_main.models.Membership):
        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(blender_fund_main.signals.membership_deactivated)
        def deactivated(sender: blender_fund_main.models.Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        # Go through the typical flow of cancelling a membership.
        self.client.force_login(self.user)
        cancel_url = reverse('settings_membership_cancel', kwargs={'membership_id': mem.pk})
        self.assertEqual(200, self.client.get(cancel_url).status_code)

        # This shouldn't do anything.
        resp = self.client.post(cancel_url, data={'confirm': False})
        self.assertEqual(200, resp.status_code)
        mem.refresh_from_db()

        # This should actually cancel the membership.
        resp = self.client.post(cancel_url, data={'confirm': True})
        self.assertEqual(302, resp.status_code)
        edit_url = reverse('settings_membership_edit', kwargs={'membership_id': mem.pk})
        self.assertEqual(edit_url, resp['Location'], 'Expected redirect to membership edit page')

        mem.refresh_from_db()
        return signal_count, signal_old_status, signal_new_status

    def test_create_activated(self):
        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(blender_fund_main.signals.membership_activated)
        def activated(sender: blender_fund_main.models.Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        mem = blender_fund_main.models.Membership.objects.create(
            level_id=1,
            status='active',
            display_name='your membership',
            customer=self.user.customer,
        )

        self.assertEqual('', signal_old_status)
        self.assertEqual('active', signal_new_status)
        self.assertEqual(1, signal_count)

        mem.status = 'active'
        mem.save()

        self.assertEqual(1, signal_count,
                         'Re-saving without modification of the status should not trigger signal')

        # An email should have been sent to the owner of the membership.
        self.assertEqual(1, len(django.core.mail.outbox))
        the_mail: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[0]
        self.assertIn(self.full_name, the_mail.body)
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        expected_email_text_activated = expected_email_text_activated_tmpl.format()
        self.assertEqual(the_mail.body, expected_email_text_activated, the_mail.body)
        self.assertEqual(html_to_text(alt0_body), expected_email_text_activated)

        self.assertEqual(len(the_mail.attachments), 0)

    def test_create_activated_with_subscription_and_order(self):
        new_order = SubscriptionFactory(
            customer=self.user.customer,
            payment_method__recognisable_name='Test payment method',
            plan_id=2,
            status='on-hold',
            price=1000,
        ).generate_order(save=False)

        new_order.status = 'paid'
        new_order.save()

        # An email should have been sent to the owner of the membership.
        self.assertEqual(1, len(django.core.mail.outbox))
        the_mail: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[0]
        self.assertIn(self.full_name, the_mail.body)
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        expected_email_text_activated = expected_email_text_activated_tmpl.format()
        self.assertEqual(the_mail.body, expected_email_text_activated, the_mail.body)
        self.assertEqual(html_to_text(alt0_body), expected_email_text_activated)

        # Check that email has an attachment: receipt PDF
        self.assertEqual(len(the_mail.attachments), 1)
        title, content, mime = the_mail.attachments[0]
        self.assertEqual(title, f'blender-development-fund-receipt-{new_order.pk}.pdf')
        self.assertEqual(mime, 'application/pdf')
        pdf_text = extract_text_from_pdf_bytes(content)
        self.assertEqual(
            pdf_text,
            expected_pdf_text_tmpl.format(
                order=new_order,
                expected_address=' Harry de Bøker',
                expected_currency_symbol='€',
                expected_date=to_ap_style_month(new_order.paid_at.strftime('%b. %-d, %Y')),
                expected_description=' Membership level\n Silver\n',
                expected_email='harry@blender.org',
                expected_payment_method='Test payment method',
                expected_total='10',
            ),
            pdf_text,
        )

    def test_activate_after_creation(self):
        signal_old_status = ''
        signal_new_status = ''
        signal_count = signal_deact_count = 0

        @receiver(blender_fund_main.signals.membership_activated)
        def activated(sender: blender_fund_main.models.Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        @receiver(blender_fund_main.signals.membership_deactivated)
        def deactivated(sender: blender_fund_main.models.Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_deact_count
            signal_deact_count += 1

        mem = blender_fund_main.models.Membership.objects.create(
            level_id=1,
            status='pending',
            display_name='your membership',
            customer=self.user.customer,
        )

        self.assertEqual('', signal_old_status)
        self.assertEqual('', signal_new_status)
        self.assertEqual(0, signal_count)
        self.assertEqual(0, signal_deact_count,
                         'Creating a non-active Membership should not send the deactivated signal')

        mem.status = 'active'
        mem.save()

        self.assertEqual('pending', signal_old_status)
        self.assertEqual('active', signal_new_status)
        self.assertEqual(1, signal_count)

        # An email should have been sent to the owner of the membership.
        self.assertEqual(1, len(django.core.mail.outbox))
        the_mail: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[0]
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        expected_email_text_activated = expected_email_text_activated_tmpl.format()
        self.assertEqual(the_mail.body, expected_email_text_activated, the_mail.body)
        self.assertEqual(html_to_text(alt0_body), expected_email_text_activated)
        self.assertEqual(len(the_mail.attachments), 0)

    def test_deactivate(self):
        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(blender_fund_main.signals.membership_deactivated)
        def deactivated(sender: blender_fund_main.models.Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        mem = blender_fund_main.models.Membership.objects.create(
            level_id=1,
            status='active',
            display_name='your membership',
            customer=self.user.customer,
        )

        self.assertEqual(0, signal_count)

        mem.status = 'inactive'
        mem.save()

        self.assertEqual('active', signal_old_status)
        self.assertEqual('inactive', signal_new_status)
        self.assertEqual(1, signal_count)

    def test_pending_to_inactive(self):
        signal_count = 0

        @receiver(blender_fund_main.signals.membership_activated)
        def activated(sender: blender_fund_main.models.Membership, **kwargs):
            nonlocal signal_count
            signal_count += 1

        @receiver(blender_fund_main.signals.membership_deactivated)
        def deactivated(sender: blender_fund_main.models.Membership, **kwargs):
            nonlocal signal_count
            signal_count += 1

        mem = blender_fund_main.models.Membership.objects.create(
            level_id=1,
            status='pending',
            display_name='your membership',
            customer=self.user.customer,
        )

        self.assertEqual(0, signal_count)

        mem.status = 'inactive'
        mem.save()

        self.assertEqual(0, signal_count)

        # No email should have been sent
        self.assertEqual(0, len(django.core.mail.outbox))

    def test_cancel_active_without_subscription(self):
        mem = blender_fund_main.models.Membership.objects.create(
            level_id=1,
            status='active',
            display_name='your membership',
            customer=self.user.customer,
        )

        # An email should have been sent to the owner of the membership.
        self.assertEqual(1, len(django.core.mail.outbox))

        signal_count, signal_old_status, signal_new_status = self.cancel_membership(mem)
        self.assertEqual('inactive', mem.status)

        # A signal about the deactivation should have been sent.
        self.assertEqual(1, signal_count)
        self.assertEqual('active', signal_old_status)
        self.assertEqual('inactive', signal_new_status)

        # Another email should have been sent to the owner of the membership.
        self.assertEqual(2, len(django.core.mail.outbox))

        # An email should have been sent to the owner of the membership.
        the_mail = self.assert_mail_ok()
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        expected_email_text_deactivated = expected_email_text_deactivated_tmpl.format()
        self.assertEqual(the_mail.body, expected_email_text_deactivated, the_mail.body)
        self.assertEqual(html_to_text(alt0_body), expected_email_text_deactivated)
        self.assertEqual(len(the_mail.attachments), 0)

    def test_cancel_active_with_subscription(self):
        subs = SubscriptionFactory(
            customer=self.user.customer,
            payment_method__recognisable_name='Test payment method',
            plan_id=2,
            status='active',
            price=1000,
        )
        mem = self.user.customer.memberships.first()

        # An email should have been sent to the owner of the membership.
        self.assertEqual(1, len(django.core.mail.outbox))
        signal_count, *_ = self.cancel_membership(mem)

        # This should not have cancelled the membership, as the subscription
        # goes to pending-cancellation.
        self.assertEqual('active', mem.status)
        self.assertEqual(0, signal_count)
        self.assertEqual(1, len(django.core.mail.outbox))
        self.assertEqual(subs.pk, mem.subscription.pk)
        self.assertEqual('pending-cancellation', mem.subscription.status)

        # An email should have been sent to the owner of the membership.
        the_mail = self.assert_mail_ok()
        self.assertEqual(len(the_mail.attachments), 0)

    def test_cancel_unpaid_subscription(self):
        subs = SubscriptionFactory(
            customer=self.user.customer,
            payment_method__recognisable_name='Test payment method',
            plan_id=2,
            status='on-hold',
            price=1000,
        )

        mem: blender_fund_main.models.Membership = self.user.customer.memberships.first()

        self.cancel_membership(mem)
        self.assertEqual('inactive', mem.status)
        self.assertEqual(subs.pk, mem.subscription.pk)
        self.assertEqual('cancelled', mem.subscription.status)

        # An email should have been sent to the owner of the membership.
        self.assertEqual(1, len(django.core.mail.outbox))
        the_mail = self.assert_mail_ok()

        # An email should have been sent to the owner of the membership.
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        expected_email_text_deactivated = expected_email_text_deactivated_tmpl.format()
        self.assertEqual(the_mail.body, expected_email_text_deactivated, the_mail.body)
        self.assertEqual(html_to_text(alt0_body), expected_email_text_deactivated)
        self.assertEqual(len(the_mail.attachments), 0)
