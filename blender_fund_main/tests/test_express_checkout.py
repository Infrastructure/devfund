# Uncomment this and the corresponding decorators on test methods to re-record live responses
# from responses import _recorder

from background_task.models import Task
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from freezegun import freeze_time

import looper.models
from looper.tests import ResponsesMixin

User = get_user_model()


class TestExpressCheckoutDonateOnce(ResponsesMixin, TestCase):
    responses_file_path = 'blender_fund_main/tests/stripe/'
    responses_files = ['express_checkout_donate_once.yaml']

    # @_recorder.record(file_path=f'{responses_file_path}/express_checkout_donate_once.yaml')
    def test_donate_once_redirect(self):
        amount = 1000
        currency = 'USD'
        tests = [
            {'amount': 1000, 'currency': 'USD', 'name': 'happy path'},
            {'amount': '', 'currency': '', 'name': 'bad params get fallback'},
            {'amount': 0, 'currency': '', 'name': 'zero gets fallback'},
        ]
        for t in tests:
            amount = t['amount']
            currency = t['currency']
            url = reverse(
                'express_checkout_donate_once'
            ) + f'?valueAmount={amount}&valueCurrency={currency}'
            resp = self.client.get(url)

            self.assertEqual(resp.status_code, 302, msg=t['name'])
            # Check that checkout session ID is saved in the request's session
            self.assertIn('stripe_session_id', self.client.session, msg=t['name'])
            self.assertTrue(
                self.client.session['stripe_session_id'].startswith('cs_test_'), msg=t['name']
            )

    def test_donate_once_missing_params_get_fallback(self):
        url = reverse('express_checkout_donate_once')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 302)

    def test_donate_once_unsupported_currency(self):
        amount = 1000
        currency = 'CNY'
        url = reverse(
            'express_checkout_donate_once'
        ) + f'?valueAmount={amount}&valueCurrency={currency}'
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 400)


class TestDonateOnceThankYou(ResponsesMixin, TestCase):
    responses_file_path = 'blender_fund_main/tests/stripe/'
    stripe_session_id = 'cs_test_a1pQ0CVeMOljMlHjB8fBmOCX1dLCt7lZYq5cB8Nx1jNf22f5OdwcSBL5eo'
    stripe_session_created = 1701548955
    maxDiff = None

    def test_get_thank_you_without_stripe_session_id(self):
        url = reverse('donate_once_thank_you')

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Thank you')

    # @_recorder.record(file_path=f'{responses_file_path}/donate_once_thank_you.yaml')
    @freeze_time('2023-12-02 20:30:00')
    def test_get_thank_you_with_stripe_session_id(self):
        session = self.client.session
        session['stripe_session_id'] = self.stripe_session_id
        session['stripe_session_created'] = self.stripe_session_created
        session.save()
        self.client.cookies[settings.SESSION_COOKIE_NAME] = session.session_key
        url = reverse('donate_once_thank_you')

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Thank you')

    # @_recorder.record(file_path=f'{responses_file_path}/donate_once_thank_you.yaml')
    @freeze_time('2023-12-02 20:30:00')
    def test_post_save_display_name_without_stripe_session_info_404(self):
        url = reverse('save_donation_display_name')

        resp = self.client.post(url)

        # If both stripe_session_{id,created} are missing it's 404
        self.assertEqual(resp.status_code, 404)

        # If only stripe_session_created is missing, still 404
        session = self.client.session
        session['stripe_session_id'] = self.stripe_session_id
        session.save()
        self.client.cookies[settings.SESSION_COOKIE_NAME] = session.session_key

        resp = self.client.post(url)

        self.assertEqual(resp.status_code, 404)

    @freeze_time('2023-12-02 20:30:00')
    def test_post_save_display_name_validation_errors(self):
        session = self.client.session
        session['stripe_session_id'] = self.stripe_session_id
        session['stripe_session_created'] = self.stripe_session_created
        session.save()
        self.client.cookies[settings.SESSION_COOKIE_NAME] = session.session_key
        url = reverse('save_donation_display_name')

        resp = self.client.post(url, {'display_name': 'blah' * 100})

        self.assertEqual(resp.status_code, 400)
        self.assertEqual(
            resp.json(),
            {
                'errors': {
                    'display_name': [
                        'Ensure this value has at most 70 characters (it has 400).'
                    ]
                }
            }
        )

    @freeze_time('2023-12-02 21:30:00')
    def test_post_save_display_name_validation_errors_too_late(self):
        session = self.client.session
        session['stripe_session_id'] = self.stripe_session_id
        session['stripe_session_created'] = self.stripe_session_created
        session.save()
        self.client.cookies[settings.SESSION_COOKIE_NAME] = session.session_key
        url = reverse('save_donation_display_name')

        resp = self.client.post(url, {'display_name': 'Jade Noe'})

        self.assertEqual(resp.status_code, 400)
        self.assertEqual(
            resp.json(),
            {'errors': {'display_name': ['Name can no longer be changed']}},
        )

    @freeze_time('2023-12-02 20:35:00')
    def test_post_save_display_name_happy(self):
        session = self.client.session
        session['stripe_session_id'] = self.stripe_session_id
        session['stripe_session_created'] = self.stripe_session_created
        session.save()
        self.client.cookies[settings.SESSION_COOKIE_NAME] = session.session_key
        url = reverse('save_donation_display_name')

        resp = self.client.post(url, {'display_name': 'Penny Horrendous'})

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(
            resp.json(),
            {'display_name': 'Penny Horrendous'},
        )
        self.assertEqual(Task.objects.count(), 1)
        task = Task.objects.first()
        self.assertEqual(task.task_name, 'blender_fund_main.tasks.update_donation_display_name')
        self.assertEqual(
            task.task_params,
            '[[], {"display_name": "Penny Horrendous", '
            f'"stripe_session_id": "{self.stripe_session_id}"'
            '}]',
        )
        self.assertEqual(str(task.run_at), '2023-12-02 20:35:00+00:00')

        # Making a POST with the same parameters doesn't duplicate the task
        resp = self.client.post(url, {'display_name': 'Penny Horrendous'})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(Task.objects.count(), 1)

        # Can be blank, which is equivalent to setting it to "Someone" in Activity
        resp = self.client.post(url, {'display_name': ''})

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json(), {'display_name': None})
        # Because parameters differed, new task was created
        self.assertEqual(Task.objects.count(), 2)

        # Empty data means the same: blank display name
        resp = self.client.post(url)

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json(), {'display_name': None})
        self.assertEqual(Task.objects.count(), 2)


class TestExpressCheckoutPlanVariationAnonymous(ResponsesMixin, TestCase):
    fixtures = ['gateways', 'devfund', 'systemuser']
    responses_file_path = 'blender_fund_main/tests/stripe/'
    responses_files = [
        'express_checkout_plan_variation_anonymous.yaml',
    ]

    # @_recorder.record(file_path=f'{responses_file_path}/express_checkout_plan_variation_anonymous.yaml')
    def test_anonymous(self):
        url = reverse('express_checkout_plan_variation', kwargs={'plan_variation': 1})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 302)
        order = looper.models.Order.objects.all().order_by('-id').first()
        self.assertIsNone(order)
        stripe_session_id = resp['Location'].split('#')[0].split('/')[-1]
        success_url = reverse(
            'stripe_success_create_subscription',
            kwargs={
                'stripe_session_id': stripe_session_id,
            },
        )

        # Uncomment the next 2 lines if responses have to be re-recorded
        # print('Continue to Stripe:', resp['Location'])
        # input('Press Enter when ready to continue')

        resp2 = self.client.get(success_url)
        self.assertEqual(resp2.status_code, 302)
        self.assertIn('?token=', resp2['Location'])
        order = looper.models.Order.objects.all().order_by('-id').first()
        self.assertIsNotNone(order.latest_transaction())
        self.assertNotEqual(order.customer.billing_address.email, '')
        self.assertNotEqual(order.email, '')
        self.assertIsNotNone(order.customer.token)

        # repeated visit of success url does nothing
        resp3 = self.client.get(success_url)
        self.assertEqual(resp2.url, resp3.url)
        order2 = looper.models.Order.objects.all().order_by('-id').first()
        self.assertEqual(order, order2)

    def test_bad_plan_variation_id(self):
        url = reverse('express_checkout_plan_variation', kwargs={'plan_variation': 100})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 404)


class TestExpressCheckoutPlanVariationLoggedIn(ResponsesMixin, TestCase):
    fixtures = ['gateways', 'devfund', 'systemuser']
    responses_file_path = 'blender_fund_main/tests/stripe/'
    responses_files = [
        'express_checkout_plan_variation_logged_in.yaml',
    ]

    # @_recorder.record(file_path=f'{responses_file_path}/express_checkout_plan_variation_logged_in.yaml')
    def test_logged_in(self):
        some_user = User.objects.create(email='joe@example.com')
        self.client.force_login(some_user)
        url = reverse('express_checkout_plan_variation', kwargs={'plan_variation': 2})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 302)
        order = looper.models.Order.objects.all().order_by('-id').first()
        self.assertIsNone(order)

        stripe_session_id = resp['Location'].split('#')[0].split('/')[-1]
        success_url = reverse(
            'stripe_success_create_subscription',
            kwargs={
                'stripe_session_id': stripe_session_id,
            },
        )

        # Uncomment the next 2 lines if responses have to be re-recorded
        # print('Continue to Stripe:', resp['Location'])
        # input('Press Enter when ready to continue')

        resp2 = self.client.get(success_url)
        self.assertEqual(resp2.status_code, 302)
        self.assertNotIn('?token=', resp2['Location'])
        order = looper.models.Order.objects.all().order_by('-id').first()
        self.assertEqual(order.customer, some_user.customer)
        self.assertFalse(hasattr(order.customer, 'token'))
        self.assertIsNotNone(order.latest_transaction())
        self.assertEqual(order.customer.billing_address.email, 'joe@example.com')
        self.assertEqual(order.email, 'joe@example.com')

        # repeated visit of success url does nothing
        resp3 = self.client.get(success_url)
        self.assertEqual(resp2.url, resp3.url)
        order2 = looper.models.Order.objects.all().order_by('-id').first()
        self.assertEqual(order, order2)
