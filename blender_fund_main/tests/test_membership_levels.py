import looper.models
from looper.money import Money
from looper.tests import AbstractLooperTestCase, AbstractBaseTestCase
from ..models import MembershipLevel


class MembershipLevelCase(AbstractBaseTestCase):
    def test_visible_attributes_set_none(self):
        ml = MembershipLevel.objects.create(name='Bronze', )
        self.assertEqual(ml.visible_attributes_set, set())

    def test_visible_attributes_set_existing(self):
        ml = MembershipLevel.objects.create(
            name='Bronze',
            visible_attributes='is_private, name',
        )
        self.assertEqual(ml.visible_attributes_set, {'is_private', 'name'})


class MembershipLevelWithFixturesTest(AbstractLooperTestCase):

    def test_price_per_day(self):
        plan = looper.models.Plan.objects.get(name='Corporate_Silver')

        # The fixtures have no corporate plan variation in USD, which gives us
        # the opportunity to try a unused-but-supported variation.
        looper.models.PlanVariation.objects.create(
            plan=plan,
            currency='USD',
            price=Money('USD', 52920),
            interval_unit='week',
            interval_length=2,
            collection_method='automatic',
            is_active=True,
            is_default_for_currency=False,
        )

        ml = MembershipLevel(name='your membership', plan=plan)
        self.assertEqual(Money('EUR', 100000), ml.price_per_month('EUR'))
        self.assertEqual(Money('USD', 114659), ml.price_per_month('USD'))
        self.assertIsNone(ml.price_per_month('HRK'))
