from unittest import mock

from django.contrib.auth import get_user_model
from django.urls import reverse
from freezegun import freeze_time
import background_task.models

from looper.tests.factories import OrderFactory
from looper.models import Address, Customer, Product
from looper.tests import AbstractLooperTestCase

import blender_fund_main.models as models
import blender_fund_main.tasks as tasks

User = get_user_model()


class LinkMembershipTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['mock-gateway']
    gateway_name = 'stripe'

    def setUp(self):
        self.get_default_plan()
        self.create_accountless_customer()
        self.subscription = self.create_active_accountless_subscription()
        self.url = reverse('link_membership', kwargs={'token': self.link_customer_token.token})

    def test_shows_cancelviatoken_if_not_logged_in(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<form class="form cancel-membership-form" method="post"')

    def test_cancelviatoken(self):
        url = reverse(
            'settings_membership_cancel_via_token',
            kwargs={'token': self.link_customer_token.token}
        )
        response = self.client.post(url, data={'confirm': 'True'})
        self.assertEqual(response.status_code, 302)
        self.subscription.refresh_from_db()

    def test_cancelviatoken_after_linking_user(self):
        some_user = User.objects.create(email='joe@example.com')
        self.client.force_login(some_user)
        response = self.client.post(self.url, data={'agree_to_link_membership': True})
        self.link_customer_token.refresh_from_db()
        self.assertIsNone(self.link_customer_token.customer)

        url = reverse(
            'settings_membership_cancel_via_token',
            kwargs={'token': self.link_customer_token.token}
        )
        response = self.client.post(url, data={'confirm': 'True'})
        self.assertEqual(response.status_code, 302)
        self.subscription.refresh_from_db()
        self.assertEqual(self.subscription.status, 'pending-cancellation')
        self.assertEqual(self.subscription.status, 'pending-cancellation')

    def test_get_displays_a_form(self):
        some_user = User.objects.create(email='joe@example.com')
        self.client.force_login(some_user)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Link membership')
        self.assertContains(response, '<form class="form link-membership-form" method="post">')

    def test_post_validation_error(self):
        some_user = User.objects.create(email='joe@example.com')
        self.client.force_login(some_user)

        # Checking the checkbox is required
        r = self.client.post(self.url, data={})
        self.assertEqual(200, r.status_code)
        self.assertEqual(
            {'agree_to_link_membership': ['This field is required.']},
            r.context['form'].errors,
        )

    @mock.patch('blender_fund_main.badges.change_badge')
    @mock.patch(
        'blender_fund_main.tasks.grant_or_revoke_badge',
        new=tasks.grant_or_revoke_badge.task_function,
    )
    def test_post_replaces_customer_and_redirects_to_account_membership_list(
        self, mock_change_badge
    ):
        # Badge should not yet been granted (there's no one to grant it to yet)
        mock_change_badge.assert_not_called()
        membership = self.subscription.membership
        accountless_customer_id = membership.customer_id
        self.assertTrue(Customer.objects.filter(id=accountless_customer_id).exists())
        some_user = User.objects.create(email='joe@example.com', username='newnickname')
        # This membership is currently linked to an account-less customer
        self.assertNotEqual(membership.customer_id, some_user.customer.pk)
        self.assertNotEqual(self.subscription.customer_id, some_user.customer.pk)
        self.assertNotEqual(self.subscription.order_set.first().customer_id, some_user.customer.pk)
        self.assertIsNone(self.subscription.customer.user)
        self.client.force_login(some_user)
        total_customers = Customer.objects.count()
        total_addresses = Address.objects.count()

        response = self.client.post(self.url, data={'agree_to_link_membership': True})

        # Successfully claiming a membership should result in a redirect to account settings
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/settings/')
        self.subscription.refresh_from_db()
        # Check that a Customer and Address were deleted
        self.assertEqual(total_customers - 1, Customer.objects.count())
        self.assertEqual(total_addresses - 1, Address.objects.count())
        # Check that subscription is now linked to the account
        self.assertIsNotNone(self.subscription.customer.user)
        self.assertEquals(self.subscription.customer.user, some_user)
        order = self.subscription.order_set.first()
        self.assertEquals(order.customer, some_user.customer)
        self.assertEquals(order.payment_method.customer, some_user.customer)
        transaction = order.transaction_set.first()
        self.assertEquals(transaction.customer, some_user.customer)
        self.assertEquals(transaction.payment_method.customer, some_user.customer)
        self.assertEquals(self.subscription.payment_method.customer, some_user.customer)

        # Badge should have been assigned
        mock_change_badge.assert_called_once_with(user=some_user, revoke='', grant='devfund_gold')

        # Existing membership and all its records should be re-linked to a different customer
        membership.refresh_from_db()
        self.subscription.refresh_from_db()
        self.assertNotEqual(membership.customer_id, accountless_customer_id)
        self.assertNotEqual(self.subscription.customer_id, accountless_customer_id)
        self.assertNotEqual(self.subscription.latest_order().customer_id, accountless_customer_id)
        self.assertNotEqual(some_user.customer.pk, accountless_customer_id)
        self.assertEqual(membership.customer_id, some_user.customer.pk)
        self.assertEqual(self.subscription.customer_id, some_user.customer.pk)
        self.assertEqual(self.subscription.order_set.first().customer_id, some_user.customer.pk)

        # The account-less customer no longer exists
        self.assertFalse(Customer.objects.filter(id=accountless_customer_id).exists())

    @freeze_time('2023-12-03 20:30:00')
    def test_post_replaces_customer_and_grants_membership_and_campaign_badges(self):
        models.Campaign.objects.create(
            starts_at='2023-12-01 00:00:00Z',
            ends_at='2023-12-10 00:00:00Z',
            badge='campaign-badge',
            is_active=True,
        )
        membership = self.subscription.membership
        order = OrderFactory(
            customer=membership.customer,
            subscription=None,
            product=Product.objects.first(),
        )
        order.status = 'paid'
        order.save(update_fields={'status'})
        accountless_customer_id = membership.customer_id
        self.assertEqual(order.campaign_set.count(), 1)
        self.assertIsNone(order.subscription_id)
        self.assertEqual(order.customer_id, accountless_customer_id)
        some_user = User.objects.create(email='joe@example.com', username='newnickname')
        tasks_q = background_task.models.Task.objects.order_by('-pk')
        tasks_before = tasks_q.count()

        self.client.force_login(some_user)
        response = self.client.post(self.url, data={'agree_to_link_membership': True})

        # Successfully claiming a membership should result in a redirect to account settings
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/settings/')

        # Successfully claiming a membership should result in a redirect to account settings
        self.subscription.refresh_from_db()

        self.assertEqual(tasks_q.count(), tasks_before + 2)
        # A membership badge was grated to the account based on membership level
        self.assertEqual(tasks_q[1].task_name, 'blender_fund_main.tasks.grant_or_revoke_badge')
        self.assertEqual(
            tasks_q[1].task_params,
            f'[[], {{"grant": "devfund_gold", "revoke": "", "user_id": {some_user.pk}}}]',
        )
        # A campaign badge was grated to the account based on an campaign order
        self.assertEqual(tasks_q[0].task_name, 'blender_fund_main.tasks.grant_or_revoke_badge')
        self.assertEqual(
            tasks_q[0].task_params,
            f'[[], {{"grant": "campaign-badge", "user_id": {some_user.pk}}}]',
        )

        # The account-less customer no longer exists
        self.assertFalse(Customer.objects.filter(id=accountless_customer_id).exists())
