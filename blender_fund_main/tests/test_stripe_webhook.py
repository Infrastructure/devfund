from unittest import mock
import os

import responses
# Uncomment this and the corresponding decorators on test methods to re-record live responses
# from responses import _recorder

from background_task.models import Task
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time
from looper.models import Customer, Order, Transaction
from looper.tests import AbstractBaseTestCase
import django.core.mail

from blender_fund_main.tests.utils import (
    expected_pdf_text_tmpl,
    extract_text_from_pdf_bytes,
    to_ap_style_month,
)
from blender_fund_main.utils import html_to_text
import blender_fund_main.models as models
import blender_fund_main.tasks as tasks

responses_file_path = 'blender_fund_main/tests/stripe/'
event_payload_cc = {
    # Most of the event payload is not used by our webhook handler:
    # it retrieves PaymentIntent from the API instead because event
    # doesn't carry enough meaningful data.
    'type': 'payment_intent.succeeded',
    'data': {
        'object': {
            'id': 'pi_3O82BkGxwuSUrRMt1am7Cvmv',
            'object': 'payment_intent',
            'latest_charge': 'ch_3O82BkGxwuSUrRMt1QbB3BoM',
            'metadata': {},
            'payment_method': 'pm_1O82BjGxwuSUrRMtjF2TKhzE',
            'payment_method_types': ['card'],
        }
    }
}
expected_email_body_tmpl = '''Blender Development Fund: payment received

Dear Jane Doe,
Thank you for donating to the Blender Development Fund!
To view your donation receipt follow the link below and sign in with your Blender ID.
https://fund.local:8010/link-donation/{expected_token}/

Payment information:

Receipt ID: 1
Amount: € 2
Payment method: Visa credit card ending in 4242
Date: {expected_date}
Description: Blender Development Fund Donation


Billing information:


test@example.com
Jane Doe
Netherlands

Your donations help dozens of people to work on Blender development, online infrastructure and documentation.
Check our annual reports and the grants overview at https://fund.blender.org/grants/.



--
Kind regards,
Blender Foundation'''  # noqa: E501


class ResponsesMixin(object):
    def setUp(self):
        responses.start()
        # Load previously recorded Stripe responses from YAML files
        for file_name in os.listdir(responses_file_path):
            if not file_name.endswith('.yaml'):
                continue
            responses._add_from_file(file_path=f'{responses_file_path}{file_name}')
        super(ResponsesMixin, self).setUp()

    def tearDown(self):
        super(ResponsesMixin, self).tearDown()
        responses.stop()
        responses.reset()


class WebhookTestCase(AbstractBaseTestCase):
    fixtures = ['gateways', 'devfund']
    webhook_url = reverse('webhook_stripe')

    def test_get_method_not_allowed(self):
        resp = self.client.get(self.webhook_url)

        self.assertEquals(resp.status_code, 405)

    def test_post_no_signature(self):
        resp = self.client.post(self.webhook_url, {})

        self.assertEquals(resp.status_code, 401)

    def test_post_wrong_signature(self):
        resp = self.client.post(
            self.webhook_url,
            {},
            HTTP_STRIPE_SIGNATURE='foobar',
        )

        self.assertEquals(resp.status_code, 400)

    @mock.patch('looper.gateways.stripe.webhook.WebhookSignature.verify_header', return_value=True)
    def test_post_payment_intent_succeeded_with_valid_signature_creates_a_task(self, _):
        now = timezone.now()
        self.assertEquals(Task.objects.count(), 0)
        payload = event_payload_cc.copy()

        resp = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEquals(resp.status_code, 200)
        self.assertEquals(Task.objects.count(), 1)

        # Repeated call doesn't create another task
        resp = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEquals(resp.status_code, 200)
        self.assertEquals(Task.objects.count(), 1)
        task = Task.objects.first()
        self.assertEqual(task.task_name, 'blender_fund_main.tasks.handle_payment_intent_succeeded')
        # Task is delayed 30 seconds
        self.assertEqual(int((task.run_at - now).total_seconds()), 30)


@mock.patch(
    # Make sure background task is executed as a normal function
    'blender_fund_main.views.webhooks.handle_payment_intent_succeeded',
    new=tasks.handle_payment_intent_succeeded.task_function,
)
@mock.patch(
    'blender_fund_main.tasks.send_mail_donation_received',
    new=tasks.send_mail_donation_received.task_function,
)
@mock.patch('looper.gateways.stripe.webhook.WebhookSignature.verify_header', return_value=True)
class WebhookPostCreatesOrderAndTransactionTestCase(ResponsesMixin, AbstractBaseTestCase):
    fixtures = ['default_site', 'gateways', 'devfund', 'systemuser']
    webhook_url = reverse('webhook_stripe')

    # @_recorder.record(file_path=f'{responses_file_path}/stripe_payment_intent__card.yaml')
    def test_payment_intent_succeeded__credit_card(self, _):
        total_orders = Order.objects.count()
        total_transactions = Transaction.objects.count()
        total_customers = Customer.objects.count()
        payload = event_payload_cc.copy()

        resp = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEquals(resp.status_code, 200)

        self.assertEquals(total_orders + 1, Order.objects.count())
        self.assertEquals(total_transactions + 1, Transaction.objects.count())
        self.assertEquals(total_customers + 1, Customer.objects.count())

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        order = transaction.order
        self.assertEquals(transaction.status, 'succeeded')
        self.assertEquals(transaction.amount.cents, 200)
        self.assertEquals(transaction.amount.currency, 'EUR')
        self.assertIsNotNone(order.pi_id)
        self.assertEquals(order.pi_id, payment_intent_id)
        self.assertEquals(order.status, 'paid')
        self.assertEquals(order.name, 'Blender Development Fund Donation')
        self.assertEquals(order.email, 'test@example.com')
        self.assertEquals(order.billing_address, 'Jane Doe\nNetherlands')
        self.assertTrue(hasattr(transaction.customer, 'token'))
        self.assertEquals(transaction.customer.billing_address.email, 'test@example.com')
        self.assertEquals(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEquals(payment_method.method_type, 'cc')
        self.assertEquals(payment_method.recognisable_name, 'visa credit card ending in 4242')

        self.assertEqual(1, len(django.core.mail.outbox))
        email: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[0]
        token = order.customer.token.token
        expected_email_body = expected_email_body_tmpl.format(
            expected_token=token,
            expected_date=order.paid_at.strftime('%Y-%m-%d'),
        )
        self.assertEqual(email.body, expected_email_body, email.body)
        alt0_body, alt0_type = email.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(html_to_text(alt0_body), expected_email_body, html_to_text(alt0_body))

        # Check that receipt PDF is attached to the email
        self.assertEqual(len(email.attachments), 1)
        title, content, mime = email.attachments[0]
        self.assertEqual(title, f'blender-development-fund-receipt-{order.pk}.pdf')
        self.assertEqual(mime, 'application/pdf')
        pdf_text = extract_text_from_pdf_bytes(content)
        self.assertEqual(
            pdf_text,
            expected_pdf_text_tmpl.format(
                order=order,
                expected_address=' Jane Doe\nNetherlands',
                expected_currency_symbol='€',
                expected_date=to_ap_style_month(order.paid_at.strftime('%b. %-d, %Y')),
                expected_description=' Description\n ',
                expected_email='test@example.com',
                expected_payment_method='visa credit card ending in 4242',
                expected_total='2',
            ),
            pdf_text,
        )

    # @_recorder.record(file_path=f'{responses_file_path}/stripe_payment_intent__ideal.yaml')
    def test_payment_intent_succeeded__ideal(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9QDEGxwuSUrRMt0UsPcCBQ',
                    'object': 'payment_intent',
                    'latest_charge': 'py_3O9QDEGxwuSUrRMt0RSh3jXQ',
                    'metadata': {},
                    'payment_method': 'pm_1O9QDEGxwuSUrRMtsVZHf6yr',
                    'payment_method_types': ['ideal'],
                }
            }
        }

        resp = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEquals(resp.status_code, 200)

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self.assertEquals(transaction.status, 'succeeded')
        self.assertEquals(transaction.amount.cents, 400)
        self.assertEquals(transaction.amount.currency, 'EUR')
        self.assertIsNotNone(transaction.order.pi_id)
        self.assertEquals(transaction.order.pi_id, payment_intent_id)
        self.assertEquals(transaction.order.status, 'paid')
        self.assertEquals(transaction.order.name, 'Blender Development Fund Donation')
        self.assertEquals(transaction.order.email, 'ideal@example.com')
        self.assertEquals(transaction.order.billing_address, 'Jane Doe')
        self.assertTrue(hasattr(transaction.customer, 'token'))
        self.assertEquals(transaction.customer.billing_address.email, 'ideal@example.com')
        self.assertEquals(transaction.customer.billing_address.full_name, 'Jane Doe')
        payment_method = transaction.payment_method
        self.assertEquals(payment_method.method_type, 'ideal')
        self.assertEquals(
            payment_method.recognisable_name,
            'RABONL2U**********5264 via iDEAL',
        )

    # @_recorder.record(file_path=f'{responses_file_path}/stripe_payment_intent__paypal.yaml')
    def test_payment_intent_succeeded__paypal(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3O9WfTGxwuSUrRMt1DO8XtnR',
                    'object': 'payment_intent',
                    'latest_charge': 'py_3O9WfTGxwuSUrRMt1NpTvxr2',
                    'metadata': {},
                    'payment_method': 'pm_1O9WmfGxwuSUrRMtDGNVPrxO',
                    'payment_method_types': ['paypal'],
                }
            }
        }

        resp = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEquals(resp.status_code, 200)

        charge_id = payload['data']['object']['latest_charge']
        payment_intent_id = payload['data']['object']['id']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        self.assertEquals(transaction.status, 'succeeded')
        self.assertEquals(transaction.amount.cents, 400)
        self.assertEquals(transaction.amount.currency, 'EUR')
        self.assertIsNotNone(transaction.order.pi_id)
        self.assertEquals(transaction.order.pi_id, payment_intent_id)
        self.assertEquals(transaction.order.status, 'paid')
        self.assertEquals(transaction.order.name, 'Blender Development Fund Donation')
        self.assertEquals(transaction.order.email, 'sb-j1a84328066132@personal.example.com')
        self.assertEquals(transaction.order.billing_address, 'John Doe\nUnited States of America')
        self.assertTrue(hasattr(transaction.customer, 'token'))
        self.assertEquals(
            transaction.customer.billing_address.email, 'sb-j1a84328066132@personal.example.com'
        )
        self.assertEquals(transaction.customer.billing_address.full_name, 'John Doe')
        payment_method = transaction.payment_method
        self.assertEquals(payment_method.method_type, 'pa')
        self.assertEquals(
            payment_method.recognisable_name,
            'PayPal account sb-j1a84328066132@personal.example.com',
        )

    # @_recorder.record(file_path=f'{responses_file_path}/stripe_payment_intent__card.yaml')
    @freeze_time('2023-12-03 20:30:00')
    def test_payment_intent_succeeded_campaign_email_mentions_badge(self, _):
        models.Campaign.objects.create(
            starts_at='2023-10-01 00:00:00Z',
            ends_at='2024-01-01 00:00:00Z',
            badge='campaign-badge',
            is_active=True,
        )

        payload = event_payload_cc.copy()
        resp = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEquals(resp.status_code, 200)

        charge_id = payload['data']['object']['latest_charge']
        transaction = Transaction.objects.get(transaction_id=charge_id)
        # Order should have been added to the campaign
        self.assertEquals(transaction.order.campaign_set.count(), 1)
        self.assertEqual(1, len(django.core.mail.outbox))
        email: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[0]
        self.assertIn('Jane Doe', email.body)
        self.assertIn(str(transaction.order.pk), email.body)
        self.assertIn('Visa credit card ending in 4242', email.body)
        self.assertIn('Thank you for donating', email.body)
        token = transaction.order.customer.token.token
        self.assertIn(f'/link-donation/{token}/', email.body)
        alt0_body, alt0_type = email.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertIn(
            'To view your donation receipt and to <b>claim your badge</b> follow the link ',
            alt0_body,
        )
        self.assertIn(
            'To view your donation receipt and to claim your badge follow the link ',
            email.body,
        )

    # @_recorder.record(file_path=f'{responses_file_path}/checkout_plan_variation_webhook.yaml')
    def test_payment_intent_succeeded__create_subscription(self, _):
        payload = {
            'type': 'payment_intent.succeeded',
            'data': {
                'object': {
                    'id': 'pi_3OOg58Kh8wGqs1ax1RoXFsDP',
                    'object': 'payment_intent',
                    'metadata': {'plan_variation_id': 1},
                    'payment_method': 'pm_1OOg58Kh8wGqs1axLHnW7hMw',
                }
            }
        }
        payment_intent_id = 'pi_3OOg58Kh8wGqs1ax1RoXFsDP'

        resp = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEquals(resp.status_code, 200)

        order = Order.objects.filter(
            gateway_order_ids__gateway_id=3,
            gateway_order_ids__gateway_order_id=payment_intent_id,
        ).get()
        transaction = order.latest_transaction()

        self.assertEquals(transaction.status, 'succeeded')
        self.assertIsNotNone(order.pi_id)
        self.assertEquals(order.pi_id, payment_intent_id)
        self.assertEquals(order.status, 'paid')
        self.assertEquals(order.subscription.status, 'active')
