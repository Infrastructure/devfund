# from unittest import mock
import copy
import datetime
import logging

from dateutil.relativedelta import relativedelta
from django.urls import reverse
from django.utils.timezone import utc
import responses
# from responses import _recorder

from looper.models import Transaction, Subscription
from looper.tests import AbstractLooperTestCase

log = logging.getLogger(__name__)
responses_file_path = 'blender_fund_main/tests/stripe/'


class MembershipExtendTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['mock-gateway']
    gateway_name = 'stripe'

    def setUp(self):
        super().setUp()
        responses._add_from_file(file_path=f'{responses_file_path}/extend_pay_47_00_eur.yaml')
        responses.start()

        self.subs = self.create_active_subscription()
        self.subs.next_payment = datetime.datetime(2200, 1, 2, 3, 4, 5, tzinfo=utc)
        self.subs.save()

        self.mem = self.subs.membership
        self.url = reverse('membership_extend', kwargs={'membership_id': self.mem.id})
        self.client.force_login(self.user)

    def tearDown(self):
        super().tearDown()
        responses.stop()
        responses.reset()

    def test_get_not_allowed(self):
        # This view only expects POST
        resp = self.client.get(self.url)
        self.assertEqual(405, resp.status_code)

    # @_recorder.record(file_path=f'{responses_file_path}/extend_pay_47_00_eur.yaml')
    def test_extend_happy(self):
        orig_next_payment = copy.copy(self.mem.subscription.next_payment)
        self.assertEqual(self.subs.order_set.count(), 1)
        self.assertEqual(Transaction.objects.count(), 0)

        payload = {
            'currency': self.subs.currency,
            'price': '47.00',
        }
        resp = self.client.post(self.url, payload)

        self.assertEqual(302, resp.status_code)
        self.assertTrue('/c/pay/cs_test' in resp['Location'])
        # A new order was created
        self.assertEqual(self.subs.order_set.count(), 2)
        order = self.subs.latest_order()
        self.assertEqual(4700, order.price.cents)
        self.assertEqual('EUR', order.currency)
        # No charge has happened yet
        self.assertIsNone(order.latest_transaction())
        # A gateway customer was created and stored
        gateway_customer = order.customer.gatewaycustomerid_set.get(gateway__name='stripe')
        self.assertEqual('cus_P33LMwDVnLnXtZ', gateway_customer.gateway_customer_id)

        # Hacks follow: this flow happens in 2 different views separated by a Stripe payment page.
        # 1) Replace metadata's "order_id" hardcoded in the response YAML with current order ID,
        # because it differs depending on whether this test is run alone or with all the tests.
        for _ in responses.registered():
            if 'expand%5B0%5D=payment_intent' in _.url:
                assert '\"order_id\": \"2' in _.body
                _.body = _.body.replace('\"order_id\": \"2', f'\"order_id\": \"{order.pk}')

        # 2) Parse the Stripe's session ID from the redirect URL we got.
        stripe_session_id = resp['Location'].split('#')[0].split('/')[-1]
        success_url = reverse(
            'membership_extend_done',
            kwargs={
                'membership_id': self.mem.id,
                'stripe_session_id': stripe_session_id,
            },
        )

        # Uncomment the next 2 lines if responses have to be re-recorded
        # print('Continue to Stripe:', resp['Location'])
        # input('Press Enter when ready to continue')

        # Next step happens after Stripe page redirects to success_url
        resp = self.client.get(success_url)
        self.assertEqual(302, resp.status_code)
        self.assertEqual(f'/settings/membership/{self.mem.pk}', resp['Location'])
        self.assertEqual(self.subs.order_set.count(), 2)
        # A transaction was created
        self.assertEqual(Transaction.objects.count(), 1)

        order.refresh_from_db()
        self.subs.refresh_from_db()

        # The membership is for €25 per month and paid €47.
        bought = relativedelta(months=1, days=26, hours=18, minutes=2, seconds=52)
        self.assertAlmostEqualDateTime(orig_next_payment + bought, self.subs.next_payment)

        gateway_order_id = order.gateway_order_ids.get(gateway__name='stripe').gateway_order_id
        self.assertEqual('pi_3OExFDKh8wGqs1ax0sacoTGq', gateway_order_id)
        transaction = order.latest_transaction()
        self.assertEqual('ch_3OExFDKh8wGqs1ax0znFYtbN', transaction.transaction_id)
        self.assertEqual('Unscheduled', transaction.get_source_display())
        self.assertEqual('succeeded', transaction.status)
        self.assertTrue(transaction.paid)
        payment_method = transaction.payment_method
        self.assertEqual('cc', payment_method.method_type)
        self.assertEqual('visa credit card ending in 4242', payment_method.recognisable_name)
        # Subscription still should have a different payment method
        self.assertNotEqual(payment_method.pk, self.subs.payment_method.pk)
        self.assertEqual(payment_method.pk, order.payment_method.pk)
        self.assertEqual('stripe', payment_method.gateway.name)
        self.assertEqual('pm_1OExFCKh8wGqs1axd4Hhro2x', payment_method.token)

        # Visiting the same URL does nothing
        resp = self.client.get(success_url)
        self.assertEqual(302, resp.status_code)
        self.assertEqual(f'/settings/membership/{self.mem.pk}', resp['Location'])
        self.assertEqual(self.subs.order_set.count(), 2)
        self.assertEqual(Transaction.objects.count(), 1)

    # @_recorder.record(file_path=f'{responses_file_path}/extend_pay_47_00_eur.yaml')
    def test_extend_no_payment_method_present_still_happy(self):
        self.assertIsNotNone(self.mem.subscription.payment_method)
        # Extend must work even though payment method is not set on the subscription
        Subscription.objects.filter(pk=self.mem.subscription_id).update(payment_method=None)
        self.mem.subscription.refresh_from_db()
        self.assertIsNone(self.mem.subscription.payment_method)
        self.assertEqual(self.subs.order_set.count(), 1)
        self.assertEqual(Transaction.objects.count(), 0)

        payload = {
            'currency': self.subs.currency,
            'price': '47.00',
        }
        resp = self.client.post(self.url, payload)

        self.assertEqual(302, resp.status_code)
        self.assertTrue('/c/pay/cs_test' in resp['Location'])

    def test_extend_invalid_customer_ip_address(self):
        resp = self.client.post(
            self.url,
            {
                'currency': self.subs.currency,
                'price': '47.00',
            },
            REMOTE_ADDR='bogus',
            HTTP_X_FORWARDED_FOR='bogus',
        )
        self.assertEqual(400, resp.status_code)

    # TODO: test for extending with an existing order or drop support for this entirely
    # TODO: test for subscriptionless memberships.
    # TODO: test for next_payment in the past.
