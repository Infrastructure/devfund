from django.contrib.auth import get_user_model
from django.urls import reverse
from freezegun import freeze_time
import background_task.models

from looper.models import (
    Customer,
    Gateway,
    GatewayOrderId,
    Order,
    PaymentMethod,
    Product,
)
from looper.tests import AbstractLooperTestCase
from looper.tests.factories import OrderFactory

import blender_fund_main.models as models

User = get_user_model()


class LinkDonationTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['mock-gateway']
    gateway_name = 'mock'

    def setUp(self):
        self.create_accountless_customer()
        gateway = Gateway.objects.get(name='mock')
        product = Product.objects.filter(name__contains='Donation').get()
        pm = PaymentMethod.objects.create(
            customer=self.accountless_customer,
            token='foobar',
            gateway=gateway,
            recognisable_name='test payment method',
        )
        self.order = Order.objects.create(
            customer=self.accountless_customer,
            product=product,
            payment_method=pm,
            name=product.name,
            price=3000,
            status='paid',
            email='joe-one-time@example.com',
        )
        tr = self.order.generate_transaction()
        tr.source = None
        tr.status = 'succeeded'
        tr.transaction_id = 'mock-transaction-id'
        tr.save()
        GatewayOrderId.objects.create(
            gateway=gateway,
            gateway_order_id='mock-gateway-order-id',
            order=self.order,
        )
        self.url = reverse('link_donation', kwargs={'token': self.link_customer_token.token})

    def test_get_redirects_if_not_logged_in(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response['Location'],
            f'/oauth/login?next=/link-donation/{self.link_customer_token.token}/',
        )

    def test_get_displays_a_form(self):
        some_user = User.objects.create(email='joe@example.com')
        self.client.force_login(some_user)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Link donation')
        self.assertContains(response, '<form class="form link-donation-form" method="post">')

    def test_post_replaces_customer_and_redirects_to_receipts_list(self):
        self.assertIsNone(self.order.customer.user)
        some_user = User.objects.create(email='joe@example.com')
        self.client.force_login(some_user)
        total_customers = Customer.objects.count()

        response = self.client.post(self.url, data={'agree_to_link_donation': True})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/settings/receipts/')
        self.order.refresh_from_db()
        # Check that a Customer were deleted
        self.assertEqual(total_customers - 1, Customer.objects.count())
        # Check that order is now linked to the account
        self.assertIsNotNone(self.order.customer.user)
        self.assertEquals(self.order.customer.user, some_user)
        self.assertEquals(self.order.payment_method.customer, some_user.customer)
        transaction = self.order.transaction_set.first()
        self.assertEquals(transaction.customer, some_user.customer)
        self.assertEquals(transaction.payment_method.customer, some_user.customer)
        self.assertEquals(self.order.payment_method.customer, some_user.customer)

    @freeze_time('2023-12-03 20:30:00')
    def test_post_replaces_customer_and_grants_badges(self):
        models.Campaign.objects.create(
            starts_at='2023-12-01 00:00:00Z',
            ends_at='2023-12-10 00:00:00Z',
            badge='campaign-badge',
            is_active=True,
        )
        models.Campaign.objects.create(
            slug='campaign-that-is-not-affecting-any-orders',
            starts_at='2022-12-01 00:00:00Z',
            ends_at='2022-12-10 00:00:00Z',
            badge='foobar',
            is_active=True,
        )
        self.assertIsNone(self.order.customer.user)
        order = OrderFactory(
            customer=self.order.customer,
            subscription=None,
            product=Product.objects.first(),
        )
        order.status = 'paid'
        order.save(update_fields={'status'})

        some_user = User.objects.create(email='joe@example.com')
        self.client.force_login(some_user)
        response = self.client.post(self.url, data={'agree_to_link_donation': True})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/settings/receipts/')

        # A campaign badge was grated to the account based on an campaign order
        self.assertEqual(background_task.models.Task.objects.count(), 1)
        self.assertEqual(
            background_task.models.Task.objects.first().task_params,
            f'[[], {{"grant": "campaign-badge", "user_id": {some_user.pk}}}]',
        )
