from unittest.mock import patch

from django.contrib.auth.models import User
from django.urls import reverse
import django.core.mail

from looper.models import Subscription
from looper.tests import AbstractBaseTestCase

from ..models import Membership
from blender_fund_main.utils import absolute_url, html_to_text
import blender_fund_main.tasks as tasks

expected_mail_body = '''Blender Development Fund membership Bank Payment

Dear Erik von Namenstein,

Thank you for joining the Blender Development Fund.
You chose to pay for your Silver membership by bank transfer,
which means that we will be waiting for you to perform this transfer.

Your membership will be activated as soon as we have handled your bank transfer.
When paying, please mention the following:

Blender Fund Membership subs-1

Please send your payment of € 10.00 to:

Stichting Blender Foundation
Buikslotermeerplein 161
1025 ET Amsterdam, the Netherlands

Bank: ING Bank
Bijlmerdreef 109
1102 BW Amsterdam, the Netherlands

BIC/Swift code: INGB NL 2A
IBAN: NL45 INGB 0009356121 (for Euro countries)

Tax number NL 8111.66.223


You can always go to https://fund.local:8010/settings/membership/1 to view and update your membership.


--
Kind regards,
Blender Foundation'''  # noqa: E501
expected_mail_w_token_body = '''Blender Development Fund membership Bank Payment

Dear Erik von Namenstein,

Thank you for joining the Blender Development Fund.
You chose to pay for your Silver membership by bank transfer,
which means that we will be waiting for you to perform this transfer.

One more step: to manage or cancel this membership and claim your Silver badge,
follow the link below and sign in with your Blender ID.
https://fund.local:8010/link-membership/{token}/
Your membership will be activated as soon as we have handled your bank transfer.
When paying, please mention the following:

Blender Fund Membership subs-1

Please send your payment of € 10.00 to:

Stichting Blender Foundation
Buikslotermeerplein 161
1025 ET Amsterdam, the Netherlands

Bank: ING Bank
Bijlmerdreef 109
1102 BW Amsterdam, the Netherlands

BIC/Swift code: INGB NL 2A
IBAN: NL45 INGB 0009356121 (for Euro countries)

Tax number NL 8111.66.223



--
Kind regards,
Blender Foundation'''


@patch(
    'blender_fund_main.tasks.send_mail_membership_status_changed',
    new=tasks.send_mail_membership_status_changed.task_function,
)
class CheckoutTestCase(AbstractBaseTestCase):
    fixtures = ['default_site', 'gateways', 'devfund', 'systemuser']

    def setUp(self):
        self.user = User.objects.create_user('explodingchicken', 'chicken@example.com')

    def _test_bank_creates_inactive_membership(self):
        total_subscriptions = Subscription.objects.count()
        total_memberships = Membership.objects.count()

        payload = {
            'email': 'test@example.com',
            'gateway': 'bank',
            'payment_method_nonce': 'fake-valid-nonce',
            'full_name': 'Erik von Namenstein',
            'company': 'Nöming Thingéys',
            'street_address': 'Scotland Pl',
            'locality': 'Amsterdongel',
            'postal_code': '1025 ET',
            'region': 'Worbelmoster',
            'country': 'NL',
        }
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 5})
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode())

        self.assertEqual(total_subscriptions + 1, Subscription.objects.count())
        self.assertEqual(total_memberships + 1, Membership.objects.count())
        created_subs = Subscription.objects.order_by('-id').first()
        created_memb = created_subs.membership
        self.assertIsNotNone(created_subs)
        self.assertEqual('on-hold', created_subs.status)

        self.assertIsNotNone(created_subs.membership)
        self.assertEqual('inactive', created_subs.membership.status)

        # An email should have been sent with the bank info.
        self.assertEqual(1, len(django.core.mail.outbox))
        the_mail: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[0]
        self.assertIn(created_subs.membership.level.name, the_mail.body)
        self.assertIn(created_subs.price.with_currency_symbol(), the_mail.body)
        return created_memb, the_mail

    def test_bank_creates_inactive_membership_sends_email(self):
        self.client.force_login(self.user)
        membership, the_mail = self._test_bank_creates_inactive_membership()

        edit_url = absolute_url('settings_membership_edit',
                                kwargs={'membership_id': membership.id})
        self.assertIn(edit_url, the_mail.body)
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(the_mail.body, expected_mail_body, the_mail.body)
        self.assertEqual(
            html_to_text(alt0_body),
            expected_mail_body,
            html_to_text(the_mail.alternatives[0][0]),
        )

    def test_bank_creates_inactive_membership_sends_email_without_an_account(self):
        membership, the_mail = self._test_bank_creates_inactive_membership()

        # Membership level should be in the claim CTA text
        self.assertIn(membership.level.name, the_mail.body, the_mail.body)
        # Edit membership link shouldn't be in the email
        edit_url = absolute_url('settings_membership_edit',
                                kwargs={'membership_id': membership.id})
        self.assertNotIn(edit_url, the_mail.body)
        # but claim link should
        token = membership.customer.token.token
        link_membership_url = absolute_url(
            'link_membership',
            kwargs={'token': token},
        )
        self.assertIn(link_membership_url, the_mail.body)
        self.assertEqual(
            the_mail.body,
            expected_mail_w_token_body.format(token=token),
            the_mail.body,
        )
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(
            html_to_text(alt0_body),
            expected_mail_w_token_body.format(token=token),
            html_to_text(alt0_body),
        )
