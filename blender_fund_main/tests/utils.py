from io import BytesIO

from pypdf import PdfReader

expected_pdf_text_tmpl = '''Stichting Blender Foundation
Buikslotermeerplein 161
1025 ET Amsterdam, the Netherlands
Tax number NL 8111.66.223
Blender Development Fund Donation Receipt
 Receipt ID
 {order.pk}
 Payment received on
 {expected_date}
{expected_description}Donation towards the Blender Development Fund, to support core development and public
research of the free and open source 3D software at blender.org.
Billing Address
 Address
{expected_address}
 E-mail
 {expected_email}
Payment Information
 Amount paid
 {expected_currency_symbol} {expected_total}
 Payment method
 {expected_payment_method}
'''  # noqa: E501
expected_pdf_text_bank_details = ''' Bank details
 Bank: ING Bank, P/O Box 1800, Amsterdam, the Netherlands
BIC/Swift code: INGB NL 2A (international code)
IBAN: NL45 INGB 0009356121 (for euro accounts)
Account #: 93 56 121
'''


def extract_text_from_pdf_bytes(content: bytes):
    pdf = PdfReader(BytesIO(content))
    assert 1 == len(pdf.pages)
    pdf_page = pdf.pages[0]
    return pdf_page.extract_text()


def extract_text_from_pdf(response):
    return extract_text_from_pdf_bytes(response.content)


def to_ap_style_month(formatted_date):
    """See https://docs.djangoproject.com/en/5.1/ref/templates/builtins/#date for `N`"""

    replacements = [
        ('Mar.', 'March'),
        ('Apr.', 'April'),
        ('May.', 'May'),
        ('Jun.', 'June'),
        ('Jul.', 'July'),
        ('Sep.', 'Sept.'),
    ]
    for r in replacements:
        formatted_date = formatted_date.replace(*r)
    return formatted_date
