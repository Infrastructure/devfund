from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key
from django.test import override_settings

from looper.models import Subscription
from looper.money import Money
from looper.tests import AbstractBaseTestCase
from .. import models
from ..views import landing

User = get_user_model()


@override_settings(
    LOOPER_CONVERTION_RATES_FROM_EURO={
        'EUR': 1.0,
        'USD': 1.15,  # US$ 1.15 = € 1.00
    }
)
class TotalIncomeTest(AbstractBaseTestCase):
    fixtures = ['systemuser', 'gateways', 'devfund', 'testuser']

    def setUp(self):
        self.user = User.objects.get(email='harry@blender.org')
        self.customer = self.user.customer

    def test_total_income(self):
        # Create INDIVIDUAL memberships.

        # This should produce US$ 5.83 = € 5.07 per month.
        subs_yearly_usd = Subscription.objects.create(
            customer=self.user.customer,
            plan_id=1,
            price=7000,
            currency="USD",
            interval_unit="year",
            interval_length=1,
            collection_method="automatic",
        )
        self.assertIsNotNone(subs_yearly_usd.membership)
        self.assertEqual(Money('USD', 583), subs_yearly_usd.monthly_rounded_price)

        # This should produce € 10.00 per month.
        subs_monthly_eur = Subscription.objects.create(
            customer=self.user.customer,
            plan_id=2,
            price=1000,
            currency="EUR",
            interval_unit="month",
            interval_length=1,
            collection_method="automatic",
        )
        self.assertIsNotNone(subs_monthly_eur.membership)
        self.assertEqual(Money('EUR', 1000), subs_monthly_eur.monthly_rounded_price)

        # Create CORP memberships.

        # This should produce US$ 58.33 = € 50.72 per month.
        subs_yearly_usd = Subscription.objects.create(
            customer=self.user.customer,
            plan_id=7,
            price=70000,
            currency="USD",
            interval_unit="year",
            interval_length=1,
            collection_method="automatic",
        )
        self.assertIsNotNone(subs_yearly_usd.membership)
        self.assertEqual(Money('USD', 5833), subs_yearly_usd.monthly_rounded_price)

        # This should produce € 100.00 per month.
        subs_monthly_eur = Subscription.objects.create(
            customer=self.user.customer,
            plan_id=9,
            price=10000,
            currency="EUR",
            interval_unit="month",
            interval_length=1,
            collection_method="automatic",
        )
        self.assertIsNotNone(subs_monthly_eur.membership)
        self.assertEqual(Money('EUR', 10000), subs_monthly_eur.monthly_rounded_price)

        # Pay for the subscriptions to enable all memberships.
        for subs in Subscription.objects.all():
            order = subs.generate_order()
            order.status = 'paid'
            order.save()

        # Test the total income in €
        total_income = landing.get_total_income_in_euro()
        expect_totals = {
            'INDIV': Money('EUR', 1507),
            'CORP': Money('EUR', 15072),
        }
        self.assertEqual(expect_totals['INDIV'] + expect_totals['CORP'], total_income)

        # Test convertion to US$
        landing.total_income_in_currency.cache_clear()
        total_income_in_usd = landing.total_income_in_currency('USD')
        expect_totals = {
            'INDIV': Money('USD', 1733),
            'CORP': Money('USD', 17333),
        }
        self.assertEqual(expect_totals['INDIV'] + expect_totals['CORP'], total_income_in_usd)

    def test_empty(self):
        total_income = landing.get_total_income_in_euro()
        expect_totals = {
            'INDIV': Money('EUR', 0),
            'CORP': Money('EUR', 0),
        }
        self.assertEqual(expect_totals['INDIV'] + expect_totals['CORP'], total_income)


class PrivateMembershipsTest(AbstractBaseTestCase):
    fixtures = ['systemuser', 'gateways', 'devfund', 'testuser', 'flatpages']

    def setUp(self):
        self.user = User.objects.get(email='harry@blender.org')
        self.customer = self.user.customer

    def test_hide_on_landing_page(self):
        display_name = 'невероятно застенчивый человек'
        memb = models.Membership.objects.create(
            level_id=7,
            customer=self.user.customer,
            display_name=display_name,
            status='active',
            is_private=False,
        )

        resp = self.client.get('/')
        self.assertIn(display_name, resp.content.decode())

        memb.refresh_from_db()
        memb.is_private = True
        memb.save()

        cache.delete(make_template_fragment_key("credits"))
        resp = self.client.get('/')
        self.assertNotIn(display_name, resp.content.decode())
