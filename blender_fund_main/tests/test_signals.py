from django.contrib.auth.models import User

from looper.models import Customer
from looper.tests import AbstractBaseTestCase

from blender_fund_main import signals


class CustomerCreationTestCase(AbstractBaseTestCase):
    def setUp(self):
        self.user = User.objects.create_user('harry', 'harry@blender.org')
        self.customer: Customer = self.user.customer

    def test_customer_creation(self):
        # The Customer should have been created via the post_save signal
        self.assertIsNotNone(self.customer)
        # The default billing Address email should match the User email
        self.assertEqual(self.user.email, self.customer.billing_address.email)

    def test_double_customer_creation(self):
        signals.create_customer(User, self.user, created=True, raw=False)
        self.user.refresh_from_db()
        self.assertEqual(self.customer.pk, self.user.customer.pk)
