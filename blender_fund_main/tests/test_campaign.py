from unittest import mock
import urllib.parse

from background_task.models import Task
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase
from freezegun import freeze_time
import responses

from looper.tests import AbstractLooperTestCase
from looper.tests.factories import OrderFactory, SubscriptionFactory, CustomerFactory, UserFactory
import looper.models

import blender_fund_main.models as models
import blender_fund_main.tasks as tasks

User = get_user_model()


class TestCampaign(TestCase):
    fixtures = ['gateways', 'devfund', 'systemuser']

    def setUp(self):
        self.customer = CustomerFactory()
        self.campaign = models.Campaign.objects.create(
            starts_at='2023-12-01 00:00:00Z',
            ends_at='2023-12-10 00:00:00Z',
            badge='campaign-badge',
            is_active=True,
        )
        responses.start()

    def tearDown(self):
        responses.stop()
        responses.reset()

    @freeze_time('2023-12-03 20:30:00')
    def test_product_order_paid_during_inactive_campaign_not_added(self):
        self.campaign.is_active = False
        self.campaign.save(update_fields={'is_active'})
        order = OrderFactory(
            customer=self.customer,
            subscription=None,
            product=looper.models.Product.objects.first(),
        )
        # Not paid yet hence not added to the campaign yet either
        self.assertIsNone(order.paid_at)
        self.assertEqual(order.campaign_set.count(), 0)

        order.status = 'paid'
        order.save(update_fields={'status'})

        # Not added to the campaign just now even though now paid
        self.assertEqual(str(order.paid_at), '2023-12-03 20:30:00+00:00')
        self.assertEqual(order.campaign_set.count(), 0)

        # No task for granting badges was created
        self.assertEqual(Task.objects.count(), 0)

    @freeze_time('2023-12-12 20:30:00')
    def test_product_order_paid_outside_campaign_dates_not_added(self):
        order = OrderFactory(
            customer=self.customer,
            subscription=None,
            product=looper.models.Product.objects.first(),
        )
        # Not paid yet hence not added to the campaign yet either
        self.assertIsNone(order.paid_at)
        self.assertEqual(order.campaign_set.count(), 0)

        order.status = 'paid'
        order.save(update_fields={'status'})

        # Not added to the campaign just now even though now paid
        self.assertEqual(str(order.paid_at), '2023-12-12 20:30:00+00:00')
        self.assertEqual(order.campaign_set.count(), 0)

    @freeze_time('2023-12-03 20:30:00')
    def test_product_order_paid_during_campaign_is_added(self):
        order = OrderFactory(
            customer=self.customer,
            subscription=None,
            product=looper.models.Product.objects.first(),
        )
        # Not paid yet hence not added to the campaign yet either
        self.assertIsNone(order.paid_at)
        self.assertEqual(order.campaign_set.count(), 0)

        order.status = 'paid'
        order.save(update_fields={'status'})

        # Added to the campaign just now
        self.assertEqual(str(order.paid_at), '2023-12-03 20:30:00+00:00')
        self.assertEqual(order.campaign_set.count(), 1)

        # Saving order once more shouldn't change or break anything
        order.save()
        order.refresh_from_db()
        self.assertEqual(str(order.paid_at), '2023-12-03 20:30:00+00:00')
        self.assertEqual(order.campaign_set.count(), 1)

        # No task for granting a badge created because no user account
        self.assertIsNone(Task.objects.first())

    @freeze_time('2023-12-03 20:30:00')
    def test_subscription_order_paid_during_campaign_is_added(self):
        order = OrderFactory(
            subscription=SubscriptionFactory(customer=self.customer),
            customer=self.customer,
        )
        # Not paid yet hence not added to the campaign yet either
        self.assertIsNone(order.paid_at)
        self.assertEqual(order.campaign_set.count(), 0)

        order.status = 'paid'
        order.save(update_fields={'status'})

        # Added to the campaign just now
        self.assertEqual(str(order.paid_at), '2023-12-03 20:30:00+00:00')
        self.assertEqual(order.campaign_set.count(), 1)

        # No task for granting a badge created because no user account
        self.assertIsNone(Task.objects.first())

    @freeze_time('2023-12-03 20:30:00')
    def test_product_order_paid_during_campaign_badge_granted_to_account(self):
        user = UserFactory()
        order = OrderFactory(
            customer=user.customer,
            subscription=None,
            product=looper.models.Product.objects.first(),
        )

        order.status = 'paid'
        order.save(update_fields={'status'})

        # Added to the campaign just now
        self.assertEqual(order.campaign_set.count(), 1)

        # A task for granting a badge was created
        self.assertEqual(
            Task.objects.first().task_params,
            f'[[], {{"grant": "campaign-badge", "user_id": {user.pk}}}]',
        )

    @freeze_time('2023-12-03 20:30:00')
    def test_subscription_order_paid_during_campaign_badge_granted_to_account(self):
        user = UserFactory()
        order = OrderFactory(
            subscription=SubscriptionFactory(
                customer=user.customer,
            ),
            customer=user.customer,
        )

        order.status = 'paid'
        order.save(update_fields={'status'})

        # Added to the campaign just now
        self.assertEqual(order.campaign_set.count(), 1)

        # A task for granting a badge was created
        self.assertEqual(
            Task.objects.first().task_params,
            f'[[], {{"grant": "campaign-badge", "user_id": {user.pk}}}]',
        )

    @mock.patch(
        'blender_fund_main.tasks.grant_or_revoke_badge',
        new=tasks.grant_or_revoke_badge.task_function,
    )
    @freeze_time('2023-12-03 20:30:00')
    def test_subscription_order_paid_during_campaign_blender_id_called(self):
        user = UserFactory()
        order = OrderFactory(
            subscription=SubscriptionFactory(
                customer=user.customer,
            ),
            customer=user.customer,
        )

        order.status = 'paid'
        order.save(update_fields={'status'})

        # Added to the campaign just now
        self.assertEqual(order.campaign_set.count(), 1)

        # Blender ID was called to grant a role matching campaign badge
        self.assertEqual(len(responses.calls), 1)
        self.assertEqual(
            responses.calls[0].request.url,
            f'{settings.BLENDER_ID["BASE_URL"]}api/badger/grant/campaign-badge/'
            f'{urllib.parse.quote(user.email)}',
        )


class TestCampaignWithMembership(AbstractLooperTestCase):
    gateway_name = 'bank'

    @freeze_time('2023-12-03 20:30:00')
    def test_corporate_order_paid_during_campaign_is_not_added(self):
        subscription = self.create_active_subscription()
        level = subscription.membership.level
        level.category = 'CORP'
        level.save(update_fields={'category'})

        self.campaign = models.Campaign.objects.create(
            starts_at='2023-12-01 00:00:00Z',
            ends_at='2023-12-10 00:00:00Z',
            badge='campaign-badge',
            is_active=True,
        )
        order = OrderFactory(
            subscription=subscription,
            customer=subscription.customer,
        )

        order.status = 'paid'
        order.save(update_fields={'status'})

        # Not added to the campaign
        self.assertEqual(str(order.paid_at), '2023-12-03 20:30:00+00:00')
        self.assertEqual(order.campaign_set.count(), 0)

        # No task for granting the campaign badge created either
        self.assertIsNone(Task.objects.filter(task_params__contains='campaign-badge').first())
