from unittest.mock import patch, Mock

from django.conf import settings
from django.test import override_settings
from django.urls import reverse
from freezegun import freeze_time

from looper.tests import AbstractLooperTestCase
from looper.tests.factories import (
    OrderFactory,
    PaymentMethodFactory,
    SubscriptionFactory,
    UserFactory,
    create_customer_with_billing_address,
)

from blender_fund_main.tests.utils import (
    extract_text_from_pdf,
    expected_pdf_text_tmpl,
    expected_pdf_text_bank_details,
)


production_storage = settings.STATICFILES_STORAGE


# This test needs to use the static file storage we also use in production,
# regardless of what the superclass overrides. There was an issue finding
# the image file, which went unseen because we test with a static file
# storage class that doesn't require you to run 'manage.py collectstatic'.
@override_settings(
    STATICFILES_STORAGE=production_storage,
)
class TestReceiptPDFView(AbstractLooperTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Run the collectstatic command, as that's needed for the production
        # storage class to work.
        from django.contrib.staticfiles.management.commands import collectstatic
        cmd = collectstatic.Command()

        # This mimicks the relevant part of BaseCommand.run_from_argv()
        parser = cmd.create_parser('python', 'collectstatic')
        options = parser.parse_args(['--no-input'])
        cmd_options = vars(options)
        cmd.handle(**cmd_options)

    def test_get_receipt_pdf_has_logo(self):
        subs = self.create_active_subscription()
        order_id = subs.latest_order().id
        self.client.force_login(self.user)
        resp = self.client.get(reverse('settings_receipt_pdf',
                                       kwargs={'order_id': order_id}))
        self.assertEqual(200, resp.status_code)
        self.assertEqual(b'%PDF-', resp.content[:5])

        # Check that the image is there.
        self.assertIn(b'/Subtype /Image', resp.content)
        self.assertIn(b'/Height 125', resp.content)
        self.assertIn(b'/Width 601', resp.content)

    def test_get_receipt_pdf_donation(self):
        subs = self.create_active_subscription()
        product = subs.plan.product
        product.description = 'Product description here'
        product.save(update_fields={'description'})
        order_id = subs.latest_order().id
        self.client.force_login(self.user)
        resp = self.client.get(reverse('settings_receipt_pdf',
                                       kwargs={'order_id': order_id}))
        self.assertEqual(200, resp.status_code)

        # Check that "donation" is there.
        self.assertIn(b'Donation Receipt', resp.content)


@patch('looper.admin_log.attach_log_entry', Mock(return_value=None))
@freeze_time('2024-07-04')
class TestReceiptPDFContent(AbstractLooperTestCase):
    maxDiff = None

    @patch('looper.admin_log.attach_log_entry', Mock(return_value=None))
    def setUp(self):
        super().setUp()

        customer = create_customer_with_billing_address(
            country='NL',
            email='billing@example.com',
            locality='Testville',
            postal_code='1234 AB',
            region='North Testborough',
            street_address='Main st. 123',
        )
        self.payment_method = PaymentMethodFactory(
            customer=customer,
            recognisable_name='PayPal account test@example.com',
        )
        self.subscription = SubscriptionFactory(
            customer=customer,
            payment_method=self.payment_method,
            plan_id=1,
            price=990,
            currency='EUR',
        )
        self.paid_order = self.subscription.generate_order(save=False)
        self.paid_order.status = 'paid'
        self.paid_order.save()

    def test_get_pdf_unpaid_order_not_found(self):
        unpaid_order = OrderFactory(
            customer=self.payment_method.customer,
            price=990,
            tax_country='NL',
            payment_method=self.payment_method,
            subscription__customer=self.payment_method.customer,
            subscription__payment_method=self.payment_method,
            subscription__plan_id=1,
        )
        self.client.force_login(unpaid_order.customer.user)
        url = reverse('settings_receipt_pdf', kwargs={'order_id': unpaid_order.pk})
        response = self.client.get(url)

        self.assertEqual(404, response.status_code)

    def test_get_pdf_anonymous_redirects_to_login(self):
        url = reverse('settings_receipt_pdf', kwargs={'order_id': self.paid_order.pk})
        response = self.client.get(url)

        self.assertEqual(302, response.status_code)
        self.assertEqual(f'/oauth/login?next={url}', response['Location'])

    def test_get_pdf_someone_elses_order_not_found(self):
        another_user = UserFactory()

        self.client.force_login(another_user)
        url = reverse('settings_receipt_pdf', kwargs={'order_id': self.paid_order.pk})
        response = self.client.get(url)

        self.assertEqual(404, response.status_code)

    def test_pdf_content_eur_with_address(self):
        self.client.force_login(self.paid_order.customer.user)
        url = reverse('settings_receipt_pdf', kwargs={'order_id': self.paid_order.pk})
        response = self.client.get(url)

        self.assertEqual(200, response.status_code)

        pdf_text = extract_text_from_pdf(response)
        self.assertEqual(
            pdf_text,
            expected_pdf_text_tmpl.format(
                order=self.paid_order,
                expected_address=(
                    ' Main st. 123\n'
                    'Testville\n'
                    '1234 AB\n'
                    'North Testborough\n'
                    'Netherlands'
                ),
                expected_currency_symbol='€',
                expected_date=self.paid_order.paid_at.strftime('%B %-d, %Y'),
                expected_description=' Membership level\n Bronze\n',
                expected_email='billing@example.com',
                expected_payment_method='PayPal account test@example.com',
                expected_total='9.90',
            ),
            pdf_text,
        )

    def test_pdf_content_eur(self):
        user = UserFactory()
        order = OrderFactory(
            customer=user.customer,
            price=990,
            currency='EUR',
            status='paid',
            email='billing@example.com',
            payment_method=self.payment_method,
            subscription__customer=user.customer,
            subscription__plan_id=1,
        )
        self.client.force_login(order.customer.user)
        url = reverse('settings_receipt_pdf', kwargs={'order_id': order.pk})
        response = self.client.get(url)

        self.assertEqual(200, response.status_code)

        pdf_text = extract_text_from_pdf(response)
        self.assertEqual(
            pdf_text,
            expected_pdf_text_tmpl.format(
                order=order,
                expected_address=' -',
                expected_currency_symbol='€',
                expected_date=order.paid_at.strftime('%B %-d, %Y'),
                expected_description=' Membership level\n Bronze\n',
                expected_email='billing@example.com',
                expected_payment_method='PayPal account test@example.com',
                expected_total='9.90',
            ),
            pdf_text,
        )

    def test_pdf_content_eur_bank_payment(self):
        user = UserFactory()
        order = OrderFactory(
            customer=user.customer,
            price=990,
            currency='EUR',
            status='paid',
            email='billing@example.com',
            payment_method=PaymentMethodFactory(
                method_type='ba',
                recognisable_name='Bank Transfer',
            ),
            subscription__customer=user.customer,
            subscription__plan_id=1,
        )
        self.client.force_login(order.customer.user)
        url = reverse('settings_receipt_pdf', kwargs={'order_id': order.pk})
        response = self.client.get(url)

        self.assertEqual(200, response.status_code)

        pdf_text = extract_text_from_pdf(response)
        self.assertEqual(
            pdf_text,
            expected_pdf_text_tmpl.format(
                order=order,
                expected_address=' -',
                expected_currency_symbol='€',
                expected_date=order.paid_at.strftime('%B %-d, %Y'),
                expected_description=' Membership level\n Bronze\n',
                expected_email='billing@example.com',
                expected_payment_method='Bank Transfer',
                expected_total='9.90',
            ) + expected_pdf_text_bank_details,
            pdf_text,
        )

    def test_pdf_content_usd(self):
        user = UserFactory()
        order = OrderFactory(
            customer=user.customer,
            price=1480,
            currency='USD',
            status='paid',
            tax_country='US',
            email='billing@example.com',
            payment_method=self.payment_method,
            subscription__customer=user.customer,
            subscription__plan_id=1,
        )
        self.client.force_login(order.customer.user)
        url = reverse('settings_receipt_pdf', kwargs={'order_id': order.pk})
        response = self.client.get(url)

        self.assertEqual(200, response.status_code)

        pdf_text = extract_text_from_pdf(response)
        self.assertEqual(
            pdf_text,
            expected_pdf_text_tmpl.format(
                order=order,
                expected_address=' -',
                expected_currency_symbol='$',
                expected_date=order.paid_at.strftime('%B %-d, %Y'),
                expected_description=' Membership level\n Bronze\n',
                expected_email='billing@example.com',
                expected_payment_method='PayPal account test@example.com',
                expected_total='14.80',
            ),
            pdf_text,
        )
