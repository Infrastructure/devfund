from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.urls import reverse

from looper.money import Money
from looper.tests import AbstractLooperTestCase
import looper.models
import looper.signals

from .. import forms, models


class ManagedMembershipCreationTest(AbstractLooperTestCase):

    def setUp(self):
        super().setUp()
        self.user = User.objects.get(email='harry@blender.org')
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()
        self.client.force_login(self.user)

    def test_creation_via_form(self):
        # post_url = reverse('admin:blender_fund_main_membership_add')
        memb_level = models.MembershipLevel.objects.get(pk=8)  # Corporate Silver

        form = forms.MembershipAdminAddForm({
            'customer': self.user.customer.pk,
            'status': 'active',
            'level': str(memb_level.pk),
            'interval_unit': 'month',
            'interval_length': '3',
            'currency': 'USD',
            'price': '4700',
            'display_name': 'Gekke Henkie',
            'description': 'Müüü',
        })
        form.full_clean()
        memb = form.save()
        self.check_results(memb)

    def test_creation_via_admin(self):
        post_url = reverse('admin:blender_fund_main_membership_add')
        memb_level = models.MembershipLevel.objects.get(pk=8)  # Corporate Silver

        resp = self.client.post(post_url, {
            'customer': self.user.customer.pk,
            'status': 'active',
            'level': str(memb_level.pk),
            'interval_unit': 'month',
            'interval_length': '3',
            'currency': 'USD',
            'price': '4700',
            'display_name': 'Gekke Henkie',
            'description': 'Müüü',
            '_continue': 'Submit and Continue Editing',
        })
        self.assertEqual(302, resp.status_code)

        # URL will be something like /admin/blender_fund_main/membership/188/change/
        memb_pk = int(resp['Location'].split('/')[-3])
        change_url = reverse('admin:blender_fund_main_membership_change',
                             kwargs={'object_id': memb_pk})
        self.assertEqual(change_url, resp['Location'])

        memb = models.Membership.objects.get(pk=memb_pk)
        self.check_results(memb)

    def check_results(self, memb):
        # Don't trust what we get returned; always test against what is in the database.
        memb.refresh_from_db()
        subscription = memb.subscription
        # Check that no extra memberships or subscriptions were created
        self.assertEqual(1, models.Membership.objects.count())
        self.assertEqual(1, looper.models.Subscription.objects.count())
        self.assertEqual(looper.models.Subscription.objects.first().pk, memb.subscription_id)
        # Check membership properties.
        self.assertEqual('Gekke Henkie', memb.display_name)
        self.assertEqual('Müüü', memb.description)
        self.assertEqual(Money('USD', 470000), subscription.price)
        self.assertEqual('USD', subscription.currency)
        self.assertEqual('managed', subscription.collection_method)
        # Check subscription properties.
        self.assertEqual(relativedelta(months=3), subscription.interval)
        self.assertAlmostEqualDateTime(memb.created_at + subscription.interval,
                                       subscription.next_payment)
