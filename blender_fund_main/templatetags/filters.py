from django import template
from django.contrib.humanize.templatetags.humanize import naturaltime

register = template.Library()


@register.filter(name='naturaltime_compact')
def naturaltime_compact(time):
    """A more compact version of the humanize naturaltime filter."""

    compact_time = naturaltime(time)

    # Replace non-breaking space with regular space so it can be adjusted per case.
    compact_time = compact_time.replace(u'\xa0', u' ')

    # Take only the first part, e.g. "3 days, 2h ago", becomes " 3d ago"
    compact_time = compact_time.split(',')[0]

    compact_time = compact_time.replace(' ago', '')
    compact_time = compact_time.replace('a second', '1 s')
    compact_time = compact_time.replace(' seconds', ' s')
    compact_time = compact_time.replace(' second', ' s')
    compact_time = compact_time.replace(' minutes', ' m')
    compact_time = compact_time.replace('a minute', '1 m')
    compact_time = compact_time.replace(' minute', ' m')
    compact_time = compact_time.replace(' hours', ' h')
    compact_time = compact_time.replace('an hour', '1 h')
    compact_time = compact_time.replace(' hour', ' h')
    compact_time = compact_time.replace(' days', ' d')
    compact_time = compact_time.replace(' day', ' d')
    compact_time = compact_time.replace(' weeks', ' w')
    compact_time = compact_time.replace(' week', ' w')
    compact_time = compact_time.replace(' months', ' mo')
    compact_time = compact_time.replace(' month', ' mo')
    compact_time = compact_time.replace(' years', ' y')
    compact_time = compact_time.replace(' year', ' y')
    compact_time = compact_time.replace(' ', '')

    return compact_time


@register.filter(name='add_form_classes')
def add_form_classes(form, size_class=""):
    """Add Bootstrap classes and our custom classes to the form fields."""
    for field_name, field in form.fields.items():
        input_type = getattr(field.widget, 'input_type', None)
        if input_type in ('radio', 'checkbox'):
            continue
        classes = {f'field-{field_name}', 'form-control'}
        if size_class:
            classes.add(f'form-control-{size_class}')
        if input_type == 'select':
            classes.add('form-select')
            if size_class:
                classes.add(f'form-select-{size_class}')
        field.widget.attrs.update({'class': ' '.join(classes)})

    # Add error class to all the fields with errors
    invalid_fields = form.fields if '__all__' in form.errors else form.errors
    for field_name in invalid_fields:
        attrs = form.fields[field_name].widget.attrs
        attrs.update({'class': attrs.get('class', '') + ' is-invalid'})
    return form


@register.filter
def get_attr(value, attr_name):
    """
    Dynamically get an attribute or dictionary key from the provided object.
    - If value is a dictionary, it tries to access the key.
    - If value is an object, it tries to access the attribute.
    """
    if isinstance(value, dict):
        return value.get(attr_name)
    return getattr(value, attr_name, None)
