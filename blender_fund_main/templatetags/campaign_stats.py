import datetime
import json

from django import template
from django.conf import settings
from django.db.models import Sum, Count
from django.db.models.functions import TruncDate

import looper.models

register = template.Library()


@register.simple_tag
def campaign_stats_json():
    start_date = datetime.datetime(2023, 12, 1)
    end_date = datetime.datetime(2024, 1, 2, 23, 59, 59)
    stats_per_date = {}
    currencies = ['EUR', 'USD']
    # rate unit is USD/EUR, so to get EUR from USD we need to divide by this rate
    conversion_rate = settings.LOOPER_CONVERTION_RATES_FROM_EURO['USD']
    for currency in currencies:
        stats_per_currency = _per_currency(currency, start_date, end_date)
        for row in list(stats_per_currency):
            date = row['date']
            date_stats = stats_per_date.get(date, {'count': 0, 'date': date, 'sum': 0})
            date_stats['count'] += row['count']
            if currency == 'EUR':
                date_stats['sum'] += row['sum']
            else:
                # safe to do it here once per date, because we will have fractions only for USD
                # otherwise we could accumulate a small error
                date_stats['sum'] += round(row['sum'] / conversion_rate)
            stats_per_date[date] = date_stats
    return json.dumps([stats_per_date[k] for k in sorted(stats_per_date.keys())], default=str)


def _per_currency(currency, start_date, end_date):
    return looper.models.Order.objects.filter(
        paid_at__range=(start_date, end_date),
        currency=currency,
    ).annotate(date=TruncDate('paid_at')).values('date').annotate(
        count=Count('id'),
        sum=Sum('price') / 100
    ).order_by('date')
