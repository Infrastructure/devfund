import json
import logging

from django import forms as django_forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.contrib.flatpages.admin import FlatPageAdmin, FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.template import Template, Context
from django.urls import path, reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
import django.utils.timezone

from codemirror.widgets import CodeMirror
from looper import decorators
from looper.admin import (
    subscription_link,
    create_admin_fk_link,
    plan_link,
    EditableWhenNewMixin,
    copy_to_clipboard_link,
    REL_CUSTOMER_SEARCH_FIELDS,
    customer_user_link,
    customer_link,
    ACCOUNT_INFORMATION_FIELDSET,
)
from looper.admin.filters import EmptyFieldListFilter
import blender_fund_main.email
from . import models, forms
import looper.admin.reports
import looper.forms
import looper.models

import nested_admin

admin.site.site_header = 'Blender Development Fund'
admin.site.index_title = 'Fund Membership and Subscription Administration'
admin.site.enable_nav_sidebar = False

membership_link = create_admin_fk_link(
    'membership', 'Membership', 'admin:blender_fund_main_membership_change'
)
user_link = create_admin_fk_link('user', 'user', 'admin:auth_user_change')
logger = logging.getLogger(__name__)


def custom_titled_filter(title):
    class Wrapper(admin.FieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.FieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance
    return Wrapper


@decorators.short_description('Links for Customers')
def subscribe_memlev_link(memlev: models.MembershipLevel) -> str:
    """Show the link to pay for & create a subscription, can be sent to users."""
    if not memlev or not memlev.pk:
        return ''

    return copy_to_clipboard_link(
        'looper:checkout_new',
        kwargs={'plan_id': memlev.plan_id},
        link_text='Pay & Subscribe',
        title='Send this to customers',
    ) if memlev.plan_id else 'No plan -- no link'


@admin.register(models.MembershipLevel)
class MembershipLevelAdmin(admin.ModelAdmin):
    model = models.MembershipLevel
    list_display = (
        'name',
        'category',
        'visible_attributes',
        'order',
        plan_link,
        'is_popular',
        subscribe_memlev_link,
    )
    list_editable = ('category', 'visible_attributes', 'order', 'is_popular')
    list_filter = ('category',)
    search_fields = ('category', 'name')


@decorators.short_description('Last Order Receipt')
def last_order_pdf_link(membership: models.Membership) -> str:
    subs: looper.models.Subscription = membership.subscription
    if not subs:
        return '-'
    order = subs.latest_order()
    if not order:
        return '-'
    if order.status != 'paid':
        return '-'

    pdf_url = reverse('settings_receipt_pdf', kwargs={'order_id': order.id})
    return format_html('<a href="{}" target="_blank">PDF for order #{}</a>', pdf_url, order.pk)


@admin.register(models.Membership)
class MembershipAdmin(EditableWhenNewMixin, admin.ModelAdmin):
    model = models.Membership
    list_display = (
        'id',
        customer_user_link,
        'display_name',
        'level',
        'status_icon',
        'is_managed',
        'is_private',
        subscription_link,
        last_order_pdf_link,
    )
    list_display_links = ('id', 'display_name')
    list_filter = (
        'level',
        'is_managed',
        'status',
        'is_private',
        ('subscription__status', custom_titled_filter('subscription status')),
        'granted_badge',
        ('customer__user', EmptyFieldListFilter),
    )
    search_fields = (
        'id',
        'customer__user__username',
        *REL_CUSTOMER_SEARCH_FIELDS,
        'display_name',
        'url',
    )

    raw_id_fields = ['customer']
    readonly_fields = [
        customer_user_link,
        customer_link,
        subscription_link,
        'is_managed',
        'created_at',
        'updated_at',
        'granted_badge',
    ]
    editable_when_new = {subscription_link}
    fieldsets = [
        (
            None,
            {
                'fields': [
                    subscription_link,
                    'is_managed',
                    'status',
                    'level',
                    'granted_badge',
                    'created_at',
                    'updated_at',
                ],
            },
        ),
        (
            'Account information',
            {
                'fields': [
                    (customer_user_link, 'customer'),
                ],
                'description': (
                    f"<p>{ACCOUNT_INFORMATION_FIELDSET[1]['description']}</p>"
                    "<p>If you need to transfer the membership to another account, "
                    "you can do so by using 🔍 button next to the <b>Customer</b></p>"
                ),
            },
        ),
        (
            'Attributes',
            {
                'fields': [
                    'display_name',
                    'description',
                    ('url', 'suppress_nofollow'),
                    'logo',
                    'is_private',
                ],
            },
        ),
    ]
    add_fieldsets = [
        (
            None,
            {
                'fields': ['customer', 'status', 'level'],
            },
        ),
        (
            'Subscription Parameters',
            {
                'fields': ['currency', 'price', 'interval_unit', 'interval_length'],
            },
        ),
        (
            'Attributes',
            {
                'fields': ['display_name', 'description', 'url', 'logo', 'is_private'],
            },
        ),
    ]

    def get_form(self, request, obj=None, **kwargs):
        if obj is None:
            kwargs['form'] = forms.MembershipAdminAddForm
        form = super().get_form(request, obj, **kwargs)
        return form

    def get_fieldsets(self, request, obj=None):
        if obj is None:
            return self.add_fieldsets
        return super().get_fieldsets(request, obj)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        if add:
            context['title'] = 'Add Managed Membership'
        return super().render_change_form(request, context, add, change, form_url, obj)

    def get_changeform_initial_data(self, request) -> dict:
        return {
            **super().get_changeform_initial_data(request),
            'status': 'active',
            'customer_id': 1,
        }

    @decorators.short_description('Is active')
    def status_icon(self, membership: models.Membership):
        from django.templatetags.static import static

        try:
            icon = {'active': 'yes', 'inactive': 'no'}[membership.status]
        except KeyError:
            icon = 'unknown'

        icon_url = static(f'admin/img/icon-{icon}.svg')
        return format_html(
            '<img src="{}" alt="{}" title="{}">', icon_url, membership.status, membership.status
        )


admin.site.unregister(User)
admin.site.unregister(looper.models.Subscription)


@admin.register(looper.models.Subscription)
class SubscriptionAdmin(looper.admin.SubscriptionAdmin):
    """Override Subscription admin, adding a link to the membership."""

    readonly_fields = looper.admin.SubscriptionAdmin.readonly_fields + [membership_link]
    # Add `membership_link` to the main section of Subscription details
    fieldsets = [
        (name, {'fields': _set['fields'] + [membership_link]} if name is None else _set)
        for name, _set in looper.admin.SubscriptionAdmin.fieldsets
    ]


@admin.register(User)
class UserAdmin(BaseUserAdmin, nested_admin.NestedModelAdmin):
    list_display = (
        'username',
        'email',
        'user_memberships_link',
        'user_donations_link',
        'is_staff',
        'is_superuser',
    )
    search_fields = ('username', 'email', 'linked_tokens__token', *REL_CUSTOMER_SEARCH_FIELDS)

    add_fieldsets = (
        (
            None,
            {
                'classes': ('wide',),
                'fields': ('username', 'password1', 'password2', 'email'),
            },
        ),
    )
    form = looper.forms.AdminUserChangeForm
    add_form = looper.forms.AdminUserCreationForm

    inlines = (
        looper.admin.LinkCustomerTokenInline,
        looper.admin.CustomerInline,
    )

    fieldsets = (
        (
            None,
            {
                'fields': (
                    'username',
                    'password',
                    'email',
                    'user_memberships_link',
                    'user_subscriptions_link',
                    'user_donations_link',
                )
            }
        ),
        (
            _('Permissions'),
            {
                'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
                'classes': ('collapse',),
            },
        ),
        (
            _('Important dates'),
            {
                'fields': ('last_login', 'date_joined'),
                'classes': ('collapse',),
            },
        ),
    )

    def get_inline_instances(self, request, obj=None):
        """Show inlines only for already-existing users."""
        if obj is None:
            return []
        return super().get_inline_instances(request, obj=obj)

    def get_readonly_fields(self, request, obj=None):
        return [
            *super().get_readonly_fields(request, obj=obj),
            'user_memberships_link',
            'user_subscriptions_link',
            'user_donations_link',
        ]

    @decorators.short_description('Memberships')
    def user_memberships_link(self, user: models.User) -> str:
        from django.urls import reverse
        from urllib.parse import urljoin

        count = user.customer.memberships.count()
        if count == 0:
            return '-'
        if count == 1:
            admin_link = reverse(
                'admin:blender_fund_main_membership_change',
                kwargs={'object_id': user.customer.memberships.first().pk},
            )
            label = '1 membership'
        else:
            admin_link = urljoin(
                reverse('admin:blender_fund_main_membership_changelist'),
                f'?customer_id={user.customer.pk}',
            )
            label = f'{count} memberships'
        return format_html('<a href="{}">{}</a>', admin_link, label)

    @decorators.short_description('Donations')
    def user_donations_link(self, user: models.User) -> str:
        from django.urls import reverse
        from urllib.parse import urljoin

        count = user.customer.order_set.filter(
            subscription__isnull=True, product__isnull=False
        ).count()
        if count == 0:
            return '-'
        admin_link = urljoin(
            reverse('admin:looper_order_changelist'),
            f'?customer_id={user.customer.pk}',
        )
        label = f'{count} donation{count == 1 and "" or "s"}'
        return format_html('<a href="{}">{}</a>', admin_link, label)

    @decorators.short_description('Subscriptions')
    def user_subscriptions_link(self, user: models.User) -> str:
        from django.urls import reverse
        from urllib.parse import urljoin

        count = user.subscription_set.count()
        if count == 0:
            return '-'
        if count == 1:
            admin_link = reverse(
                'admin:looper_subscription_change',
                kwargs={'object_id': user.memberships.first().pk},
            )
            label = '1 subscription'
        else:
            admin_link = urljoin(
                reverse('admin:looper_subscription_changelist'), f'?customer_id={user.customer.pk}'
            )
            label = f'{count} subscriptions'
        return format_html('<a href="{}">{}</a>', admin_link, label)

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related(
            'customer',
            'customer__subscription_set',
            'customer__memberships',
            'customer__order_set__subscription',
        )


def can_login_as(request, target_user: User) -> bool:
    """Determine who can use the 'login as' feature.

    See https://github.com/skorokithakis/django-loginas
    """

    return request.user.is_staff and request.user.has_perm('auth.can_loginas')


@admin.register(models.Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'category', 'created_at']
    readonly_fields = ['donation']
    autocomplete_fields = ['membership']


class CustomFlatpageForm(FlatpageForm):
    content = django_forms.CharField(widget=CodeMirror(mode='html'))


class CustomFlatPageAdmin(FlatPageAdmin):
    save_on_top = True
    form = CustomFlatpageForm


class _RenderEmailPreview:
    @property
    def subject(self):
        return self._subject

    @subject.setter
    def subject(self, value):
        self._subject = value

    @property
    def html_message(self):
        """Render the "html_message" attribute of this object."""
        iframe_template = Template(
            '<iframe sandbox="allow-same-origin" style="min-width: 40vw;" srcdoc="{{ body|safe }}"'
            'onload="this.style.height=(this.contentWindow.document.body.scrollHeight + 16)+\'px\';">'  # noqa: E501
            '</iframe>'
        )

        # Escape " and & to avoid breaking srcdoc. Order of escaping is important.
        body = self._html_message.replace('&', '&amp;').replace('"', '&quot;')
        context = Context({'body': body})
        rendered: str = iframe_template.render(context)
        return mark_safe(rendered)

    @html_message.setter
    def html_message(self, value):
        self._html_message = value

    @property
    def message(self):
        _template = Template(
            '''
            <style>
            .divtext {
                border: ridge 0.1em;
                padding: 1em;
                min-height: 5em;
                overflow: auto;
            }
            </style>
            '''
            '<div class="divtext" contentEditable>{{ text|linebreaksbr }}</div>'
        )
        context = Context({'text': self._message})
        rendered: str = _template.render(context)
        return mark_safe(rendered)

    @message.setter
    def message(self, value):
        self._message = value

    def __str__(self):
        return (
            f'Previewing with {"an account-less" if not self.obj.customer.user_id else ""}'
            f' { self.obj }'
        )


class AutomaticPaymentEmailPreview(_RenderEmailPreview, looper.models.Order):
    class Meta:
        proxy = True
        managed = False
        verbose_name = '[Emails previews] Payment notification'


class MembershipChangedEmailPreview(_RenderEmailPreview, models.Membership):
    class Meta:
        proxy = True
        managed = False
        verbose_name = '[Emails previews] Membership notification'


class _EmailPreviewChangelist:
    list_display = ['id', 'subject']
    fields = ['id', 'subject', 'message', 'html_message']
    readonly_fields = ['id', 'subject', 'html_message', 'message']

    def get_changelist_instance(self, request):
        """Monkey-patch changelist replacing the queryset with the existing transactional emails."""
        try:
            cl = super().get_changelist_instance(request)
            emails = self._get_emails_list(request)
            cl.result_list = emails
            cl.result_count = len(emails)
            cl.can_show_all = True
            cl.multi_page = False
            cl.title = 'Select an email to preview it'
            return cl
        except Exception:
            raise


@admin.register(AutomaticPaymentEmailPreview)
class AutomaticPaymentEmailPreviewAdmin(
    looper.admin.mixins.NoChangeMixin,
    looper.admin.mixins.NoAddDeleteMixin,
    _EmailPreviewChangelist,
    admin.ModelAdmin
):
    def _get_emails_list(self, request):
        emails = []
        for mail_name in (
            'payment_paid',
            'payment_soft-failed',
            'payment_failed',
            'donation_received',
        ):
            emails.append(self.get_object(request, object_id=mail_name))
        return emails

    def make_a_fallback_obj(self, mail_name) -> 'looper.models.Order':
        """Create temporary Order object to use in email preview."""
        customer = looper.models.Customer()
        order = looper.models.Order(
            id=9999999,
            price=9999,
            customer=customer,
            email='test@example.com',
            billing_address='Jane Doe\nTestlane 123\nTestville\nRainbowland',
            name='Test donation',
            paid_at=django.utils.timezone.now(),
        )
        transaction = looper.models.Transaction
        payment_method = looper.models.PaymentMethod(
            recognisable_name='Test Payment',
            method_type='cc',
        )
        transaction.payment_method = order.payment_method = payment_method
        if mail_name == 'payment_paid':
            order.status = 'paid'
            transaction.status = 'succeeded'
        elif mail_name == 'payment_soft-failed':
            order.status = 'soft-failed'
            transaction.status = 'failed'
            transaction.failure_message = 'Test soft failure message'
        elif mail_name == 'payment_failed':
            order.status = transaction.status = 'failed'
            transaction.failure_message = 'Test failure message'
        elif mail_name == 'donation_received':
            order.status = 'paid'
            transaction.status = 'succeeded'
        if mail_name != 'donation_received':
            subscription = looper.models.Subscription(customer=customer)
            order.subscription = subscription
            membership = models.Membership(customer=customer)
            subscription.membership = membership
        return order, transaction

    def get_object(self, request, object_id, from_field=None):
        """Construct the Email on th fly from known subscription email templates."""

        mail_name = object_id
        objects_q = looper.models.Order.objects
        _template_function = blender_fund_main.email.construct_payment_mail
        if mail_name == 'payment_paid':
            objects_q = objects_q.filter(status='paid')
        elif mail_name == 'payment_soft-failed':
            objects_q = objects_q.filter(status='soft-failed')
        elif mail_name == 'payment_failed':
            objects_q = objects_q.filter(status='failed')
        elif mail_name == 'donation_received':
            objects_q = objects_q.filter(status='paid', subscription__isnull=True)
            _template_function = blender_fund_main.email.construct_donation_received_mail
        else:
            raise Exception(f'Unknown payment notification email {mail_name}')
        obj = default_obj = objects_q.filter(customer__user__isnull=False).first()
        accountless_obj = objects_q.filter(customer__user__isnull=True).first()
        # Passing a particular order ID via a GET param is possible:
        object_id = request.GET.get('object_id')
        if object_id:
            obj = objects_q.get(pk=object_id)
        transaction = obj and obj.transaction_set.order_by('-id').first()
        # Create temporary objects if no suitable ones found in database
        if not obj:
            obj, transaction = self.make_a_fallback_obj(mail_name)
        if mail_name == 'donation_received':
            email_body_html, email_body_txt, subject = _template_function(obj)
        else:
            email_body_html, email_body_txt, subject = _template_function(
                obj, transaction=transaction
            )
        # "original" is what it's called in Django's change_form template
        original = AutomaticPaymentEmailPreview(
            id=mail_name,
            subject=subject,
            html_message=email_body_html,
            message=email_body_txt,
        )
        original.obj = obj
        original.default_obj = default_obj
        original.accountless_obj = accountless_obj
        return original


@admin.register(MembershipChangedEmailPreview)
class MembershipChangedEmailPreviewAdmin(
    looper.admin.mixins.NoChangeMixin,
    looper.admin.mixins.NoAddDeleteMixin,
    _EmailPreviewChangelist,
    admin.ModelAdmin
):
    def _get_emails_list(self, request):
        emails = []
        for mail_name in (
            'membership_activated',
            'membership_deactivated',
            'membership_bankinfo',
            'managed_memb_notif',
        ):
            emails.append(self.get_object(request, object_id=mail_name))
        return emails

    def get_object(self, request, object_id, from_field=None):
        """Construct the Email on th fly from known subscription email templates."""

        mail_name = object_id
        _template_function = blender_fund_main.email.construct_membership_mail
        if mail_name == 'managed_memb_notif':
            _template_function = blender_fund_main.email.construct_managed_subscription_mail

        objects_q = models.Membership.objects
        if mail_name == 'membership_activated':
            objects_q = objects_q.filter(status='active')
        elif mail_name == 'membership_deactivated':
            objects_q = objects_q.filter(status='inactive', subscription__status='cancelled')
        elif mail_name == 'membership_bankinfo':
            objects_q = objects_q.filter(
                status='inactive',
                subscription__status='on-hold',
                subscription__payment_method__method_type='ba',
            )
        elif mail_name == 'managed_memb_notif':
            objects_q = models.Subscription.objects.filter(
                collection_method='managed',
            )
        else:
            raise Exception(f'Unknown membership notification email {mail_name}')
        obj = default_obj = objects_q.filter(customer__user__isnull=False).first()
        accountless_obj = objects_q.filter(customer__user__isnull=True).first()
        # Passing a particular membership's ID via a GET param is possible:
        object_id = request.GET.get('object_id')
        if object_id:
            obj = objects_q.get(pk=object_id)
        obj = objects_q.first()
        email_body_html, email_body_txt, subject = _template_function(obj)
        # "original" is what it's called in Django's change_form template
        original = MembershipChangedEmailPreview(
            id=mail_name,
            subject=subject,
            html_message=email_body_html,
            message=email_body_txt,
        )
        original.obj = obj
        original.default_obj = default_obj
        original.accountless_obj = accountless_obj
        return original


class FundRevenuePerPlanAndProductAdmin(looper.admin.reports.RevenuePerPlanAndProductAdmin):
    subtotals = (
        (
            'Subtotal Individual sponsors',
            (
                'Bronze',
                'Silver',
                'Gold',
                'Platinum',
                'Titanium',
                'Diamond',
                'Other',
            ),
        ), (
            'Subtotal Corporate sponsors',
            (
                'Corporate_Bronze',
                'Corporate_Silver',
                'Corporate_Gold',
                'Corporate_Platinum',
                'Corporate_Titanium',
                'Corporate_Patron',
                'Corporate Patron',
                'Patron',
            ),
        )
    )

    def row_title(self, r) -> str:
        if isinstance(r, str):
            title = r
        elif isinstance(r, looper.models.Product) and 'Donation' in r.name:
            title = 'Subtotal One-time donations'
        else:
            title = f'{r}'
        if 'Subtotal' in title:
            title = format_html(f'<b>{title.replace("Subtotal", "Subtotal<br>")}<b>')
        return title

    def sort_kwargs(self):
        """Define a sort function for report lines."""
        sort_keys = []
        for subtotal_title, rows in self.subtotals:
            sort_keys.extend(rows)
            sort_keys.append(subtotal_title)

        def f_sort_keys(v) -> int:
            line_title = str(v.plan)
            if line_title in sort_keys:
                return sort_keys.index(line_title)
            return -1
        return f_sort_keys

    def get_report_lines(self, queryset):
        """Add subtotals to the report lines."""
        currency = self.get_currency(queryset)
        report_lines = super().get_report_lines(queryset)
        subtotals_report_lines = []
        # When currency is not selected, EUR is used as a fallback
        currency = currency or 'EUR'
        for subtotal_title, rows in self.subtotals:
            grouped = [r for r in report_lines if self.row_title(r.plan) in rows]
            subtotals_report_lines.append(
                looper.admin.reports.RevenuePerPlanAndProduct(
                    title=str(subtotal_title),
                    currency=currency,
                    plan=subtotal_title,
                    gross_revenue=looper.money.Money(
                        currency,
                        sum(_.gross_revenue.cents for _ in grouped),
                    ),
                    refunds=looper.money.Money(
                        currency,
                        sum(_.refunds.cents for _ in grouped),
                    ),
                    taxes=looper.money.Money(
                        currency,
                        sum(_.taxes.cents for _ in grouped),
                    ),
                    orders_paid=sum(_.orders_paid for _ in grouped),
                    orders_refunded=sum(_.orders_refunded for _ in grouped),
                    days=sum(_.days for _ in grouped),
                )
            )
        all_report_lines = subtotals_report_lines + report_lines
        return sorted(all_report_lines[:-1], key=self.sort_kwargs()) + [report_lines[-1]]


class OrderInline(admin.TabularInline):
    model = models.Campaign.orders.through
    fields = ('added_at', 'order')
    readonly_fields = ('added_at', )
    autocomplete_fields = ['order']
    extra = 0
    verbose_name = 'Campaign order'
    verbose_name_plural = 'Campaign orders'

    def get_queryset(self, request):
        """
        Hide existing campaign orders to avoid breaking the page when there's too many.

        The inline it kept as is to allow adding orders to campaign manually.
        """
        queryset = super().get_queryset(request)
        return queryset.none()


@admin.register(models.Campaign)
class CampaignAdmin(admin.ModelAdmin):
    readonly_fields = ['total_orders']
    prepopulated_fields = {'slug': ('title', )}
    autocomplete_fields = ['orders']
    inlines = [OrderInline]

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related(
            'orders',
            'orders__subscription',
        )

    def total_orders(self, obj):
        return obj.orders.count()


@admin.register(models.Survey)
class SurveyAdmin(admin.ModelAdmin):
    model = models.Survey
    list_display = (
        'id',
        'created_at',
        user_link,
    )

    raw_id_fields = ['user']
    readonly_fields = ['response_data']

    def get_urls(self):
        return [
            path('download/', self.download_surveys_as_jsonl, name='download_surveys_as_jsonl')
        ] + super().get_urls()

    def download_surveys_as_jsonl(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        jsonl_data = '\n'.join(
            [
                json.dumps(
                    {
                        'blender_user_id': s.user.oauth_info.oauth_user_id,
                        'created_at': int(s.created_at.timestamp()),
                        'response_data': s.response_data,
                    }
                )
                for s in models.Survey.objects.select_related('user__oauth_info').all()
            ]
        )
        response = HttpResponse(jsonl_data, content_type='text/jsonl')
        response['Content-Disposition'] = 'attachment; filename=survey_data.jsonl'
        return response


admin.site.unregister(FlatPage)
admin.site.register(FlatPage, CustomFlatPageAdmin)

admin.site.unregister(looper.admin.reports.RevenuePerPlanAndProduct)
admin.site.register(
    looper.admin.reports.RevenuePerPlanAndProduct, FundRevenuePerPlanAndProductAdmin
)
