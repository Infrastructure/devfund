from oauth2_provider.views.generic import ProtectedResourceView
from django.contrib.auth.mixins import LoginRequiredMixin

class SomeProtectedView(LoginRequiredMixin, ProtectedResourceView):
    def get(self, request, *args, **kwargs):
        # Your view logic here
        pass