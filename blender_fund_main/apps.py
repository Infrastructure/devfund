from django.apps import AppConfig


class BlenderFundMainConfig(AppConfig):
    name = 'blender_fund_main'
    verbose_name = 'Blender Fund'

    def ready(self):
        # Import modules to register signal handlers.
        # noinspection PyUnresolvedReferences
        from . import signals
