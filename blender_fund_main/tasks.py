"""Defines background tasks such as handling of Stripe webhook events."""
import inspect
import logging
import sys

from background_task import background
from background_task.models import Task
from background_task.tasks import TaskSchedule
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.transaction import atomic
import django.core.mail

from looper.pdf import PDFResponse
from looper.stripe_utils import (
    upsert_order_from_payment_intent_and_product,
    upsert_subscription_payment_method_from_setup_intent,
)
import looper.models
import looper.stripe_utils
import stripe

from blender_fund_main.utils import is_noreply
from blender_fund_main.email import (
    construct_donation_received_mail,
    construct_managed_subscription_mail,
    construct_membership_mail,
    construct_payment_mail,
)
import blender_fund_main.models

User = get_user_model()
logger = logging.getLogger(__name__)


def attach_receipt_pdf(msg: django.core.mail.EmailMultiAlternatives, order: looper.models.Order):
    """Attach the PDF receipt file to a given email message."""
    assert order.status == 'paid'
    file_name = f'blender-development-fund-receipt-{order.display_number}.pdf'
    pdf_context = {'order': order}
    pdf_response = PDFResponse(None, 'looper/settings/receipt_pdf.html', context=pdf_context)
    file_data = pdf_response.rendered_content
    msg.attach(file_name, file_data, 'application/pdf')
    logger.info('Attached receipt PDF of order pk=%s to email', order.pk)


def _attach_latest_receipt_pdf(
    msg: django.core.mail.EmailMultiAlternatives,
    membership: blender_fund_main.models.Membership,
) -> None:
    if membership.status != 'active':
        return
    subscription = membership.subscription
    if not subscription:
        logger.warning("Membership pk=%s has no subscription, won't attach receipt", membership.pk)
        return
    order = subscription.latest_order()
    if not order:
        logger.warning("Subscription pk=%s has no order, won't attach receipt", subscription.pk)
        return
    if order.status != 'paid':
        logger.warning("Latest order pk=%s has is not paid, won't attach receipt", order.pk)
        return
    attach_receipt_pdf(msg, order)


@atomic
def _retry_task_later(task_args, task_kwargs):
    """Reschedules the task from which this function was called."""
    # Current frame is 2 frames away from the task: this frame + the atomic decorator
    frame = inspect.currentframe().f_back.f_back
    f_name = frame.f_code.co_name
    # Construct a task with the same arguments to calculate its hash:
    _fake_task = Task.objects.new_task(f'{__name__}.{f_name}', args=task_args, kwargs=task_kwargs)
    task_hash = _fake_task.task_hash

    task_q = Task.objects.filter(task_hash=task_hash)
    task = task_q.get()
    # Removing the ID of the task instance will cause a new record to be inserted
    # when it is saved by .reschedule(...)
    task.pk = None
    t, e, traceback = sys.exc_info()
    task.reschedule(t, e, traceback)


# Run this handler with 30 seconds delay to avoid racing with the synchronous success page
@background(schedule={'action': TaskSchedule.RESCHEDULE_EXISTING, 'run_at': 30})
def handle_payment_intent_succeeded(payload: str):
    """Handle payload of payment_intent.succeeded event."""
    payment_intent = payload.get('data', {}).get('object', {})
    metadata = payment_intent.get('metadata', {})
    plan_variation_id = metadata.get('plan_variation_id', None)

    if plan_variation_id:
        looper.stripe_utils.create_active_subscription_from_payment_intent(
            payload,
            plan_variation_id,
        )
    else:
        # Hardcoding ID of the one-time donation product because we don't currently
        # have another way of associating it with Stripe's payment intents.
        old_status, order = upsert_order_from_payment_intent_and_product(
            payload, product_id=2,
        )
        new_status = order.status
        if order.subscription is None and old_status != new_status and new_status == 'paid':
            send_mail_donation_received(order_id=order.pk)


@background()
def handle_setup_intent_succeeded(payload: str):
    """Handle payload of setup_intent.succeeded event."""
    return upsert_subscription_payment_method_from_setup_intent(payload)


@background(schedule={'action': TaskSchedule.RESCHEDULE_EXISTING})
def update_donation_display_name(stripe_session_id: str, display_name: str):
    """Update donation's display name."""
    kwargs = {
        'stripe_session_id': stripe_session_id,
        'display_name': display_name,
    }
    session = stripe.checkout.Session.retrieve(stripe_session_id)
    gateway_order_id = session.payment_intent
    order_q = looper.models.Order.objects.filter(
        gateway_order_ids__gateway_order_id=gateway_order_id
    )
    # Reschedule itself if no matching order is found
    if not order_q.exists():
        _retry_task_later(task_args=[], task_kwargs=kwargs)
        return
    order = order_q.get()
    donation = order.donation
    with atomic():
        donation.display_name = display_name
        donation.save(update_fields={'display_name'})
        activity = donation.activity
        activity.title = donation.activity_text
        activity.save(update_fields={'title'})


@background(schedule={'action': TaskSchedule.RESCHEDULE_EXISTING})
def grant_or_revoke_badge(user_id: int, grant: str = '', revoke: str = ''):
    """Call Blender ID API to grant/revoke given badges to/from a user with given ID."""
    from . import badges
    if not grant and not revoke:
        logger.warning('Nothing to grant or revoke')
        return
    user = User.objects.get(pk=user_id)
    badges.change_badge(user=user, revoke=revoke, grant=grant)


@background(schedule={'action': TaskSchedule.RESCHEDULE_EXISTING})
def send_mail_automatic_payment_performed(order_id: int, transaction_id: int):
    """Send out an email notifying about the status of an automated payment."""
    order = looper.models.Order.objects.get(pk=order_id)
    transaction = looper.models.Transaction.objects.get(pk=transaction_id)
    email = order.email
    if is_noreply(email):
        logger.debug('Not sending payment notification to no-reply address %s', email)
        return

    email_body_html, email_body_txt, subject = construct_payment_mail(
        order=order, transaction=transaction
    )

    logger.debug('Sending payment %r notification to %s', order.status, email)
    msg = django.core.mail.EmailMultiAlternatives(
        subject=subject,
        body=email_body_txt,
        from_email=None,  # just use the configured default From-address.
        to=[email],
    )
    msg.attach_alternative(email_body_html, 'text/html')

    if order.status == 'paid':
        attach_receipt_pdf(msg, order)

    msg.send(fail_silently=False)
    logger.info('Sent %r notification to %s', order.status, email)


@background(schedule={'action': TaskSchedule.RESCHEDULE_EXISTING})
def send_mail_membership_status_changed(membership_id: int):
    """Send out an email notifying about changed status of a membership."""
    membership = blender_fund_main.models.Membership.objects.get(pk=membership_id)
    email = membership.customer.billing_address.email
    if is_noreply(email):
        logger.debug('Not sending membership-changed notification to no-reply address %s', email)
        return

    email_body_html, email_body_txt, subject = construct_membership_mail(
        membership=membership
    )
    logger.debug('Sending membership-changed notification to %s', email)

    msg = django.core.mail.EmailMultiAlternatives(
        subject=subject,
        body=email_body_txt,
        from_email=None,  # just use the configured default From-address.
        to=[email],
    )
    msg.attach_alternative(email_body_html, 'text/html')

    # If membership was activated, include receipt PDF of the latest paid order
    _attach_latest_receipt_pdf(msg, membership)

    msg.send(fail_silently=False)
    logger.info('Sent membership-changed notification to %s', email)


@background(schedule={'action': TaskSchedule.RESCHEDULE_EXISTING})
def send_mail_managed_subscription_notification(subscription_id: int):
    """Send out an email notifying a manager about an expiring managed subscription."""
    subscription = looper.models.Subscription.objects.get(pk=subscription_id)
    email = settings.LOOPER_MANAGER_MAIL
    logger.debug(
        'Notifying %s about managed subscription %r passing its next_payment date',
        email,
        subscription_id,
    )

    email_body_html, email_body_txt, subject = construct_managed_subscription_mail(
        subscription=subscription
    )

    django.core.mail.send_mail(
        subject,
        message=email_body_txt,
        html_message=email_body_html,
        from_email=None,  # just use the configured default From-address.
        recipient_list=[email],
        fail_silently=False,
    )
    logger.info(
        'Notified %s about managed subscription %r passing its next_payment date',
        email,
        subscription_id,
    )


@background(schedule={'action': TaskSchedule.RESCHEDULE_EXISTING})
def send_mail_donation_received(order_id: int):
    """Send out an email notifying about a successful payment (one-time donation)."""
    order = looper.models.Order.objects.get(pk=order_id)
    assert order.subscription is None, (
        f'Expected a one-time donation order, got subscription order pk={order.pk}'
    )
    assert order.status == 'paid', f'Expected paid order, got {order.status}'
    email = order.email
    if is_noreply(email):
        logger.debug('Not sending donation notification to no-reply address %s', email)
        return
    email_body_html, email_body_txt, subject = construct_donation_received_mail(order)

    logger.debug('Sending donation %r notification to %s', order.status, email)

    msg = django.core.mail.EmailMultiAlternatives(
        subject=subject,
        body=email_body_txt,
        from_email=None,  # just use the configured default From-address.
        to=[email],
    )
    msg.attach_alternative(email_body_html, 'text/html')

    attach_receipt_pdf(msg, order)

    msg.send(fail_silently=False)
    logger.info('Sent donation %r notification to %s', order.status, email)
