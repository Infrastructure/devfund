from django.urls import path, reverse_lazy
from django.views.generic import RedirectView

from looper.views import settings as looper_settings
import looper.views.checkout_stripe as checkout_stripe

from .views import (
    activity,
    development,
    errors,
    express_checkout,
    landing,
    link_membership_or_donation,
    settings,
    survey,
    webhooks,
)

urlpatterns = [
    path('', landing.landing, name='landing'),
    path('campaign/', landing.landing_campaign, name='landing_campaign'),
    path('campaign/orders_raw', landing.get_orders_json_raw, name='orders_json_campaign_raw'),
    path('thanks/', RedirectView.as_view(url=reverse_lazy('landing'))),
    # The settings/ path prefix is shared between this module and looper.urls.
    path('settings/', settings.settings_home, name='settings_home'),
    path('settings/receipts/', looper_settings.settings_receipts, name='settings_receipts'),
    path(
        'settings/receipts/<int:order_id>',
        looper_settings.ReceiptView.as_view(),
        name='settings_receipt',
    ),
    path(
        'settings/receipts/blender-fund-<int:order_id>.pdf',
        looper_settings.ReceiptPDFView.as_view(),
        name='settings_receipt_pdf',
    ),
    path('settings/membership/', RedirectView.as_view(
        url=reverse_lazy('settings_home'),
        permanent=False)),
    path('settings/membership/<int:membership_id>', settings.MembershipView.as_view(),
         name='settings_membership_edit'),
    path('settings/membership/<int:membership_id>/cancel', settings.CancelMembershipView.as_view(),
         name='settings_membership_cancel'),
    path('settings/membership/cancelviatoken/<token>',
         settings.CancelMembershipViaTokenView.as_view(),
         name='settings_membership_cancel_via_token'),

    path(
        'settings/billing/payment-methods/change/<int:subscription_id>',
        looper_settings.PaymentMethodChangeView.as_view(
            success_url='payment_method_change_done',
            cancel_url='settings_home',
        ),
        name='payment_method_change',
    ),
    path(
        'settings/billing/payment-methods/change-done/<int:subscription_id>/<stripe_session_id>',
        # TODO? settings_home redirects to settings_membership_edit if there is just one membership
        # so this is good for 99%
        # as an improvement we could make success_url parameter a callable that does a proper
        # pk lookup and explicitly redirects to settings_membership_edit
        looper_settings.PaymentMethodChangeDoneView.as_view(success_url='settings_home'),
        name='payment_method_change_done',
    ),

    path(
        'checkout/pay/<int:order_id>',
        checkout_stripe.CheckoutExistingOrderView.as_view(cancel_url='settings_home'),
        name='checkout_existing_order',
    ),

    # Flexible payment
    path('settings/membership/<int:membership_id>/extend', settings.ExtendMembershipView.as_view(),
         name='membership_extend'),
    path('settings/membership/<int:membership_id>/extend-done/<stripe_session_id>',
         settings.ExtendMembershipDoneView.as_view(),
         name='membership_extend_done'),
    path('settings/membership/<int:membership_id>/extend-not-possible',
         settings.ExtendMembershipNotPossibleView.as_view(),
         name='membership_extend_not_possible'),

    path('my-permissions', development.my_permissions),
    path('test-error/500', errors.test_error_500),
    path('activity/', activity.ActivityListView.as_view(), name='activity-list'),
    path('activity/reports/', activity.ReportsView.as_view(), name='activity-reports'),
    path(
        'link-membership/<token>/',
        link_membership_or_donation.LinkMembershipView.as_view(),
        name='link_membership',
    ),
    path(
        'link-donation/<token>/',
        link_membership_or_donation.LinkDonationView.as_view(),
        name='link_donation',
    ),
    path(
        'express-checkout/',
        express_checkout.ExpressCheckoutDonateOnceView.as_view(),
        name='express_checkout_donate_once',
    ),
    path(
        'express-checkout/<int:plan_variation>/',
        express_checkout.ExpressCheckoutPlanVariationView.as_view(),
        name='express_checkout_plan_variation',
    ),
    path(
        'donate-once-thank-you/',
        express_checkout.DonateOnceThankYou.as_view(),
        name='donate_once_thank_you'
    ),
    path(
        'donate-once-thank-you/display-name/',
        express_checkout.save_donation_display_name,
        name='save_donation_display_name',
    ),
    path(
        'stripe-success-create-subscription/<stripe_session_id>',
        checkout_stripe.StripeSuccessCreateSubscriptionView.as_view(),
        name='stripe_success_create_subscription',
    ),
    path('webhooks/stripe/', webhooks.StripeWebhookView.as_view(), name='webhook_stripe'),
    path(
        'survey/',
        survey.SurveyView.as_view(),
        name='survey'
    ),
    path(
        'survey/done/',
        survey.SurveyDoneView.as_view(),
        name='survey-done'
    ),
]
