---
gitea: none
include_toc: true
---

# Blender Development Fund

## Requirements

* Python 3.10
* [virtualenv](https://pypi.org/project/virtualenv/)

## Development

### Setup

Create and activate a virtual environment using your favourite method, e.g.

    python3.10 -m venv .venv
    source .venv/bin/activate

Install required packages:

    pip install -r requirements_dev.txt

Make a copy of example `.env`:

    cp .env.example .env

Initialise submodules included into this repo:

    git submodule update --init

Create the database tables and load some basic data to work with using the following commands:

    ./manage.py migrate
    ./manage.py loaddata systemuser devfund default_site
    ./manage.py collectmedia --noinput

Update `/etc/hosts` to point to `fund.local`, e.g.:

    127.0.0.1	fund.local

Create superuser:

    ./manage.py createsuperuser

The last command creates an admin account that can be used to log in at http://fund.local:8010/admin/.

Run development server

    ./manage.py runserver 8010

### Blender ID

Blender DevFund, as all other Blender web services, uses Blender ID.

For development, Blender ID's code contains a fixture with an OAuth app
that should work without any changes to default configuration.
To load this fixture, go to your development Blender ID and run the following:

    ./manage.py loaddata blender_development_fund_devserver

## Stripe integration

TODO improve this section.

Follow the official guide for setting up Stripe CLI: https://stripe.com/docs/development/quickstart

### Webhook testing

Run a local listener that will make webhook requests to your development server:
```
stripe listen --forward-to fund.local:8010/webhooks/stripe/
```

Run background tasks that are created by the webhook code:
```
./manage.py process_tasks
```

## Documentation

See [docs](docs) for more detailed documentation.

## Testing

Run `./manage.py test --parallel` to run all project's tests.

Use the `--reuse-db` option to speed up subsequent test runs.
However, this skips the database migrations; add `--create-db` on the CLI to
recreate the test database and run all the migrations when necessary.


## GeoIP

The GeoIP database was downloaded from
[GeoLite2-Country](http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz)

# Deploy

See [playbooks](playbooks#deploy).
