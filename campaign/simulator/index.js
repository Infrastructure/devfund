import { writeJsonFile } from 'write-json-file';
import Matter from 'matter-js';
import fetch from 'node-fetch';

const WIDTH = 1920;
const HEIGHT = 1080;
const SIM_TIME_MS = 5000;

// Data - the presents!
const masses = [5, 10, 20, 60, 120, 300, 600, 5*12, 10*12, 25*12, 50*12, 100*12, 250*12]; // in euros :)
function getTierIdx(giftAmount, giftType) {
  // Match the order values given by the backend with the suggested tiers offered in the front end in euros.
  giftAmount = giftAmount / 100; // cents to euro units.
  // Recurring monthly subscriptions ('s'ubscribed, 'r'enewed).
  if (giftType === 'r' || giftType === 's') {
    giftAmount = giftAmount * 12;
  }

  // Look up the specified amount in the array, picking the closest tier in euros.
  const minTierIdx = (giftType === 'd') ? 0 : 7; // Donations ('d') are in the first half.
  for (let idx = (giftType === 'd') ? 5 : 11;  idx >= minTierIdx ; idx--) {
    // If the value is bigger than this tier and closer to higher tier than to this one, we found it: return the highest.
    const upperRange = masses[idx+1] - masses[idx];
    if (giftAmount > masses[idx] + upperRange * 0.5) {
      return idx+1;
    }
  }
  return minTierIdx;
}

let widthVariancePercent = [      // How much longer is one side vs the other.
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0,   // e.g. 0.5 = one side is 50% longer than the other.
  0.0, 0.7, 0.3, 0.5, 0.25, 0.1]; // 0.0 is a square. Don't type 1.0 ;)

const scale = 8 / Math.sqrt(12);  // Visual size based on gift value is balanced with "monthly factor".
const sidesPx = masses.map((v) => Math.sqrt(v) * scale);
const widthsPx =  sidesPx.map((v, i) => v + v * widthVariancePercent[i]);
const heightsPx = widthsPx.map((v, i) => sidesPx[i]*sidesPx[i] / v);
if (widthsPx[0] < 5 || heightsPx[0] < 5 || sidesPx[0] < 5)
  console.warn("Smallest present is only", widthsPx[0].toFixed(1), "x", heightsPx[0].toFixed(1),
    "px wide. It won't be well visible. Increase 'scale' and make it moar square.");
//console.log("Present widths in px:", widthsPx);
//console.log("Present heights in px:", heightsPx);

// Simulation Configuration
const gravityScale = 0.0003; // Controls the speed of the fall and how compressed the presents get.

// create runner - update loop
const runner = Matter.Runner.create({
  isFixed: true // Fixed or variable timestep. Fixed is deterministic.
});

async function fetchOrdersJSON() {
  const response = await fetch('http://fund.local:8005/campaign/orders_raw');
  const orders = await response.json();
  return orders['orders'];
}

async function writeJson(data) {
  await writeJsonFile('../../static/offline-simulation-data.json', data);
}

function setup(orders, engine) {
  // Create the setup.
  addWalls(engine.world, WIDTH, HEIGHT);
  addPresents(engine.world, WIDTH, HEIGHT, orders);
}

function addWalls(world, worldWidthPx, worldHeightPx) {
  // Make walls relatively thick, so that objects don't go through.
  // Position them around (outside) the simulation area.
  const wall = 50;
  const h = worldHeightPx + wall;
  const simOptions = { isStatic: true, render: { opacity: 1 }, label: "border"};
  Matter.Composite.add(world, [
    Matter.Bodies.rectangle(worldWidthPx*0.5, worldHeightPx+wall*0.5, worldWidthPx + wall, wall, simOptions), // Floor
    Matter.Bodies.rectangle(worldWidthPx + wall*0.5, worldHeightPx*0.5, wall, h, simOptions), // Right
    Matter.Bodies.rectangle(           0 - wall*0.5, worldHeightPx*0.5, wall, h, simOptions)  // Left
  ]);
}

function addPresents(world, worldWidthPx, worldHeightPx, orders) {

  // Calculate grid layout.
  const gap = 3; // num pixels in between gifts and the simulation margin, so things don't drag on each other.
  const gridCellSize = Math.floor(widthsPx[widthsPx.length - 3] + gap); // Temp: use third largest gift size.
  // const numCols = Math.ceil(worldWidthPx / gridCellSize);
  // const numRows = Math.ceil(totalPresents / numCols);
  //console.log("Distributing", totalPresents, "gifts in", numCols, "cols,", numRows, "rows,", gridCellSize, "px cell side");
  //console.log(numCols * gridCellSize, worldWidthPx, worldWidthPx - (numCols * gridCellSize));
  const startY = worldHeightPx - 50;
  const startX = gap;

  // Create the matter.js bodies in a "stack" arrangement.
  let presentsGroup = Matter.Composite.create({ label: 'PresentStack' });

  let x = startX;
  let y = startY;
  let row = 0;
  let maxObservedHeightInRow = 0;
  for (let p=0; p<orders.length; p++) {
    let tierIdx = getTierIdx(orders[p][1], orders[p][2]);

    // Created the gift for the mattter.js simulation.
    // Create the shape.
    const width = widthsPx[tierIdx];
    const height = heightsPx[tierIdx];
    let body = Matter.Bodies.rectangle(x, y, width, height, {label: String(p)})

    // Re-query the bounds in case we introduce the shapes with an initial random orientation.
    let bodyHeight = body.bounds.max.y - body.bounds.min.y;
    let bodyWidth  = body.bounds.max.x - body.bounds.min.x;
    if (bodyHeight > maxObservedHeightInRow)
      maxObservedHeightInRow = bodyHeight;

    // Add the shape.
    Matter.Body.translate(body, { x: bodyWidth * 0.5, y: bodyHeight * 0.5 });
    Matter.Body.rotate(body, Math.random()*20-10);
    Matter.Composite.addBody(presentsGroup, body);

    x = body.bounds.max.x + gap;

    if (x > worldWidthPx - gap) {
      x = startX;
      y -= maxObservedHeightInRow + gap;
      maxObservedHeightInRow = 0;
      row ++;
    }
  }

  Matter.Composite.add(world, presentsGroup);
}

function checkCanvasOccupancy(counts) {
  // Check if the volume of presents will roughly fit the canvas size.
  const totalRequestedHorizontalSide = widthsPx.reduce((result, v, i) => result + v * counts[i]);
  const totalRequestedArea = Math.ceil(totalRequestedHorizontalSide*heightsPx[heightsPx.length-1]);
  const totalAvailableArea = WIDTH * HEIGHT;
  const occupancyFactor = totalRequestedArea / totalAvailableArea;
  if (occupancyFactor > 2.5) {
    const extraGiftsFactor = occupancyFactor - 1.5;
    const neededSide = Math.ceil(Math.sqrt(totalRequestedArea));
    console.warn("Gift Sim Setup: too many gifts for the given canvas space!!",
      "Reduce the nr of gifts by", extraGiftsFactor.toFixed(1),
      "times, or increase the canvas size, or reduce 'scale'.");
  }
}

function generateCounts(orders) {
  let counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  for (let i=0; i < orders.length; i++) {
    const tierIdx = getTierIdx(orders[i][1], orders[i][2]);
    counts[tierIdx]++;
  }
  return counts;
}

function gatherStats(orders) {
  // Gift purchases coming from the backend.
  const totalPresents = orders.length;
  const counts = generateCounts(orders);
  console.log("Present counts:", counts, " Total:", totalPresents);
}

function dumpData(engine) {
  const bodyGroup = engine.world.composites[0];
  const bodies = Matter.Composite.allBodies(bodyGroup).map(item => {
    return {
      id: item.id,
      label: item.label,
      position: item.position,
      angle: item.angle,
    };
  });
  writeJson({
    bodies: bodies,
    gravityScale: engine.gravity.scale,
    width: WIDTH,
    height: HEIGHT}
  );
}

function stopAndDump(runner, engine) {
  Matter.Runner.stop(runner);
  dumpData(engine, engine.world);
}

function run(runner, engine) {
  Matter.Runner.run(runner, engine);
  setTimeout(stopAndDump, SIM_TIME_MS, runner, engine);
  // console.log(Matter.Composite.allBodies(engine.world).length);
  // console.log(Matter.Composite.allBodies(engine.world));
}

fetchOrdersJSON().then(orders => {
  // create engine
  const engine = Matter.Engine.create({
    positionIterations: 10,
    velocityIterations: 10,
    enableSleeping: true,
    gravity: { scale: gravityScale },
  });
  gatherStats(orders);
  checkCanvasOccupancy(generateCounts(orders));
  setup(orders, engine);
  run(runner, engine);
})
