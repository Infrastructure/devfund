FROM python:3.10

RUN python -m venv /venv && \
    /venv/bin/pip install gunicorn django-debug-toolbar

ADD requirements.txt /requirements.txt
RUN /venv/bin/pip install -r /requirements.txt

ADD requirements_dev.txt /requirements_dev.txt
RUN /venv/bin/pip install -r /requirements_dev.txt

COPY . /app
WORKDIR /app
RUN git submodule update --init

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["/venv/bin/gunicorn", "blender_fund.wsgi:application", "--bind", "0.0.0.0:8000"]
