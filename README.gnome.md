```
git submodule update --init
docker compose up --build
```

In another terminal:

```
docker compose exec devfund /venv/bin/python3 manage.py migrate
docker compose exec devfund /venv/bin/python3 manage.py loaddata systemuser devfund default_site
docker compose exec devfund /venv/bin/python3 manage.py collectmedia --noinput
docker compose exec devfund /venv/bin/python3 manage.py collectstatic --noinput
docker compose exec devfund /venv/bin/python3 manage.py createsuperuser
```

The last command creates admin user. Log in at http://localhost:8000/admin/.
