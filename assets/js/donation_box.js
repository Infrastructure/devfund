$(function () {
  const $navButtons = $("#donation-box [role=tablist] button");
  const $panels = $("#donation-box section[role=tabpanel]");
  const $body = $("body");

  $navButtons.click(function (event) {
    $navButtons.each(function () {
      const $this = $(this);
      $this.removeClass("bg-white");
      $this.attr("aria-selected", "false");
    });

    const $target = $(event.target);
    $target.addClass("bg-white");
    $target.attr("aria-selected", "true");

    $panels.each(function () {
      const $this = $(this);

      $this.addClass("hidden");
      $this.attr("aria-hidden", "true");
    });

    const panelId = $target.attr("aria-controls");
    const $targetPanel = $("#" + panelId);
    $targetPanel.removeClass("hidden");
    $targetPanel.attr("aria-hidden", "true");

    $('#donation-box input[type="radio"]').prop("checked", false);

    $body.removeClass();
    if (panelId === "one-time") {
      $body.addClass("group is-custom");
    }
    if (panelId === "monthly") {
      // check gold monthly donation - it is default
      // it doesn't make sense to not have anything selected
      $('#donation-box #monthly-option3').prop("checked", true);
      $('#donation-box #monthly-option3').trigger("change");
    }
  });

  $("#donation-box #custom-amount").on("input", function (event) {
    let field = event.target;
    let errorContainerId = field.getAttribute("aria-describedby");

    if (!field.checkValidity()) {
      field.setAttribute("aria-invalid", "true");
      console.log("validation message", field.validationMessage);
      $("#" + errorContainerId).text(field.validationMessage);

      $("#submit-donation-once").attr("disabled", true);
    } else {
      field.setAttribute("aria-invalid", "false");
      $("#" + errorContainerId).text("");
      $("#submit-donation-once").attr("disabled", false);
    }

    $('#one-time-donation input[type="radio"]').prop("checked", false);
  });

  $('#one-time-donation input[type="radio"]').on("change", function () {
    $("#donation-box #custom-amount").prop("value", "");
  });

  $("#monthly-donation input[type='radio']").on("change", function (event) {
    let $section = $("#monthly");
    let $tierName = $("#tier-name");
    let $websiteName = $("#website-name");
    let $websiteLink = $("#website-link");
    let value = $(event.target).val();

    $body.removeClass();

    switch (value) {
      case "5":
        $tierName.text("Bronze");
        $websiteLink.addClass("hidden");
        $websiteName.addClass("hidden");
        $section.removeClass().addClass("group is-bronze");
        changeColors("#8C7853");
        break;
      case "10":
        $tierName.text("Silver");
        $websiteLink.addClass("hidden");
        $websiteName.addClass("hidden");
        $section.removeClass().addClass("group is-silver");
        changeColors("#c0c0c0");
        break;
      case "25":
        $tierName.text("Gold");
        $websiteLink.addClass("hidden");
        $websiteName.removeClass("hidden");
        $section.removeClass().addClass("group is-gold");
        changeColors("#E5A50A");
        break;
      case "50":
        $tierName.text("Platinum");
        $websiteLink.addClass("hidden");
        $websiteName.removeClass("hidden");
        $section.removeClass().addClass("group is-platinium");
        changeColors("#e5e4e2");
        break;
      case "100":
        $tierName.text("Titanium");
        $websiteLink.removeClass("hidden");
        $websiteName.addClass("hidden");
        $section.removeClass().addClass("group is-titanium");
        changeColors("#878681");
        break;
      case "250":
        $tierName.text("Diamond");
        $websiteLink.removeClass("hidden");
        $websiteName.addClass("hidden");
        $section.removeClass().addClass("group is-diamond");
        $body.addClass("group is-diamond");
        changeColors("#6D4DCB");
        break;
    }
  });

  function changeColors(color) {
    $("#badge-container svg > path").attr("fill", color);
    $("#benefit-list svg > path").attr("stroke", color);
  }

  $("#currencies").on("change", function () {
    let value = $(this).val();
    let currencyValue = value === "USD" ? "usd" : "eur";
    let currencySymbol = value === "USD" ? "$" : "€";

    $("[data-currency]").each(function () {
      $(this).text(currencySymbol);
    });

    $("#donation-currency").val(currencyValue);
  });

  $("#donation-box form").on("submit", function (event) {
    event.preventDefault();

    let form = event.currentTarget;
    let formData = Object.fromEntries(new FormData(form));
    let url = "";
    let currencyValue = formData.currencyValue;

    if (formData.customAmount !== "") {
      url = getOneTimeUrl(formData.customAmount, currencyValue);
    } else if (formData.oneTimeOption) {
      url = getOneTimeUrl(formData.oneTimeOption, currencyValue);
    } else if (formData.monthlyOption) {
      let currencySymbol = currencyValue === "usd" ? "$" : "€";
      let option = {
        "€5": "1",
        "$5": "2",
        "€10": "5",
        "$10": "6",
        "€25": "7",
        "$25": "8",
        "€50": "9",
        "$50": "10",
        "€100": "11",
        "$100": "12",
        "€250": "15",
        "$250": "16",
      }[currencySymbol + formData.monthlyOption];

      url = CHECKOUT_URL + option;
    }

    if (url) {
      window.open(url, "_blank").focus();
    }
  });

  const CHECKOUT_URL = "https://fund.blender.org/express-checkout/";

  function getOneTimeUrl(value, currency) {
    let valueInCents = Number(value).toFixed(2).replace(".", "");
    return CHECKOUT_URL + "?valueAmount=" + valueInCents + "&valueCurrency=" + currency;
  }
});
