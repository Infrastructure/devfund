#!/bin/bash
set -x

/venv/bin/python3 manage.py migrate
/venv/bin/python3 manage.py loaddata systemuser devfund default_site
/venv/bin/python3 manage.py collectmedia --noinput
/venv/bin/python3 manage.py collectstatic --noinput
exec $@
