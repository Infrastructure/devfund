"""Django settings module.

All configuration is supplied via environment variables.
"""

import os
import pathlib
import sys

import braintree
import dj_database_url
from dateutil.relativedelta import relativedelta
from django.urls import reverse_lazy
from looper.money import Money

try:
    from dotenv import load_dotenv

    # Load variables from .env, if available
    path = os.path.dirname(os.path.abspath(__file__)) + "/../.env"
    if os.path.isfile(path):
        load_dotenv(path)
except ImportError:  # This is expected: there should be no python-dotenv in production
    pass


def _get(name: str, default=None, coerse_to=None):
    val = os.environ.get(name, default)
    return coerse_to(val) if coerse_to is not None else val


BASE_DIR = pathlib.Path(__file__).absolute().parent.parent
TESTING = sys.argv[1:2] == ["test"]

SITE_ID = 1

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = _get("SECRET_KEY", "DEVELOPMENT-ONLY NON SECRET NEVER USE IN PRODUCTION")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = _get("DEBUG", False, bool)
TEMPLATE_DEBUG = DEBUG

# ALLOWED_HOSTS = _get('ALLOWED_HOSTS', 'fund.local', str).split(',')
# CSRF_TRUSTED_ORIGINS = _get('CSRF_TRUSTED_ORIGINS', 'fund.local', str).split(',')

ALLOWED_HOSTS = _get("ALLOWED_HOSTS", "http://localhost", str).split(",")
CSRF_TRUSTED_ORIGINS = _get("CSRF_TRUSTED_ORIGINS", "http://localhost", str).split(",")

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.admindocs",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.sites",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
    "django.contrib.flatpages",
    "django_countries",
    "pipeline",
    "codemirror",
    "loginas",
    "blender_notes",
    "looper",
    "blender_fund_main",
    "logentry_admin",
    "nested_admin",
    "background_task",
    "waffle",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "allauth.socialaccount.providers.openid_connect",
]

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.flatpages.middleware.FlatpageFallbackMiddleware",
    "looper.middleware.PreferredCurrencyMiddleware",
    "waffle.middleware.WaffleMiddleware",
    "allauth.account.middleware.AccountMiddleware",
]

ROOT_URLCONF = "blender_fund.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            BASE_DIR / "templates",
            BASE_DIR / "assets_shared" / "src" / "templates",
            BASE_DIR / "donation-box" / "dist",
        ],
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "blender_fund_main.context_processors.settings",
                "blender_fund_main.context_processors.page_id",
                "looper.context_processors.preferred_currency",
                "django.template.context_processors.request",
            ],
            "loaders": {
                (
                    "pypugjs.ext.django.Loader",
                    (
                        "django.template.loaders.filesystem.Loader",
                        "django.template.loaders.app_directories.Loader",
                    ),
                )
            },
            "builtins": [
                "pypugjs.ext.django.templatetags",
            ],
        },
    }
]

WSGI_APPLICATION = "blender_fund.wsgi.application"

# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases

DEFAULT_DATABASE_URL = "sqlite:///{}".format(BASE_DIR / "db/db.sqlite3")
DATABASE_URL = os.getenv("DATABASE_URL", DEFAULT_DATABASE_URL)
CONN_MAX_AGE = int(os.getenv("CONN_MAX_AGE", 0))
DATABASES = {
    "default": dj_database_url.config(default=DATABASE_URL, conn_max_age=CONN_MAX_AGE),
}

SESSION_ENGINE = "django.contrib.sessions.backends.signed_cookies"

# Password validation
# https://docs.djangoproject.com/en/4.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# https://docs.djangoproject.com/en/4.2/topics/i18n/
LOOPER_MONEY_LOCALE = "en_US.UTF-8"

LANGUAGE_CODE = "en-us"

TIME_ZONE = "Europe/Amsterdam"  # This influences rendering in templates.
USE_TZ = True  # This causes all datetimes to be UTC in the database.

USE_I18N = True
USE_L10N = False
DATE_FORMAT = "l Y-b-d"
TIME_FORMAT = "H:i:s"
# f'{DATE_FORMAT}, {TIME_FORMAT}' is too long for admin, there seem to be no easy way to
# change datetime formatting in the admin only:
DATETIME_FORMAT = f"Y-b-d, {TIME_FORMAT}"
SHORT_DATE_FORMAT = "Y-m-d"
SHORT_DATETIME_FORMAT = f"{SHORT_DATE_FORMAT} H:i"
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.2/howto/static-files/
STATIC_URL = "/static/"

STATICFILES_DIRS = [
    str(BASE_DIR / "assets_shared"),
    str(BASE_DIR / "assets_shared/src/scripts/"),
    str(BASE_DIR / "donation-box/dist"),
    str(BASE_DIR / "assets"),
]

STATIC_ROOT = os.getenv("STATIC_ROOT", BASE_DIR / "static")
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "pipeline.finders.PipelineFinder",
]

LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "default": {"format": "%(asctime)-15s %(levelname)8s %(name)s %(message)s"},
        "verbose": {
            "format": "%(asctime)-15s %(levelname)8s %(name)s %(process)d %(thread)d %(message)s"
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "verbose",  # Set to 'verbose' in production
            "stream": "ext://sys.stderr",
        },
        "mail_admins": {
            "level": "ERROR",
            "class": "django.utils.log.AdminEmailHandler",
            "include_html": True,
        },
    },
    "loggers": {
        "blender_fund": {"level": "DEBUG"},
        "blender_fund_main": {"level": "DEBUG"},
        "looper": {"level": "DEBUG"},
        "background_task": {"level": "INFO"},
    },
    "root": {
        "level": "WARNING",
        "handlers": [
            "console",
            "mail_admins",
        ],
    },
}

PIPELINE = {
    "JS_COMPRESSOR": "pipeline.compressors.jsmin.JSMinCompressor",
    "CSS_COMPRESSOR": "pipeline.compressors.NoopCompressor",
    "JAVASCRIPT": {
        "tutti": {
            "source_filenames": [
                "blender_fund_main/scripts/tutti/*.js",
            ],
            "output_filename": "js/tutti.js",
            "extra_context": {"async": False, "defer": False},
        },
        "blender_notes": {
            "source_filenames": ("blender_notes/scripts/*.js",),
            "output_filename": "js/blender_notes.js",
            "extra_context": {"async": False, "defer": False},
        },
        "looper": {
            "source_filenames": [
                "looper/scripts/*.js",
            ],
            "output_filename": "js/looper.js",
            "extra_context": {"async": False, "defer": False},
        },
        "looper_currency": {
            # Separate script for currency selector for pages that don't need a bundle
            "source_filenames": [
                "looper/scripts/currency.js",
            ],
            "output_filename": "js/looper_currency.js",
            "extra_context": {"async": False, "defer": False},
        },
        "web-assets": {
            "source_filenames": ("tutti/10_navbar.js",),
            "output_filename": "js/web-assets.js",
            "extra_context": {"async": True, "defer": True},
        },
        "choices": {
            "source_filenames": ("js/vendor/choices.min.js",),
            "output_filename": "js/vendor/choices.min.js",
        },
        "donation-box": {
            "source_filenames": ("js/donation-box.js",),
            "output_filename": "js/donation-box.js",
            "extra_context": {"async": False, "defer": False},
        },
        "donation_box": {
            "source_filenames": ("js/donation_box.js",),
            "output_filename": "js/donation_box.js",
            "extra_context": {"async": False, "defer": False},
        },
        "donation-campaign": {
            "source_filenames": (
                "js/vendor/matter.min.js",
                "js/vendor/present-renderer.js",
                "js/campaign.js",
            ),
            "output_filename": "js/donation-campaign.js",
        },
    },
    "STYLESHEETS": {
        "blender_notes": {
            "source_filenames": ("blender_notes/styles/blender_notes.sass",),
            "output_filename": "css/blender_notes.css",
            "extra_context": {"media": "screen,projection"},
        },
        "main": {
            "source_filenames": ("blender_fund_main/styles/main.sass",),
            "output_filename": "css/main.css",
            "extra_context": {"media": "screen,projection"},
        },
        "choices": {
            "source_filenames": ("blender_fund_main/styles/vendor/choices.scss",),
            "output_filename": "css/choices.css",
            "extra_context": {"media": "screen,projection"},
        },
        "looper_admin": {
            "source_filenames": ("looper/styles/*.sass",),
            "output_filename": "css/looper_admin.css",
            "extra_context": {"media": "screen,projection"},
        },
        "campaign": {
            "source_filenames": ("blender_fund_main/styles/campaign.sass",),
            "output_filename": "css/campaign.css",
            "extra_context": {"media": "screen,projection"},
        },
    },
    "COMPILERS": ("libsasscompiler.LibSassCompiler",),
    "DISABLE_WRAPPER": True,
}

# Cache busting for static files.
# See https://docs.djangoproject.com/en/4.2/ref/contrib/staticfiles/#manifeststaticfilesstorage
STATICFILES_STORAGE = "pipeline.storage.PipelineManifestStorage"

# Uploaded files
# https://docs.djangoproject.com/en/4.2/ref/settings/#std:setting-MEDIA_ROOT
MEDIA_URL = "/media/"
MEDIA_ROOT = os.getenv(
    "MEDIA_ROOT", BASE_DIR / "media"
)  # may not be inside STATIC_ROOT

LOGIN_URL = os.getenv("LOGIN_URL", "/accounts/login")
LOGOUT_URL = os.getenv("LOGOUT_URL", "/accounts/logout")
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"

# HTTP clients on those IP addresses get to see the Django Debug Toolbar.
INTERNAL_IPS = ["127.0.0.1"]

BLENDER_ID = {
    # MUST end in a slash:
    "BASE_URL": os.getenv("BLENDER_ID_BASE_URL", "http://id.local:8000/"),
    "OAUTH_CLIENT": os.getenv(
        "BLENDER_ID_OAUTH_CLIENT", "BLENDER-DEVELOPMENT-FUND-DEV"
    ),
    "OAUTH_SECRET": os.getenv(
        "BLENDER_ID_OAUTH_SECRET", "DEVELOPMENT-ONLY NON SECRET NEVER USE IN PRODUCTION"
    ),
    "BADGER_API_SECRET": os.getenv("BLENDER_ID_BADGER_API_SECRET", ""),
}

SESSION_COOKIE_SECURE = True

# Collection of automatically renewing subscriptions will be attempted this
# many times before giving up and setting the subscription status to 'on-hold'.
#
# This value is only used when automatic renewal fails, so setting it < 1 will
# be treated the same as 1 (one attempt is made, and failure is immediate, no
# retries).
LOOPER_CLOCK_MAX_AUTO_ATTEMPTS = 3

# Only retry collection of automatic renewals this long after the last failure.
# This separates the frequency of retrials from the frequency of the clock.
LOOPER_ORDER_RETRY_AFTER = relativedelta(days=2)

# The system user from looper/fixtures/systemuser.json. This user is required
# for logging things in the admin history (those log entries always need to
# have a non-NULL user ID).
LOOPER_SYSTEM_USER_ID = 1

# Convertion rates from the given rate to euros.
# This allows us to express the foreign currency in €.
LOOPER_CONVERTION_RATES_FROM_EURO = {
    "EUR": 1.0,
    "USD": 1.15,
}

# A list of payment method types used with stripe for setting a recurring payment:
# https://stripe.com/docs/api/checkout/sessions/create#create_checkout_session-payment_method_types
STRIPE_OFF_SESSION_PAYMENT_METHOD_TYPES = [
    "card",
    "link",
    "paypal",
]

STRIPE_CHECKOUT_SUBMIT_TYPE = "donate"

# Blender Fund income targets in €, used for themometer on landing page.
FUND_INCOME_TARGET = Money("EUR", cents=2_000 * 100)
FUND_INCOME_TARGET_LABEL = "½ developers"
SHOW_THERMOMETER = False


GEOIP2_DB = BASE_DIR / "GeoLite2-Country_20181002" / "GeoLite2-Country.mmdb"
SUPPORTED_CURRENCIES = {"EUR", "USD"}

# Used in our error templates and other pages.
ADMIN_EMAIL = "fundsupport@blender.org"

# Where Looper sends reminders for managed subscriptions.
LOOPER_MANAGER_MAIL = "noreply@blender.org"

# Determines who can use 'login as' from the admin.
CAN_LOGIN_AS = "blender_fund_main.admin.can_login_as"
LOGINAS_LOGOUT_REDIRECT_URL = reverse_lazy("admin:index")

# For collecting usage metrics
GOOGLE_ANALYTICS_TRACKING_ID = os.getenv("GOOGLE_ANALYTICS_TRACKING_ID")

LOOPER_SUBSCRIPTION_CREATION_WARNING_THRESHOLD = relativedelta(days=1)
LOOPER_ORDER_RECEIPT_PDF_URL = "settings_receipt_pdf"

LOOPER_USER_SEARCH_FIELDS = ("username", "email")

USE_THOUSAND_SEPARATOR = True
DECIMAL_SEPARATOR = "."
THOUSAND_SEPARATOR = ","
NUMBER_GROUPING = 3

CODEMIRROR_CONFIG = {
    "lineNumbers": True,
    "lineWrapping": True,
    "theme": "darcula",
}

CODEMIRROR_JS = [
    "/static/js/vendor/codemirror/codemirror.min.js",
]
CODEMIRROR_CSS = [
    "/static/js/vendor/codemirror/codemirror.min.css",
    "/static/js/vendor/codemirror/darcula.min.css",
    "/static/js/vendor/codemirror/custom.css",
]

WAFFLE_CREATE_MISSING_FLAGS = True
WAFFLE_FLAG_DEFAULT = False
WAFFLE_OVERRIDE = True

# Background tasks settings
MAX_ATTEMPTS = 5

# Braintree configuration
# Provide merchant accounts in the following format:
#   `CURRENCY_CODE:ACCOUNT_ID,CURRENCY_CODE:ACCOUNT_ID`
# where comma separates multiple merchant accounts
_BT_MERCHANT_ACCOUNTS = _get("BT_MERCHANT_ACCOUNTS", "", str)
BT_MERCHANT_ACCOUNTS = _BT_MERCHANT_ACCOUNTS.split(",") if _BT_MERCHANT_ACCOUNTS else []
BT_ENVIRONMENT = _get("BT_ENVIRONMENT", "Sandbox")  # Sandbox or Production
GATEWAYS = {
    "braintree": {
        "environment": getattr(braintree.Environment, BT_ENVIRONMENT),
        "merchant_id": _get("BT_MERCHANT_ID"),
        "public_key": _get("BT_PUBLIC_KEY"),
        "private_key": _get("BT_PRIVATE_KEY"),
        "merchant_account_ids": dict(acc.split(":") for acc in BT_MERCHANT_ACCOUNTS),
        # DevFund allows only automatic collection with Braintree:
        "supported_collection_methods": {"automatic"},
    },
    # No settings, but a key is required here to activate the gateway.
    "bank": {"supported_collection_methods": {"manual"}},
    "stripe": {
        "api_publishable_key": _get("STRIPE_API_PUBLISHABLE_KEY"),
        "api_secret_key": _get("STRIPE_API_SECRET_KEY"),
        "endpoint_secret": _get("STRIPE_ENDPOINT_SECRET"),
        "supported_collection_methods": {"automatic"},
    },
}

USE_STRIPE_CHECKOUT = True

# For development, dump email to the console instead of trying to actually send it.
EMAIL_BACKEND = os.getenv(
    "EMAIL_BACKEND", "django.core.mail.backends.console.EmailBackend"
)
DEFAULT_FROM_EMAIL = os.getenv(
    "DEFAULT_FROM_EMAIL", "Blender Development Fund <fund@blender.org>"
)
if os.environ.get("EMAIL_HOST") is not None:
    EMAIL_HOST = os.getenv("EMAIL_HOST")
if os.environ.get("EMAIL_PORT") is not None:
    EMAIL_PORT = os.getenv("EMAIL_PORT")
if os.environ.get("EMAIL_HOST_USER") is not None:
    EMAIL_HOST_USER = os.getenv("EMAIL_HOST_USER")
if os.environ.get("EMAIL_HOST_PASSWORD") is not None:
    EMAIL_HOST_PASSWORD = os.getenv("EMAIL_HOST_PASSWORD")

if TESTING:
    import logging

    logging.disable(logging.CRITICAL)
    # Use in-memory SQLite database for tests
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "ATOMIC_REQUESTS": True,
        },
    }

if DEBUG:
    MIDDLEWARE = [
        "debug_toolbar.middleware.DebugToolbarMiddleware",
        # 'pyinstrument.middleware.ProfilerMiddleware',
    ] + MIDDLEWARE
    INSTALLED_APPS += [
        "debug_toolbar",
    ]
    TEMPLATE_STRING_IF_INVALID = (
        "DEBUG WARNING: undefined template variable [%s] not found"
    )
    DEBUG_TOOLBAR_CONFIG = {
        "PROFILER_MAX_DEPTH": 20,
        "SQL_WARNING_THRESHOLD": 100,  # milliseconds
    }
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    SESSION_COOKIE_SECURE = False

if os.environ.get("ADMINS") is not None:
    # Expects the following format:
    #   ADMINS='J Doe: jane@example.com, John Dee: john@example.com'
    ADMINS = [
        [_.strip() for _ in adm.split(":")]
        for adm in os.environ.get("ADMINS").split(",")
    ]
    EMAIL_SUBJECT_PREFIX = f"[{ALLOWED_HOSTS[0]}]"
    SERVER_EMAIL = f"django@{ALLOWED_HOSTS[0]}"

# Optional Sentry configuration
SENTRY_DSN = _get("SENTRY_DSN")
if SENTRY_DSN:
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration

    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[DjangoIntegration()],
        traces_sample_rate=1.0,
        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=False,
        # Looks like IP address is also not sent when this is False.
    )
