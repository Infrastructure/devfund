from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.contrib.flatpages import views
from django.urls import include, path

from blender_fund_main.views import errors

handler500 = errors.error_handler_500


urlpatterns = (
    [
        path('accounts/', include('allauth.urls')),
        path('admin/doc/', include('django.contrib.admindocs.urls')),
        path('admin/', include('loginas.urls')),
        path('admin/notes/', include('blender_notes.urls')),
        path('admin/', admin.site.urls),
        path('error/500', errors.test_error_500),
        path('', include('blender_fund_main.urls')),
        path('', include('looper.urls')),
        # Flatpages
        path('about/', views.flatpage, {'url': '/about/'}, name='flatpage-about'),
        path('grants/', views.flatpage, {'url': '/grants/'}, name='flatpage-grants'),
        path('contact/', views.flatpage, {'url': '/contact/'}, name='flatpage-contact'),
        path(
            'corporate-memberships/',
            views.flatpage,
            {'url': '/corporate-memberships/'},
            name='flatpage-corporate-memberships',
        ),
        path('donate-once/', views.flatpage, {'url': '/donate-once/'}, name='flatpage-donate-once'),
    ]
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
